export function doCombination(arr) {
	var count = arr.length - 1; //数组长度(从0开始)
	var tmp = [];
	var totalArr = []; // 总数组
	return doCombinationCallback(arr, 0); //从第一个开始
	//js 没有静态数据，为了避免和外部数据混淆，需要使用闭包的形式
	function doCombinationCallback(arr, curr_index) {
		for (var val of arr[curr_index]) {
			tmp[curr_index] = val; //以curr_index为索引，加入数组
			//当前循环下标小于数组总长度，则需要继续调用方法
			if (curr_index < count) {
				doCombinationCallback(arr, curr_index + 1); //继续调用
			} else {
				totalArr.push(tmp); //(直接给push进去，push进去的不是值，而是值的地址)
			}
			//js  对象都是 地址引用(引用关系)，每次都需要重新初始化，否则 totalArr的数据都会是最后一次的 tmp 数据；
			let oldTmp = tmp;
			tmp = [];
			for (var index of oldTmp) {
				tmp.push(index);
			}
		}
		return totalArr;
	}
}
export function doHandleMonth(month) {
	var m = month;
	if (month.toString().length == 1) {
		m = "0" + month;
	}
	return m;
}
export function getDay(day) {
	var today = new Date();
	var targetday_milliseconds = today.getTime() + 1000 * 60 * 60 * 24 * day;
	today.setTime(targetday_milliseconds); //注意，这行是关键代码
	var tYear = today.getFullYear();
	var tMonth = today.getMonth();
	var tDate = today.getDate();
	tMonth = doHandleMonth(tMonth + 1);
	tDate = doHandleMonth(tDate);
	return tYear + "-" + tMonth + "-" + tDate;
}

//完全拷贝对象
export function objCopy(target, source) {
	for (let key in source) {
		target[key] = source[key]
	}
	return target
}

export function objextend(obj1, obj2) {
  for (let key in obj1) {
    if (obj2[key] || obj2[key] === 0) {
      obj1[key] = obj2[key]
    }
  }
  return obj1
}