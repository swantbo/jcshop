/**
 * 请求拦截
 * @param {Object} http
 */
module.exports = (vm) => {
	uni.$u.http.interceptors.request.use((config) => { // 可使用async await 做异步操作
			// 初始化请求拦截器时，会执行此方法，此时data为undefined，赋予默认{}
			config.data = config.data || {}
			let id = config.custom.id;
			let ruleList = vm.$store.state.$RuleList
			let PageAction = ''
			if (id) {
				let l = ruleList.filter(_ => {
					return _.level == id
				})
				let madmin = /madmin/
				let myconfig = /configuration\//
				var numreg = /[\s\S]*\d$/g;
				let url = config.url
				if (url.indexOf("?") > -1) {
					url = url.substr(0, url.indexOf("?"))
				}
				if (numreg.test(url)) {
					url = url.replace(/[\d]+/, '<id>')
				}
				if (l.length > 0) {
					l.forEach(_ => {
						let i = _.ruleExtend.findIndex(a => {
							return config.method == a.method && url == a.rule_route
						})
						if (i > -1) {
							PageAction = _.level + ',' + _.resource
						}
					})
				}
			}
			config.header = {
				...config.header,
				Authorization: vm.$store.state.$Token,
				Verification: 1,
				PageAction: PageAction
			}
			// 可以在此通过vm引用vuex中的变量，具体值在vm.$store.state中
			// console.log(vm.$store.state);
			return config
		}, (config) => // 可使用async await 做异步操作
		Promise.reject(config))
}
