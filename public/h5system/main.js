import Vue from 'vue'
import App from './App'

// vuex
import store from './store'

// 引入全局uView
import uView from '@/uni_modules/uview-ui'

import mixin from './common/mixin'

import has from '@/util/btnPermissions.js';


Vue.prototype.$store = store

Vue.config.productionTip = false

//filler

App.mpType = 'app'
Vue.use(uView)
//注册公共组件

import filler from '@/components/filler/filler.vue';
Vue.component("filler", filler);
import selectimg from '@/components/select-img/select-img.vue';
Vue.component("selectimg", selectimg);
//接口执行顺序
Vue.prototype.$getWx = new Promise(resolve => {
	Vue.prototype.$isResolve = resolve
})


// #ifdef MP
// 引入uView对小程序分享的mixin封装
const mpShare = require('@/uni_modules/uview-ui/libs/mixin/mpShare.js')
Vue.mixin(mpShare)
// #endif

Vue.mixin(mixin)

const app = new Vue({
    store,
    ...App
})

// 引入请求封装
require('./util/request/index')(app)

app.$mount()
