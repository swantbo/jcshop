import http from '@/common/http.js';
import base from '@/common/baseUrl'
import {
	getjuris
} from '@/common/jurisdiction'
export const getimgurl = (url, type) => {
	if (url) {
		if (url.indexOf('http') > -1) {
			return url
		} else {
			if (url.indexOf('/') == 0) {
				url = url.slice(0)
			}
			if (type == "1") {
				return base.imgUrl + '/' + url
			} else {
				let url=''
				getjuris('ossConfig').then(res=>{
					url= res + '/' + url
				})
				return url
				// if (store.state.oss) {
					
				// 	// return store.state.oss + '/' + url
				// 	// var endpoint;
				// 	// var bucket;
				// 	// if (store.state.oss.is_open == 1) {
				// 	// 	endpoint = store.state.oss.ali.endpoint;
				// 	// 	bucket = store.state.oss.ali.bucket;
				// 	// 	let burl = endpoint.replace('https://', 'https://' + bucket + '.')
				// 	// 	return burl + url
				// 	// } else {
				// 	// 	endpoint = store.state.oss.qcloud.endpoint;
				// 	// 	let burl = endpoint
				// 	// 	return burl + '/' + url
				// 	// }
				// } else {
				// 	return base.imgUrl + '/' + url
				// }
			}
		}
	} else {
		return ''
	}
}
module.exports = {
	getimgurl: getimgurl
}