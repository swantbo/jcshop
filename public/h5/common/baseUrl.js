import data from '@/static/base.json'

let baseUrl=''
let socketUrl = "";
if (process.env.NODE_ENV === 'development') {
	// 开发环境
	// baseUrl = "http://localhost:7001/";
	// socketUrl = "ws://localhost:6001/";
	baseUrl = data.baseUrl;
	// socketUrl = "ws://8.129.186.35:6001/";
} else if (process.env.NODE_ENV === 'production') {
	// 生产环境
	// #ifdef MP-WEIXIN
	baseUrl = data.baseUrl;
	// #endif
	
	// #ifndef MP-WEIXIN
	 baseUrl = window.location.origin;
	// #endif
	// socketUrl = "ws://8.129.186.35:6001/";
}

const courtConfig = {
	//微信公众号APPID
	publicAppId: "",
	//请求接口
	// baseUrl: "https://x.jc362.com",
	// #ifdef MP-WEIXIN
	baseUrl:baseUrl,
	imgUrl:baseUrl,
	// #endif
	// #ifndef MP-WEIXIN
	baseUrl:baseUrl,
	imgUrl:baseUrl,
	// #endif
	//webSocket地址
	socketUrl: socketUrl,
	//平台名称
	platformName: "惊蛰商城",
	//项目logo
	logoUrl: "",
	//页面分享配置
	share: {
		title: '惊蛰商城',
		// #ifdef MP-WEIXIN
		path: '/pages/home/home', //小程序分享路径
		// #endif
		// #ifdef H5 || APP-PLUS
		//公众号||APP分享
		desc: "uniApp-案例", // 分享描述
		link: "", // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
		imgUrl: "", // 分享图标
		// #endif
	}
};
//手机号验证正则表达式
const phoneRegular = /^1\d{10}$/;
//邮箱验证正则表达式
const mailRegular = /^\w+@\w+(\.[a-zA-Z]{2,3}){1,2}$/;
//密码验证正则表达式
const passwordRegular = /^[a-zA-Z0-9]{4,10}$/;
export default Object.assign({
	phoneRegular,
	mailRegular,
	passwordRegular
}, courtConfig);
