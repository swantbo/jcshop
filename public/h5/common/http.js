import sign from "./sign"
import base from '@/common/baseUrl';
import store from '@/store';
import util from '@/common/util'
import data from '@/static/base.json';
export const getBrowser = () => {
	let ua = navigator.userAgent.toLowerCase();
	if (ua.match(/MicroMessenger/i) == "micromessenger") {
		return "微信";
	}
	return "其他";
};
module.exports = (url, params = {}, method = 'GET') => {
	return new Promise((resolve, reject) => {
		params = params || {};
		method = method || 'GET'; // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
		let token = uni.getStorageSync('token') || null;
		let is_wx = uni.getStorageSync('is_wx')
		// #ifndef MP-WEIXIN
		let mall_id = uni.getStorageSync('mall_id') || null
		if (!mall_id) {
			uni.showModal({
				title: '缺少商场id!',
			})
			return false
		}
		// #endif
		// #ifdef MP-WEIXIN
		let mall_id = data.dymall_id
		// #endif
		let addons = [{
			list: ['/receive']
		}, {
			list: ['/article/list']
		}]
		let UserType = 3
		// #ifdef MP-WEIXIN
		UserType = 1
		// #endif
		// #ifndef MP-WEIXIN
		if (getBrowser() == '微信' && is_wx) {
			UserType = 2
		}
		// #endif
		// http请求
		uni.request({
			url: base.baseUrl + '/index' + url,
			method: method,
			data: params,
			header: {
				"Content-Type": "application/x-www-form-urlencoded",
				"UserType": UserType,
				"Authorization": token,
				'mid': mall_id
			},
			success: (res) => {
				if (params.need) {
					resolve(res);
					return false
				}
				switch (res.statusCode) {
					case 202:
						let mid = uni.getStorageSync('mall_id')
						uni.reLaunch({
							url: '/pages/closepage/closepage?mall_id=' + mid
						});
					case 401:
						if (res.data.msg == '请先登录' || res.data.error_msg == '请先登录') {
							// 用户未登录或用户异常，跳回登录页+
							store.commit('setUserInfo', {});
							store.commit('settoken', null);
							uni.removeStorageSync('token')
							store.commit('setlogin', false);
						} else if (res.data.msg == '您已被拉黑,请与客服人员联系!' || res.data.error_msg ==
							'您已被拉黑,请与客服人员联系!') {
							let mid = uni.getStorageSync('mall_id')
							uni.reLaunch({
								url: '/pages/blockpage/blockpage?mall_id=' + mid
							});
						} else if (res.data.msg == '无权查看' || res.data.error_msg == '无权查看') {
							uni.showModal({
								title: '提示',
								content: '无权查看！',
								confirmText: '确定',
								success: function(res) {
									uni.reLaunch({
										url: '/pages/index/index'
									});
								}
							})
						} else {
							resolve(res);
						}
						break;
					case 500:
						var data = res
						uni.showModal({
							title: '提示',
							content: '配置错误',
							confirmText: '确定',
							success: function(res) {
								if (res.confirm) {
									// util.uniCopy({
									// 	content: JSON.stringify(data),
									// 	success: res => {
									// 		uni.showToast({
									// 			title: res,
									// 			icon: 'none'
									// 		});
									// 	},
									// 	error: e => {
									// 		uni.showToast({
									// 			title: e,
									// 			icon: 'none',
									// 			duration: 3000
									// 		});
									// 	}
									// });
								} else if (res.cancel) {}
							}
						});
						break
					case 400:
					case 404:
					case 406:
						util.changeloading({
							loading: false
						})
						uni.showToast({
							title: res.data.msg || res.data.error_msg || '系统错误',
							icon: 'none',
						})
						reject(res)
						break
					case 200:
						if (res.data.error_msg) {
							return false
						} else {
							resolve(res);
						}
						break
					default:
						resolve(res);
				}
			},
			fail: (res) => {
				util.changeloading({
					loading: false
				})
				reject(res)
			}
		})
	})
}
