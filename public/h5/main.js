import Vue from 'vue'
import App from './App'
import uView from 'uview-ui';
Vue.use(uView);
// import
//封装数据请求
import http from '@/common/http';

Vue.prototype.$http = http;
//数据管理中心
import store from '@/store'
Vue.prototype.$store = store;
// #ifdef H5
//微信SDK
import '@/common/wxJsSDK.js';
// #endif
// #ifdef MP-WEIXIN
//挂载全局微信分享
import {
	wxShare
} from '@/common/baseutil'
Vue.prototype.wxShare = wxShare;
// #endif
// #ifdef H5
import {
	publicShare
} from '@/common/baseutil'
Vue.prototype.publicShare = publicShare;
// #endif
import base from '@/common/baseUrl';
Vue.config.$baseUrl = base.baseUrl
//全局组件
//挂载全局登录判断
import {
	judgeLogin,
	changeloading,
	getprice,
	getTheme
} from '@/common/util'
Vue.prototype.judgeLogin = judgeLogin;
Vue.prototype.changeloading = changeloading;
Vue.prototype.getprice = getprice;
Vue.prototype.$getWx = new Promise(resolve => {

	Vue.prototype.$isResolve = resolve
})
//页面跳转
import {
	Jumpurl
} from '@/common/jumpurl'
Vue.prototype.Jumpurl = Jumpurl;
import {
	getjuris,
	getjuris_addons
} from '@/common/jurisdiction'
Vue.prototype.getjuris = getjuris;
Vue.prototype.getjuris_addons = getjuris_addons;
import {
	getimgurl
} from '@/common/getimgurl'
Vue.prototype.getimgurl = getimgurl;
//底部菜单
import tabbar from "@/components/tabbar/tabbar";
Vue.component("tab-bar", tabbar);
//自定义头部
import jnavbar from "@/components/j-navbar/j-navbar";
Vue.component("jnavbar", jnavbar);
//快速购买
import trolley from '@/components/trolley/trolley.vue';
Vue.component("trolley", trolley);
//登录
import UniLogin from '@/components/uni-login/uni-login.vue';
Vue.component("uni-login", UniLogin);
//绑定手机号
import bindmobile from '@/components/bindmobile/index.vue';
Vue.component("bind-mobile", bindmobile);
import vimage from '@/components/vimage/vimage.vue';
Vue.component("vimage", vimage);
//
import vfooter from '@/components/footer/footer.vue';
Vue.component("vfooter", vfooter);
import vcustomer from '@/components/customer/customer.vue';
Vue.component("vcustomer", vcustomer);
import common from '@/components/common/index.vue'
Vue.component("common", common);
import guesslike from "@/components/guesslike/index.vue"
Vue.component("guesslike", guesslike);
//全局加载
import nloading from '@/components/nloading/nloading.vue';
import s_list from '@/components/s_list/s_list.vue';
Vue.component("slist", s_list);
// import publicModule from "@/components/common/public-module.vue";
// Vue.component("public-module", publicModule);
import floatmenu from '@/components/floatmenu/floatmenu.vue';
Vue.component("floatmenu", floatmenu);
App.mpType = 'app'
const app = new Vue({
	...App,
})
app.$mount()
