import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);
const files = require.context("./modules", false, /\.js$/);
let modules = {
	state: {
		searchList: uni.getStorageSync("__SEARCH") || [],
		isFollow: uni.getStorageSync("__FOLLOW") || '',
	},
	mutations: {
		SET_SEARCH_LIST(state, search) {
			state.searchList = search
		},
		CLEAR_SEARCH_LIST(state) {
			state.searchList = []
		},
		SET_FOLLOW(state, follow) {
			state.isFollow = follow
		},
	},
	actions: {
		set_search({
			commit,
			state
		}, search) {
			let list = state.searchList
			let a = list.findIndex((value) => value.name == search.name)
			if (a > -1) {
				list.splice(a, 1)
			}
			list.unshift(search)
			uni.setStorageSync("__SEARCH", list)
			commit('SET_SEARCH_LIST', list)
		},
		
		clear_search({
			commit
		}) {
			uni.setStorageSync("__SEARCH", [])
			commit('CLEAR_SEARCH_LIST')
		},
		
		set_follow({
			commit,
			state
		}, follow) {
			uni.setStorageSync("__FOLLOW", follow)
			commit('SET_SEARCH_LIST', follow)
		},
		
	
	}
};

files.keys().forEach((key) => {
	Object.assign(modules.state, files(key)["state"]);
	Object.assign(modules.mutations, files(key)["mutations"]);
	Object.assign(modules.actions, files(key)["actions"]);
});
for (var item in modules.state) {
	uni.getStorageSync(item) ? modules.state[item] = uni.getStorageSync(item) : false;
}

const store = new Vuex.Store(modules);
export default store;
