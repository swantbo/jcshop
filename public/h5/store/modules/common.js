import baseJson from '@/static/base.json'
export const state = {
	//webView地址
	webViewUrl: "",
	loadingShow: false,
	//微信场景参数
	chatScenesInfo: {},
	//登录弹窗状态
	loginPopupShow: false,
	//底部菜单
	BottomMenu: {},
	hastabbar: false,
	SystemInfo: {},
	//配置信息
	integralBalance: null,
	mallBaseinfo: null,
	mallBase: null,
	mallPayMode: null,
	memberLevel: null,
	leveldata: null,
	tradeSetting: null,
	customerService: null,
	agentExplain: null,
	lottery: null,
	baseJson: baseJson,
	oss: null,
	trolley: {
		show: false,
		id: 0
	}, //快速购买
	loading: false,
	//当前位置
	currentAddress: {
		areaName: "请选择",
		areaId: ''
	},
	tab_id: 0,
	addons: [],
	pagelist: [],
	isTouchPage: 0,
	GapHeight:120,
	skin: `
		--main-color:#ff3c29;
		--content-color:#fcedeb;
	`
};
//缓存浏览器的数据名称
const cacheNameList = ["userInfo", "webViewUrl"];
let clearTime;
export const mutations = {
	// 皮肤更换
	skinPeeler(state, skin) {
		// 将皮肤配置JSON转为以 ; 分割的字符串（style 值）
		let style = `--main-color:${skin.color};--content-color:${skin.subcolor}`
		state.skin = style;
	},
	//取出缓存数据（打开APP就取出）
	setCacheData(state) {
		for (let name of cacheNameList) {
			let data;
			// #ifndef H5
			data = uni.getStorageSync(name);
			// #endif
			// #ifdef H5
			data = sessionStorage.getItem(name) || localStorage.getItem(name);
			// #endif
			if (data) {
				// #ifdef H5
				try {
					data = JSON.parse(data);
				} catch (e) {}
				// #endif
				state[name] = data;
			}
		}
	},
	//WebView地址
	setWebViewUrl(state, data) {
		if (data) {
			state.webViewUrl = data;
			// #ifdef H5
			window.sessionStorage.setItem('webViewUrl', data);
			// #endif
		}
	},
	//数据加载状态
	setLoadingShow(state, data) {
		if (state.loadingShow) {
			clearTime && clearTimeout(clearTime);
			clearTime = setTimeout(function() {
				state.loadingShow = data;
			}, 300);
		} else {
			state.loadingShow = data;
		}
	},
	//微信场景参数
	setChatScenesInfo(state, data) {
		if (data) {
			state.chatScenesInfo = Object.assign({}, state.chatScenesInfo, data);
		}
	},
	async setBottomMenu(state, data) {
		state.BottomMenu = data
	},
	setintegralBalance(state, data) {
		if (data) {
			state.integralBalance = Object.assign({}, state.integralBalance, data);
		}
	},
	setBaseinfo(state, data) {
		if (data) {
			state.mallBaseinfo = Object.assign({}, state.mallBaseinfo, data);
		}
	},
	setmallBase(state, data) {
		if (data) {
			state.mallBase = Object.assign({}, state.mallBase, data);
		}
	},
	setossConfig(state, data) {
		if (data) {
			state.oss = data
		}
	},
	settradeSetting(state, data) {
		if (data) {
			state.tradeSetting = Object.assign({}, state.tradeSetting, data);
		}
	},
	setmallPayMode(state, data) {
		if (data) {
			state.mallPayMode = Object.assign({}, state.mallPayMode, data);
		}
	},
	setcustomerService(state, data) {
		if (data) {
			state.customerService = Object.assign({}, state.customerService, data);
			uni.setStorageSync('customerService', state.customerService);
		}
	},
	setagentExplain(state, data) {
		if (data) {
			state.agentExplain = Object.assign({}, state.agentExplain, data);
		}
	},
	setmemberLevel(state, data) {
		if (data) {
			state.memberLevel = Object.assign({}, state.memberLevel, data);
		}
	},
	setleveldata(state, data) {
		if (data) {
			state.leveldata = Object.assign({}, state.leveldata, data);
		}
	},
	setlottery(state, data) {
		if (data) {
			state.lottery = Object.assign({}, state.lottery, data);
		}
	},
	sethastabbar(state, data) {
		state.hastabbar = data
		uni.setStorageSync('hastabbar', state.hastabbar);
	},
	setSystemInfo(state, data) {
		if (data) {
			state.SystemInfo = data
			uni.setStorageSync('SystemInfo', state.SystemInfo);
		}
	},
	setaddons(state, data) {
		if (data) {
			state.addons = data
			uni.setStorageSync('addons', state.addons);
		}
	},
	setpagelist(state, data) {
		if (data) {
			state.pagelist = data
			uni.setStorageSync('pagelist', state.pagelist);
		}
	},
	//加载动画
	setloading(state, data) {
		state.loading = data.loading;
		state.loadtext = data.loadtext ? data.loadtext : ''
	},
	//登录弹窗状态
	setLoginPopupShow(state, data) {
		state.loginPopupShow = data;
	},
	setGapHeight(state, data) {
		state.GapHeight = data;
	},
	
	settab(state, data) {
		state.tab_id = data
	},
	settrolley(state, data) {
		state.trolley = data
	},
	//当前地址
	setCurrentAddress(state, data) {
		if (data) {
			state.currentAddress = Object.assign(state.currentAddress, data);
			let addressInfo = {
				"provinceId": state.currentAddress.provinceId,
				"provinceName": state.currentAddress.provinceName,
				"cityId": state.currentAddress.cityId,
				"cityName": state.currentAddress.cityName,
				"areaId": state.currentAddress.areaId,
				"areaName": state.currentAddress.areaName,
			};
			uni.setStorageSync('currentAddress', addressInfo);
		}
	},
	settouch(state, data) {
		state.isTouchPage = data;
	},
};
export const actions = {};
export const getters = {}
