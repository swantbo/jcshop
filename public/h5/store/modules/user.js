export const state = {
	//用户数据
	userInfo: {},
	islogin: true,
	token: null,
};
export const mutations = {
	//储存用户信息
	setUserInfo(state, data) {
		// if (data) {
			state.userInfo = data
			uni.setStorageSync('userInfo', state.userInfo);
		// }else{
		// 	state.userInfo = {}
		// }
	},

	settoken(state, data) {
		state.token = data;
		uni.setStorageSync('token', data);
		
	},
	setlogin(state, data) {
		state.islogin = data
	},
	// 退出APP
	emptyUserInfo(state) {
		state.userInfo = {};
		// #ifdef H5
		window.sessionStorage.removeItem("userInfo");
		// #endif
		// #ifndef H5
		uni.removeStorageSync("userInfo");
		// #endif
	},
};
export const actions = {};
