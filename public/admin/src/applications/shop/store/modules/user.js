import {
  shop_login,
  shop_logout,
} from '@/api/common'
import {
  getToken,
  setToken,
  setShopToken,
  setMallToken,
  removeToken,
  removeShopToken,
  getLoginType
} from '@/utils/auth'
import {
  resetRouter
} from '@/applications/shop/router'
const getDefaultState = () => {
  return {
    token: getToken(),
    name: '',
    avatar: '',
    roles: [],
    ruleList: [],
    license: false
  }
}
const state = getDefaultState()
const ruleList = ''
const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles
  },
  SET_ROLELIST: (state, ruleList) => {
    state.ruleList = ruleList
  },
  SET_LICENSE: (state, license) => {
    state.license = license
  }
}
const actions = {
  // user login
  login({
    commit,
    ruleList
  }, userInfo) {
    const {
      username,
      password
    } = userInfo
    return new Promise((resolve, reject) => {
      shop_login({
        username: username.trim(),
        password: password
      }).then(async response => {
        let data = response.msg
        commit('SET_TOKEN', data.token)
        setToken(data.token);
        let user={
          site:data.msg.site,
          user:data.msg.user
        }
        localStorage.setItem('shop_user', JSON.stringify(user))
        localStorage.setItem('shop_ruleList', JSON.stringify(data.ruleList))
        //设置shop_token
        if (data.license == false) {
          commit('SET_LICENSE', !data.license)
          return false
        }
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },
  silent_login({
    commit,
    ruleList
  }, userInfo) {
    const {
      id,
    } = userInfo
    let data = {
      mall_id: id
    };
    return new Promise((resolve, reject) => {
      mall_login(data).then(data => {
        let token = getToken()
        setMallToken(token)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },
  //静默跳回
  silent_back({
    commit,
  }, userInfo) {
    const {
      type
    } = userInfo
    localStorage.setItem('login_type', type)
    return new Promise((resolve, reject) => {
      let token = getToken()
      removeToken()
      setMallToken(token)
      // let ruleList = [{}, {}]
      // localStorage.setItem('ruleList', JSON.stringify(ruleList))
      commit('SET_ROLELIST', null)
      resolve()
    })
  },
  // get user getrule
  getrule({
    commit,
    state
  }) {
    return new Promise((resolve, reject) => {
      let ruleList = JSON.parse(localStorage.getItem('shop_ruleList'))
      if (!ruleList) {
        ruleList = [{}, {}]
      }
      commit('SET_ROLELIST', ruleList)
      resolve(ruleList)
    })
  },
  setrule({
    commit,
    state
  }) {
    return new Promise((resolve, reject) => {
      localStorage.setItem('shop_ruleList', [{}, {}]);
      commit('SET_ROLELIST', [{}, {}])
      resolve()
    })
  },
  // user logout
  logout({
    commit,
    state
  }) {
    return new Promise((resolve, reject) => {
      shop_logout(state.token).then(() => {
        removeToken() // must remove  token  first
        resetRouter()
        commit('RESET_STATE')
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },
  // remove token
  resetToken({
    commit
  }) {
    return new Promise(resolve => {
      removeToken() // must remove  token  first
      commit('RESET_STATE')
      resolve()
    })
  },
  setlicense({
    commit
  }) {
    return new Promise(resolve => {
      commit('SET_LICENSE', true)
      resolve()
    })
  },
  resetsetlicense({
    commit
  }) {
    return new Promise(resolve => {
      commit('SET_LICENSE', false)
      resolve()
    })
  },
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}
