import {
  getallroleslist,
  getAddonsList,
  information,
  getlesseeentry,
  getpicgeturl,
  agentrank
} from '@/api/common'
import {
  resetRouter
} from '@/applications/mall/router'
import Vue from 'vue'
const getDefaultState = () => {
  return {
    allroleslist: [],
    addons: [],
    info: null,
    entry: null,
    picurl:null,
    agent_rank:3
  }
}
const state = getDefaultState()
const mutations = {
  SET_ALLROLELIST: (state, allroleslist) => {
    state.allroleslist = allroleslist
  },
  SET_ADDONS: (state, addons) => {
    state.addons = addons
  },
  SET_INFO: (state, info) => {
    state.info = info
  },
  SET_ENTRY: (state, entry) => {
    console.log(Vue.prototype.$edition_type)
    state.entry = entry
  },
  SET_PICURl: (state, data) => {
    state.picurl = data
  },
  SET_AGENT_RANK: (state, data) => {
    state.agent_rank = data
  },
}
const actions = {
  getallroleslist({
    commit
  }) {
    return new Promise((resolve, reject) => {
      getallroleslist({}).then(response => {
        var data = response.msg
        commit('SET_ALLROLELIST', data)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },
  getAddonsList({
    commit
  }) {
    return new Promise((resolve, reject) => {
      getAddonsList({n_load:1}).then(response => {
        var data = response.msg
        // data.token = '80b0de7955803e15f941d6bfb0792f8a0788fb98'
        commit('SET_ADDONS', data)
        // setToken(data.token)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },
  getpicgeturl({
    commit
  }) {
    return new Promise((resolve, reject) => {
      getpicgeturl({n_load:1}).then(response => {
        var data = response.msg
        // data.token = '80b0de7955803e15f941d6bfb0792f8a0788fb98'
        commit('SET_PICURl', data)
        // setToken(data.token)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  getinformation({
    commit
  }) {
    return new Promise((resolve, reject) => {
      return {}
      // information({n_load:1}).then(response => {
      //   var data = response.msg
      //   commit('SET_INFO', data)
      //   resolve(data)
      // }).catch(error => {
      //   reject(error)
      // })
    })
  },
  getagentrank({
    commit
  }) {
    return new Promise((resolve, reject) => {
      agentrank({n_load:1}).then(response => {
        var data = response.msg
        commit('SET_AGENT_RANK', data)
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },
  getlesseeentry({
    commit
  }) {
    return new Promise((resolve, reject) => {
      getlesseeentry({url:'madmin',n_load:1}).then(response => {
        var data = response.msg
        commit('SET_ENTRY', data)
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}
