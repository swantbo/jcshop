import request from '@/utils/request';
var qs = require('qs');
//**********以下接口为商品部分调用**********
//获取所有商品
export function getAllPro() {
  return request({
    url: '/shop/spu',
    method: 'get'
  });
};
//获取商品列表
export function getProList(data, page, size) {
  return request({
    url: '/shop/spu/list?page=' + page + '&size=' + size,
    method: 'post',
    data
  });
};
//获取已售罄商品列表
export function getselloutList(data, page, size) {
  return request({
    url: '/shop/spu/sellout?page=' + page + '&size=' + size,
    method: 'post',
    data
  });
};
//新增商品
export function addPro(data) {
  return request({
    url: '/shop/spu',
    method: 'post',
    data
  });
};
//删除商品
export function delPro(data) {
  return request({
    url: '/shop/spu',
    method: 'delete',
    data
  });
};
//编辑商品
export function editPro(data) {
  return request({
    url: '/shop/spu/' + data.id,
    method: 'put',
    data
  });
};
// /spu/name/:id
//商品上下架(批量)
export function upDownPro(data) {
  return request({
    url: '/shop/spu',
    method: 'put',
    data
  });
};
//商品库存修改
export function editProInventory(data) {
  return request({
    url: '/shop/spu/inventory/' + data.id,
    method: 'put',
    data
  });
};
//获取商品
export function getPro(id, data) {
  return request({
    url: '/shop/spu/' + id,
    method: 'get',
    data
  });
};
//获取所有商品分类
export function getAllProCate(data) {
  return request({
    url: '/shop/classify',
    method: 'get',
    data
  });
};
//获取所有运费模板
export function getAllFreight(data) {
  return request({
    url: '/shop/freight/temp',
    method: 'get',
    data
  });
};
//根据id数组获取对应商品的基本数据
export function getProByIds(data) {
  return request({
    url: '/shop/spu/ids',
    method: 'post',
    data
  });
};
export function getAuditProList(data, page, size) {
  return request({
    url: '/shop/spu/list?page=' + page + '&size=' + size,
    method: 'post',
    data
  });
};
// 获取商品预览二维码
export function getLinkUrl(data) {
  let mdata = qs.stringify(data);
  return request({
    url: '/shop/spu/shop_preview?' + mdata,
    method: 'post',
    data
  })
}
export function integralopen(data) {
  return request({
    url: '/shop/integral/open',
    method: 'get',
    data
  })
}
//重新发起审核申请
export function reaudit(data) {
  return request({
    url: '/shop/spu/reaudit/' + data.id,
    method: 'put',
    data
  });
};
