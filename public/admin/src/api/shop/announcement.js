import request from '@/utils/request';
//**********以下接口为公告部分调用**********
//新增公告
export function addNotice(data){
  return request({
    url: `/shop/notice`,
    method: 'post',
    data
  });
};

//删除公告
export function delNotice(id){
  return request({
    url: `/shop/notice/` + id,
    method: 'delete'
  });
};

//编辑公告
export function editNotice(data){
  return request({
    url: `/shop/notice/` + data.id,
    method: 'put',
    data
  });
};

//获取公告
export function getNotice(id){
  return request({
    url: `/shop/notice/` + id,
    method: 'get'
  });
};

//获取公告列表
export function getNoticeList(data,page,size) {
  return request({
    url: `/shop/notice/list?page=${page}&size=` + size,
    method: 'post',
    data
  });
};
