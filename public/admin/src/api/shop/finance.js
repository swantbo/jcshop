import request from '@/utils/request';
//获取商户提现列表
export function getWithdrawList(data, page, size) {
  return request({
    url: '/shop/merchant/withdraw/list?page=' + page + '&size=' + size,
    method: 'post',
    data
  });
};
//获取当前商户详情
export function getmerchantdetail(data) {
  return request({
    url: '/shop/merchant',
    method: 'get',
  })
}
//商户申请提现
export function merchantwithdraw(data) {
  return request({
    url: '/shop/merchant/withdraw',
    method: 'post',
    data
  })
}
export function merchantwithdrawtotal(data) {
  return request({
    url: '/shop/merchant/withdraw/total',
    method: 'put',
    data
  })
}

//商户入账详情
export function setShoplementList(data,page,size) {
  return request({
    url: '/shop/merchant/settlement/list?page=' + page + '&size=' + size,
    method: 'post',
    data
  })
}
