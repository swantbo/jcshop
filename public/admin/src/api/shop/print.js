import request from '@/utils/request';
//**********以下接口为电子面单打印部分调用**********
//电子面单 - 创建打印对象
export function createExpressBill(data) {
	return request({
    url: `/shop/express_bill/create`,
    method: 'post',
    data
	});
};

//电子面单 - 模板创建
export function addEBTemplate(data) {
	return request({
    url: `/shop/express_bill/template`,
    method: 'post',
    data
	});
};

//电子面单 - 模板删除
export function delEBTemplate(id) {
	return request({
    url: `/shop/express_bill/template/` + id,
    method: 'delete'
	});
};

//电子面单 - 模板修改
export function editEBTemplate(data) {
	return request({
    url: `/shop/express_bill/template/` + data.id,
    method: 'put',
    data
	});
};

//电子面单 - 模板修改(设为默认)
export function editEBDefault(id) {
	return request({
    url: `/shop/express_bill/template/default/` + id,
    method: 'post'
	});
};

//电子面单 - 模板获取
export function getEBTemplate(id) {
	return request({
    url: `/shop/express_bill/template/` + id,
    method: 'get'
	});
};

//电子面单 - 模板列表获取
export function getEBTemplateList() {
	return request({
    url: `/shop/express_bill/template/list`,
    method: 'post'
	});
};

//**********以下接口为小票打印部分调用**********
//小票 - 打印小票
export function createReceipt(data) {
	return request({
    url: `/shop/receipts/create`,
    method: 'post',
    data
	});
};

//小票 - 模板创建
export function addReceiptTemplate(data) {
	return request({
    url: `/shop/receipts/template`,
    method: 'post',
    data
	});
};

//小票 - 模板删除
export function delReceiptTemplate(id) {
	return request({
    url: `/shop/receipts/template/` + id,
    method: 'delete'
	});
};

//小票 - 模板修改
export function editReceiptTemplate(data,id) {
	return request({
    url: `/shop/receipts/template/` + id,
    method: 'put',
    data
	});
};

//小票 - 模板修改(设为默认)
export function editReceiptDefault(id) {
	return request({
    url: `/shop/receipts/template/default/` + id,
    method: 'post'
	});
};

//小票 - 模板获取
export function getReceiptTemplate(id) {
	return request({
    url: `/shop/receipts/template/` + id,
    method: 'get'
	});
};

//小票 - 模板列表获取
export function getReceiptTemplateList() {
	return request({
    url: `/shop/receipts/template/list`,
    method: 'get'
	});
};

//小票 - 打印机创建
export function addReceiptPrinter(data) {
	return request({
    url: `/shop/receipts/printer`,
    method: 'post',
    data
	});
};

//小票 - 打印机删除
export function delReceiptPrinter(id) {
	return request({
    url: `/shop/receipts/printer/` + id,
    method: 'delete'
	});
};

//小票 - 打印机修改
export function editReceiptPrinter(data,id) {
	return request({
    url: `/shop/receipts/printer/` + id,
    method: 'put',
    data
	});
};

//小票 - 打印机修改(设为默认)
export function editRecPrinterDefault(id) {
	return request({
    url: `/shop/receipts/printer/default/` + id,
    method: 'get'
	});
};

//小票 - 打印机获取
export function getReceiptPrinter(id) {
	return request({
    url: `/shop/receipts/printer/` + id,
    method: 'get'
	});
};

//小票 - 打印机列表获取
export function getReceiptPrinterList() {
	return request({
    url: `/shop/receipts/printer/list`,
    method: 'get'
	});
};

//小票 - 打印计划创建
export function addReceiptTask(data) {
	return request({
    url: `/shop/receipts/task`,
		method: 'post',
		data
	});
};

//小票 - 打印计划删除
export function delReceiptTask(id) {
	return request({
    url: `/shop/receipts/task/` + id,
    method: 'delete'
	});
};

//小票 - 打印计划修改
export function editReceiptTask(data) {
	return request({
    url: `/shop/receipts/task/` + data.id,
		method: 'put',
		data
	});
};

//小票 - 打印计划获取
export function getReceiptTask(id) {
	return request({
    url: `/shop/receipts/task/` + id,
		method: 'get'
	});
};

//小票 - 打印计划列表获取
export function getReceiptTaskList() {
	return request({
    url: `/shop/receipts/task/list`,
		method: 'post'
	});
};

//**********以下接口为发货单打印部分调用**********
//电子面单 - 创建打印对象
export function createinvoiceBill(data) {
	return request({
    url: `/shop/invoice/print`,
    method: 'post',
    data
	});
};
// //发货单 - 模板创建
export function addinvoice(data) {
	return request({
    url: `/shop/invoice`,
    method: 'post',
    data
	});
};

//发货单 - 模板删除
export function delinvoice(id) {
	return request({
    url: `/shop/invoice/` + id,
    method: 'delete'
	});
};

//发货单 - 模板修改
export function editinvoice(data,id) {
	return request({
    url: `/shop/invoice/` + id,
    method: 'put',
    data
	});
};



//发货单 - 模板获取
export function getinvoicedetial(id) {
	return request({
    url: `/shop/invoice/` + id,
    method: 'get'
	});
};

//发货单 - 模板列表获取
export function getinvoiceList(data) {
	return request({
    url: `/shop/invoice?page=${data.page}&size=${data.size}`,
    method: 'get',
    data
	});
};
export function saveexpress_bill(data) {
	return request({
    url: `/shop/express_bill/config`,
    method: 'post',
    data
	});
};

//发货单 - 模板获取
export function getexpress_bill() {
	return request({
    url: `/shop/express_bill/config`,
    method: 'get'
	});
};
//config
