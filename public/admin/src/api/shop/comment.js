import request from '@/utils/request';
//**********以下接口为评论部分调用**********
//删除评论
export function delComment(id) {
	return request({
		url: '/shop/evaluate/' + id,
		method: 'delete'
	});
};

//更新评论 - 评论审核
export function auditComment(data) {
	return request({
		url: '/shop/evaluate/examine',
		method: 'post',
		data
	});
};

//更新评论 - 掌柜回复
export function replyComment(data) {
	return request({
		url: '/shop/evaluate/reply',
		method: 'post',
		data
	});
};

//获取评论列表
export function getCommentList(data, page, size) {
	return request({
		url: `/shop/evaluate/evaluateList?is_self=${data.is_self}&name=${data.name}&order_no=${data.order_no}&cname=${data.cname}&status=${data.status}&page=${page}&size=${size}`,
		method: 'get'
	});
};

export function getMallCommentList(data, page, size) {
	return request({
		url: `/madmin/evaluate/evaluateList?is_self=${data.is_self}&name=${data.name}&order_no=${data.order_no}&cname=${data.cname}&status=${data.status}&page=${page}&size=${size}`,
		method: 'get'
	});
};

