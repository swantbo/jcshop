import request from '@/utils/request';
//**********以下接口为门店部分调用**********
//获取所有门店
export function getallStore(){
  return request({
    url: '/shop/store',
    method: 'get'
  });
};


//获取门店列表
export function getStoreList(data,page,size) {
  return request({
    url: '/shop/store/list?page=' + page + '&size=' + size,
    method: 'post',
    data
  });
};

//新增门店
export function addStore(data){
  return request({
    url: '/shop/store',
    method: 'post',
    data
  });
};

//删除门店
export function delStore(id){
  return request({
    url: '/shop/store/' + id,
    method: 'delete'
  });
};

//编辑门店
export function editStore(data){
  return request({
    url: '/shop/store/' + data.id,
    method: 'put',
    data
  });
};

//获取门店
export function getStore(id){
  return request({
    url: '/shop/store/' + id,
    method: 'get'
  });
};

//**********以下接口为店员部分调用**********
//list
export function getassistantlist(data,page,size) {
    return request({
      url: '/shop/store/assistant/list?page=' + page + '&size=' + size ,
      method: 'post',
      data
    });
  };
//add
export function saveassistant(data) {
  return request({
    url: '/shop/store/assistant',
    method: 'post',
    data
  })
}

//detail
export function getassistantdetail(data) {
  return request({
    url: '/shop/store/assistant/' + data.id,
    method: 'get',
  })
}
//update
export function editassistant(data) {
  return request({
    url: '/shop/store/assistant/' + data.id,
    method: 'put',
    data
  })
}
// delete
export function delassistant(data) {
  return request({
    url: '/shop/store/assistant/' + data.id,
    method: 'delete',
    data
  })
}
