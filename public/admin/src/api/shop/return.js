import request from '@/utils/request';
//**********以下接口为退货地址部分调用**********
//新增退货地址
export function addAddress(data){
  return request({
    url: `/shop/return/address`,
    method: 'post',
    data
  });
};

//删除退货地址
export function delAddress(id){
  return request({
    url: `/shop/return/address/` + id,
    method: 'delete'
  });
};

//编辑退货地址
export function editAddress(data){
  return request({
    url: `/shop/return/address/` + data.id,
    method: 'put',
    data
  });
};

//获取退货地址
export function getAddress(id){
  return request({
    url: `/shop/return/address/` + id,
    method: 'get'
  });
};

//获取退货地址列表
export function getAddressList(data,page,size) {
  return request({
    url: `/shop/return/address/list?page=${page}&size=` + size,
    method: 'post',
    data
  });
};
