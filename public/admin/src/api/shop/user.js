import request from '@/utils/request'
//**********以下接口为商户端用户数据相关功能**********
//获取用户账户下的优惠券列表
export function userCouponList(data,page,size) {
  return request({
    url: `/shop/user/coupon/list?page=${page}&size=` + size,
    method: 'post',
    data
  })
};

