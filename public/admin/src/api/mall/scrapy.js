import request from '@/utils/request'
import qs from 'qs'
export function getscrapyLogList(data) {
  let params = qs.stringify(data)
  return request({
    url: `/shop/spu/scrapyLogList?${params}`,
    method: 'get'
  })
}

export function addscrapy(data) {
  return request({
    url: '/shop/spu/scrapy',
    method: 'post',
    data
  })
}

export function scrapySave(data) {
  return request({
    url: '/madmin/configuration/scrapySave',
    method: 'post',
    data
  })
}

export function getscrapy(data) {
  return request({
    url: '/madmin/configuration/scrapyRead',
    method: 'get',
    data
  })
}
