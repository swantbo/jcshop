import request from '@/utils/request';
//**********以下接口为站点配置模块**********
//增加站点配置(单条)
export function addSiteConfig(data){
  return request({
    url: '/madmin/configuration/' + data.type,
    method: 'post',
    data
  });
};

//修改站点配置(单条)
export function editSiteConfig(data){
  return request({
    url: '/madmin/configuration/' + data.type,
    method: 'put',
    data
  });
};

//获取站点配置(单条)
export function getSiteConfig(type){
  return request({
    url: '/madmin/configuration/' + type,
    method: 'get'
  });
};

export function integralfind(type){
  return request({
    url: '/madmin/integral/share/find',
    method: 'get'
  });
};

//修改
export function editintegralshare(data){
  return request({
    url: '/madmin/integral/share/save',
    method: 'put',
    data
  });
};

export function getwxurl(){
  return request({
    url: '/madmin/wxurl',
    method: 'get'
  });
};

// 抽奖配置 
export function postLotteryConfig(data){
  return request({
    url: '/madmin/configuration/lottery',
    method: 'post',
    data
  });
};
export function editLotteryConfig(data){
  return request({
    url: '/madmin/configuration/lottery',
    method: 'put',
    data
  });
};
// 获取
export function getLotteryConfig(){
  return request({
    url: '/madmin/configuration/lottery',
    method: 'get',
  });
};