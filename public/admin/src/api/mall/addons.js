import request from '@/utils/request'

//列表
export function getAddonsList() {

  return request({
    url: '/madmin/addons' ,
    method: 'get'
  })
}
export function getNewAddonsList() {

  return request({
    url: '/madmin/addons/list' ,
    method: 'get'
  })
}



export function installAddons(data) {

  return request({
    url: '/madmin/addons/install' ,
    method: 'post',
    data
  })
}

export function uninstallAddons(data) {

  return request({
    url: '/madmin/addons/uninstall' ,
    method: 'post',
    data
  })
}

//激活
export function activeAddons(data) {

  return request({
    url: '/madmin/authentication/active/addons' ,
    method: 'post',
    data
  })
}
