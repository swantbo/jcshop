import request from '@/utils/request';
//**********以下接口为多端配置模块**********
//**********以下接口为多端配置 - 小程序上传模块**********
//登陆(获取二维码)
export function getLoginQrcode(data){
  return request({
    url: '/madmin/wxa/config/login',
    method: 'get',
    data
  });
};

//预览(轮询接口,检测是否扫码成功)
export function getIfScanSuccess(data){
  return request({
    url: '/madmin/wxa/config/preview?plugins='+data.plugins,
    method: 'get',
    data
  });
};

//上传小程序代码
export function uploadMpCode(data){
  return request({
    url: `/madmin/wxa/config/send?version=${data.version}&desc=` + encodeURIComponent(data.desc),
    method: 'get',
    data
  });
};
export function wxurlpreview(data){
  return request({
    url: `/madmin/wxurl/preview`,
    method: 'get'
  });
};
export function wxislogin(data){
  return request({
    url: `/madmin/wxa/config/is_preview`,
    method: 'get',
    data
  });
};
