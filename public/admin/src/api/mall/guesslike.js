import request from '@/utils/request';

// 获取推荐商品配置
export function getPickConfig() {
    return request({
        url: '/madmin/pick/get_config',
        method: 'get',
    })
}
// 创建更新 商品配置
export function savePickConfig(data) {
    return request({
        url: '/madmin/pick/save',
        method: 'POST',
        data
    })
}
// 获取推荐商品
export function getGrounding(data){
    return request({
        url:'/madmin/pick/get_grounding_goods',
        method:'get',
        data
    })
}
