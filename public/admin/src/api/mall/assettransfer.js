import request from '@/utils/request'
import qs from 'qs'

export function getloglist(data) {
  let params = qs.stringify(data)
  return request({
    url: `/madmin/assettransfer/log/list?${params}`,
    method: 'get'
  })
}