import request from '@/utils/request'

export function getcopyright() {

  return request({
    url: '/madmin/copyright' ,
    method: 'get'
  })
}

export function copyrightactive(data) {

  return request({
    url: '/madmin/copyright/active' ,
    method: 'post',
    data
  })
}

export function editcopyright(data) {

  return request({
    url: '/madmin/copyright' ,
    method: 'post',
    data
  })
}