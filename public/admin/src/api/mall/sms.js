import request from '@/utils/request';
//**********以下接口为短信模板模块**********
//增加短信模板
export function addTemplate(key,data){
  return request({
    url: '/madmin/sms/' + key + '/template',
    method: 'post',
    data
  });
};

//删除短信模板
export function delTemplate(key,id) {
  return request({
    url: '/madmin/sms/' + key + '/template/' + id,
    method: 'delete'
  })
}

//编辑短信模板
export function editTemplate(key,data){
  return request({
    url: '/madmin/sms/' + key + '/template/' + data.id,
    method: 'put',
    data
  });
};

//获取短信模板
export function getTemplate(key,id){
  return request({
    url: '/madmin/sms/' + key + '/template/' + id,
    method: 'get'
  });
};

//获取短信模板列表
export function getTemplateList(key,data,page,size) {
  return request({
    url: '/madmin/sms/' + key + '/template/list?page=' + page + '&size=' + size ,
    method: 'post',
    data
  });
};

//获取所有短信模板列表
export function getTemplateAll(key,scene) {
  return request({
    url: `/madmin/sms/${key}/template?scene=` + scene,
    method: 'get'
  });
};


//获取场景列表(作废)
export function getScene(key){
  return request({
    url: '/madmin/sms/' + key + '/template/scene',
    method: 'get'
  });
};


//**********以下接口为短信应用模块(废弃)**********
//获取短信发送列表
export function getSmsList(key,data,page,size) {
  return request({
    url: '/madmin/sms/' + key + '/record/list?page=' + page + '&size=' + size ,
    method: 'post',
    data
  });
};

//发送短信
export function postSms(key,data){
  return request({
    url: '/madmin/sms/' + key + '/send',
    method: 'post',
    data
  });
};

//查询短信发送记录(单条)
export function getSms(key,data){
  return request({
    url: '/madmin/sms/' + key + '/details',
    method: 'get',
    data
  });
};

//**********以下接口为微信模板模块**********
//增加微信模板
export function addTemplateWx(data){
  return request({
    url: '/madmin/sms/wechat/template',
    method: 'post',
    data
  });
};

//删除微信模板
export function delTemplateWx(id) {
  return request({
    url: '/madmin/sms/wechat/template/' + id,
    method: 'delete'
  })
}

//编辑微信模板
export function editTemplateWx(data){
  return request({
    url: '/madmin/sms/wechat/template/' + data.id,
    method: 'put',
    data
  });
};

//获取微信模板
export function getTemplateWx(id){
  return request({
    url: '/madmin/sms/wechat/template/' + id,
    method: 'get'
  });
};

//获取微信模板列表
export function getTemplateWxList(data,page,size) {
  return request({
    url: `/madmin/sms/wechat/template/list?page=${page}&size=` + size,
    method: 'post',
    data
  });
};

//获取所有微信模板列表
export function getTemplateWxAll(scene) {
  return request({
    url: `/madmin/sms/wechat/template?scene=` + scene,
    method: 'get'
  });
};

