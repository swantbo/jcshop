import request from '@/utils/request';
//**********以下接口为商户列表模块**********
//获取商户列表
export function getMerchantList(data,page,size) {
  return request({
    url: '/madmin/merchant/list?page=' + page + '&size=' + size,
    method: 'post',
    data
  });
};

//新增商户
export function addMerchant(data){
  return request({
    url: '/madmin/merchant',
    method: 'post',
    data
  });
};

//删除商户
export function delMerchant(id){
    return request({
      url: '/madmin/merchant/' + id,
      method: 'delete'
    });
};

//编辑商户
export function editMerchant(data){
  return request({
    url: '/madmin/merchant/' + data.id,
    method: 'put',
    data
  });
};

//获取商户
export function getMerchant(id){
  return request({
    url: '/madmin/merchant/' + id,
    method: 'get'
  });
};

//全局获取用法(临时)
export function getUserList(data){
  return request({
    url:'/madmin/user/list?page=1&size=999999',
    method:'post',
    data
  });
};

//**********以下接口为商户申请列表模块**********
//获取商户申请列表
export function getMerchantApplyList(data,page,size) {
  return request({
    url: '/madmin/merchant/apply/list?page=' + page + '&size=' + size,
    method: 'post',
    data
  });
};

//删除申请记录
export function delMerchantApply(id){
  return request({
    url: '/madmin/merchant/apply/' + id,
    method: 'delete'
  });
};

//更新申请记录
export function editMerchantApply(data){
  return request({
    url: '/madmin/merchant/' + data.id,
    method: 'put',
    data
  });
};

//获取申请记录数据
export function getMerchantApply(id){
  return request({
    url: '/madmin/merchant/apply/' + id,
    method: 'get'
  });
};
