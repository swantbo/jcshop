import request from '@/utils/request';
//**********以下接口为商城端文章分类功能**********
//增加文章分类
export function addArticleCate(data) {
  return request({
    url: '/madmin/article/type',
    method: 'post',
    data
  });
};

//删除文章分类
export function delArticleCate(id) {
  return request({
    url: '/madmin/article/type/' + id,
    method: 'delete'
  });
};

//更新文章分类
export function editArticleCate(data) {
  return request({
    url: '/madmin/article/type/' + data.id,
    method: 'put',
    data
  });
};

//获取文章分类列表
export function getArticleCates(data,page,size) {
  return request({
    url: '/madmin/article/type/list?page=' + page + '&size=' + size,
    method: 'post',
    data
  });
};

//获取所有文章分类
export function getArticleCateAll() {
  return request({
    url: '/madmin/article/type',
    method: 'get'
  });
};

//**********以下接口为商城端文章功能**********
//增加文章
export function addArticle(data) {
  return request({
    url: '/madmin/article',
    method: 'post',
    data
  });
};

//删除文章
export function delArticle(id) {
  return request({
    url: '/madmin/article/' + id,
    method: 'delete'
  });
};

//更新文章
export function editArticle(data) {
  return request({
    url: '/madmin/article/' + data.id,
    method: 'put',
    data
  });
};

//获取文章
export function getArticle(id) {
  return request({
    url: '/madmin/article/' + id,
    method: 'get'
  });
};

//获取文章列表
export function getArticles(data,page,size) {
  return request({
    url: '/madmin/article/list?page=' + page + '&size=' + size,
    method: 'post',
    data
  });
};