import request from '@/utils/request';
//修改积分配置
export function editintegral(data){
  return request({
    url: '/madmin/integral',
    method: 'put',
    data
  });
};

//获取积分配置
export function getintegral(){
  return request({
    url: '/madmin/integral',
    method: 'get'
  });
};
