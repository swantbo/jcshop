import request from '@/utils/request'
import qs from 'qs'
//列表
export function getgrouplist(data) {
  let params = qs.stringify(data)
  return request({
    url: `/madmin/group?${params}`,
    method: 'get'
  })
}
export function getgroup(data) {
  return request({
    url: '/madmin/group/' + data.id,
    method: 'get',
    data
  })
}
export function delgroup(data) {
  return request({
    url: '/madmin/group/' + data.id,
    method: 'delete',
  })
}
export function stopgroup(data) {
  return request({
    url: '/madmin/group/' + data.id,
    method: 'post',
  })
}
export function addgroup(data) {
  return request({
    url: '/madmin/group',
    method: 'post',
    data
  })
}
export function editgroup(data) {
  return request({
    url: '/madmin/group/' + data.id,
    method: 'put',
    data
  })
}
export function getcollagelist(data) {
   let params = qs.stringify(data)
  return request({
    url: `/madmin/collage?${params}`,
    method: 'get'
  })
}
export function getcollage(id) {
  return request({
    url: '/madmin/collage/' + id,
    method: 'get',
  })
}

export function completegroup(data) {
  return request({
    url: '/madmin/collage/' + data.id,
    method: 'post',
    data
  })
}
