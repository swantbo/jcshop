import request from '@/utils/request'
import qs from 'qs'
export function getliveroomlist(data) {
  let params = qs.stringify(data)
  return request({
    url: `/madmin/live/room/list?${params}`,
    method: 'get'
  })
}
export function addlive(data) {
  return request({
    url: '/madmin/live/room/add',
    method: 'post',
    data
  })
}
export function getlive(data) {
  return request({
    url: '/madmin/live/room/detail/' + data.id,
    method: 'get',
  })
}
export function editlive(data) {
  return request({
    url: '/madmin/live/room/edit/' + data.id,
    method: 'post',
    data
  })
}
export function syncRoom(data) {
  return request({
    url: '/madmin/live/room/syncRoom',
    method: 'post',
    data
  })
}
export function delliver(data) {
  return request({
    url: '/madmin/live/room/delete/' + data.id,
    method: 'post',
  })
}
export function updateFeedPublic(data) {
  return request({
    url: '/madmin/live/room/updateFeedPublic/' + data.id,
    method: 'post',
    data
  })
}
export function getQrcode(data) {
  return request({
    url: '/madmin/live/room/getQrcode/' + data.id,
    method: 'get',
  })
}


//*商品*//
//审核
export function addAudit(data) {
  return request({
    url: '/madmin/live/good/addAudit/' + data.id,
    method: 'post',
    data
  })
}
export function resetAudit(data) {
  return request({
    url: '/madmin/live/good/resetAudit/' + data.id,
    method: 'post',
    data
  })
}
export function repeatAudit(data) {
  return request({
    url: '/madmin/live/good/repeatAudit/' + data.id,
    method: 'post',
    data
  })
}
export function deleteAudit(data) {
  return request({
    url: '/madmin/live/good/deleteAudit/' + data.id,
    method: 'post',
    data
  })
}
export function goodlist(data) {
  let params = qs.stringify(data)
  return request({
    url: '/madmin/live/good/list?' + params,
    method: 'get',
    data
  })
}
export function updateGoods(data) {
  return request({
    url: '/madmin/live/good/updateGoods/' + data.room_goods_id,
    method: 'post',
    data
  })
}
export function changeLiveGoodStatus(data) {
  return request({
    url: '/shop/spu/changeLiveGoodStatus',
    method: 'get',
    data
  })
}
export function addRoomGoods(data) {
  return request({
    url: '/madmin/live/room/addRoomGoods/' + data.id,
    method: 'post',
    data
  })
}
export function deleteRoomGoods(data) {
  return request({
    url: '/madmin/live/room/deleteRoomGoods/' + data.id,
    method: 'post',
    data
  })
}
export function pushRoomGoods(data) {
  return request({
    url: '/madmin/live/room/pushRoomGoods/' + data.id,
    method: 'post',
    data
  })
}
export function goodsOnSale(data) {
  return request({
    url: '/madmin/live/room/goodsOnSale/' + data.id,
    method: 'post',
    data
  })
}
