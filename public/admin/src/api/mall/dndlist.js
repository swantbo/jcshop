import request from '@/utils/request';
var qs = require('qs');
//**********以下接口为装修中心调用**********
//获取商品列表
export function getProList(data, page, size) {
  return request({
    url: '/madmin/spu/list?page=' + page + '&size=' + size,
    method: 'post',
    data
  });
};

//获取商品分类列表
export function getCateList() {
  return request({
    url: '/madmin/classify',
    method: 'get'
  });
};
//获取商家商品列表
export function getcommodityList(data) {
  // data = qs.stringify(data);
  return request({
    url: '/madmin/merchant/commodity',
    method: 'post',
    data
  });
};


export function bottom_menu_list(data, page, size) {
  return request({
    url: '/madmin/fitment/bottom_menu/list?page=' + page + '&size=' + size,
    method: 'post',
    data
  });
};
///?page=:page&size=:size
//保存底部菜单
export function savebottom_menu(data) {
  return request({
    url: '/madmin/fitment/bottom_menu',
    method: 'post',
    data
  })
}


export function bottom_menudetail(data) {
  return request({
    url: '/madmin/fitment/bottom_menu/' + data.id,
    method: 'get',
  })
}

export function editbottom_menu(data) {
  return request({
    url: '/madmin/fitment/bottom_menu/' + data.id,
    method: 'put',
    data
  })
}

export function delbottom_menur(data) {
  return request({
    url: '/madmin/fitment/bottom_menu/' + data.id,
    method: 'delete',
    data
  })
}


//页面装修
//页面列表
export function pageList(data, page, size) {
  data = qs.stringify(data);
  return request({
    url: '/madmin/page/pageList?' + data,
    method: 'get',
    data
  });
};
//预览
export function pagepreview(data) {
  return request({
    url: '/madmin/page/preview?id=' + data.id,
    method: 'get',
    data
  })
}

//保存
export function pagecreate(data) {
  return request({
    url: '/madmin/page/create',
    method: 'post',
    data
  })
}
//启用or禁用
export function pageenable(data) {
  return request({
    url: '/madmin/page/enable',
    method: 'post',
    data
  })
}
export function pagebase64Up(data) {
  return request({
    url: '/madmin/page/base64Up',
    method: 'post',
    data
  })
}
export function renovationdetail(data) {
  return request({
    url: '/madmin/page/renovation?id=' + data.id,
    method: 'get',
    data
  })
}
export function editupRenovation(data) {
  return request({
    url: '/madmin/page/upRenovation',
    method: 'put',
    data
  })
}
export function saveSource(data) {
  return request({
    url: '/madmin/page/saveSource',
    method: 'put',
    data
  })
}

export function delpage(data) {
  return request({
    url: '/madmin/page/' + data.id,
    method: 'delete',
    data
  })
}

export function marketList(data) {
  return request({
    url: '/madmin/page/market?page=' + data.page + '&size=' + data.size + '&page_type=' + data.page_type + '&class_name=' + data.class_name,
    method: 'get',
    data
  });
};

export function saveTemplate(data) {
  return request({
    url: '/madmin/page/saveTemplate',
    method: 'post',
    data
  })
}

export function saveMyTemplate(data) {
  return request({
    url: '/madmin/page/saveMyTemplate',
    method: 'put',
    data
  })
}

export function myTemplateList(data) {
  return request({
    url: '/madmin/page/myTemplateList?page=' + data.page + '&size=' + data.size + '&page_type=' + data.page_type + '&class_name=' + data.class_name,
    method: 'get',
    data
  });
};

export function getMyTemplateInfo(data) {
  return request({
    url: '/madmin/page/getMyTemplateInfo?id=' + data.id,
    method: 'get',
    data
  })
}
export function getTemplateInfo(data) {
  return request({
    url: '/madmin/page/getTemplateInfo?id=' + data.id,
    method: 'get',
    data
  })
}

export function delMytemplate(data) {
  return request({
    url: '/madmin/page/delMytemplate/' + data.id,
    method: 'delete',
    data
  })
}
// 获取预览地址
export function getLinkUrl(data) {
 let  mdata = qs.stringify(data);
  return request({
    url: '/madmin/page/preview?' + mdata,
    method: 'get',
    data
  })
}
//获取启动广告
export function geadvert(data) {
  return request({
    url: '/madmin/page/advert',
    method: 'get',
    data
  })
}
//创建/编辑启动广告
export function startAdvert(data) {
  return request({
    url: '/madmin/page/startAdvert',
    method: 'post',
    data
  })
}


// 获取模板分类
export function classList(data) {
  return request({
    url: '/madmin/page/classList?page=' + data.page + '&size=' + data.size,
    method: 'GET'
  })
}
