import request from '@/utils/request';

//退款
export function orderdrawback(data){
  return request({
    url: '/madmin/order/drawback',
    method: 'post',
    data
  });
};
