import request from '@/utils/request'
import qs from 'qs'
//列表
export function getseckilllist(data) {
  let params = qs.stringify(data)
  return request({
    url: `/madmin/seckill?${params}`,
    method: 'get'
  })
}
export function getseckill(data) {
  return request({
    url: '/madmin/seckill/' + data.id,
    method: 'get',
    data
  })
}
export function delseckill(data) {
  return request({
    url: '/madmin/seckill/' + data.id,
    method: 'delete',
  })
}
export function stopseckill(data) {
  return request({
    url: '/madmin/seckill/' + data.id,
    method: 'post',
  })
}
export function addseckill(data) {
  return request({
    url: '/madmin/seckill',
    method: 'post',
    data
  })
}
export function editseckill(data) {
  return request({
    url: '/madmin/seckill/' + data.id,
    method: 'put',
    data
  })
}
export function getcollagelist(data) {
   let params = qs.stringify(data)
  return request({
    url: `/madmin/collage?${params}`,
    method: 'get'
  })
}
export function getcollage(id) {
  return request({
    url: '/madmin/collage/' + id,
    method: 'get',
  })
}

export function completeseckill(data) {
  return request({
    url: '/madmin/collage/' + data.id,
    method: 'post',
    data
  })
}
