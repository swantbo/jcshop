import request from '@/utils/request'
//**********以下接口为积分商城功能**********
//分类
export function getclassifylist(data) {
  return request({
    url: '/madmin/integral/shop/classify/list',
    method: 'post',
    data
  })
}
export function getclassifydetail(data) {
  return request({
    url: '/madmin/integral/shop/classify/' + data.id,
    method: 'get',
    data
  })
}
export function addclassify(data) {
  return request({
    url: '/madmin/integral/shop/classify',
    method: 'post',
    data
  })
}
export function editclassify(data) {
  return request({
    url: '/madmin/integral/shop/classify/' + data.id,
    method: 'put',
    data
  })
}
export function delclassify(data) {
  return request({
    url: '/madmin/integral/shop/classify/' + data.id,
    method: 'delete',
    data
  })
}
//商品列表
export function getgoodslist(data, page, size) {
  return request({
    url: '/madmin/integral/shop/goods/list?page=' + page + '&size=' + size,
    method: 'post',
    data
  })
}
export function getgoodsdetail(id) {
  return request({
    url: '/madmin/integral/shop/goods/' + id,
    method: 'get',
  })
}
export function addgoods(data) {
  return request({
    url: '/madmin/integral/shop/goods',
    method: 'post',
    data
  })
}
export function editgoods(data) {
  return request({
    url: '/madmin/integral/shop/goods/' + data.id,
    method: 'put',
    data
  })
}
export function delgoods(data) {
  return request({
    url: '/madmin/integral/shop/goods/' + data.id,
    method: 'delete',
    data
  })
}
export function getorderlist(data) {
  return request({
    url: '/madmin/integral/shop/order/list?page=' + data.page + '&size=' + data.size,
    method: 'post',
    data
  })
}
export function getorderdetail(id) {
  return request({
    url: '/madmin/integral/shop/order/' + id,
    method: 'get',
  })
}
export function orderdeliver(data) {
  return request({
    url: '/madmin/integral/shop/order/deliver/' + data.id,
    method: 'put',
    data
  })
}
export function orderdrawback(data) {
  return request({
    url: '/madmin/integral/shop/order/refund/' + data.id,
    method: 'post',
    data
  })
}
export function orderClose(data) {
  return request({
    url: '/madmin/integral/shop/order/' + data.id,
    method: 'delete',
    data
  })
}
export function updatedeliver(data) {
  return request({
    url: '/madmin/integral/shop/update/deliver/' + data.id,
    method: 'put',
    data
  })
}
