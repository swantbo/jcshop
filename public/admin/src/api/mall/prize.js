import request from '@/utils/request'

// 新增抽奖活动
export function addLottery(data) {
  return request({
    url: '/madmin/lottery/config',
    method: 'post',
    data
  })
}
//  抽奖活动列表
export function getLotteryList(data) {
  return request({
    url: `/madmin/lottery/config/list?page=${data.page}&size=${data.size}&start_time=${data.start_time}&end_time=${data.end_time}`,
    method: 'post',
    data
  })
}
//  抽奖活动详情
export function getLotteryContent(data) {
  return request({
    url: '/madmin/lottery/config/' + data.id,
    method: 'get',
    data
  })
}
//  修改抽奖活动
export function editLotteryContent(data) {
  return request({
    url: '/madmin/lottery/config/' + data.id,
    method: 'put',
    data
  })
}
//  开启或关闭活动列表
export function openLottery(data) {
  return request({
    url: '/madmin/lottery/config/open/' + data.id + '?status=' + data.status,
    method: 'post',
    data
  })
}
//  删除活动列表
export function deleteLottery(data) {
  return request({
    url: '/madmin/lottery/config/' + data.id,
    method: 'delete',
    data
  })
}

// 中奖列表
export function getLotteryLog(data) {
  return request({
      url: '/madmin/lottery/log/list',
      method: 'post',
      data
  });
};
// 中奖详情
export function getLotteryLogDetails(data) {
  return request({
      url: '/madmin/lottery/log/'+data.id,
      method: 'get',
      data
  });
};
// 中奖发货
export function getLotteryLogDeliver(data) {
  return request({
      url: '/madmin/lottery/deliver',
      method: 'post',
      data
  });
};
