import request from '@/utils/request'
export function getrulelist(token) {
  return request({
    url: '/madmin/auth/rule/value',
    method: 'get',
  })
}
export function getrolelist(page, data) {
  return request({
    url: '/madmin/auth/role/list?page=' + page + '&size=10',
    method: 'post',
    data
  })
}
export function saverole(data) {
  return request({
    url: '/madmin/auth/role',
    method: 'post',
    data
  })
}
export function getroledetail(data) {
  return request({
    url: '/madmin/auth/role/' + data.id,
    method: 'get',
  })
}
export function editrole(data) {
  return request({
    url: '/madmin/auth/role/' + data.id,
    method: 'put',
    data
  })
}
export function delrole(data) {
  return request({
    url: '/madmin/auth/role/' + data.id,
    method: 'delete',
    data
  })
}
