import request from '@/utils/request'
export function getsetting(data) {
  return request({
    url: '/madmin/configuration/WeixinAppRead',
    method: 'get',
    data
  })
}
export function WeixinAppSave(data) {
  return request({
    url: '/madmin/configuration/WeixinAppSave',
    method: 'post',
    data
  })
}

export function weixinsave(data) {
  return request({
    url: '/madmin/weixin/save',
    method: 'post',
    data
  })
}