import request from '@/utils/request'
import {
  logintype_byurl
} from '@/utils/auth'
export async function jur_addons_item(id) {
  var url = '/madmin/addons'
  if (logintype_byurl() == 'mall') {
    url = '/madmin/addons'
  } else if (logintype_byurl() == 'shop') {
    url = '/shop/addons'
  }
  let a = await request({
    url: url,
    method: 'get',
    data:{n_load:1}
  }).then(async res => {
    let addons = res.msg;
    return addons.findIndex(_ => {
      return _ == id
    }) > -1
  })
  return a
}
export async function jur_addons(data) {
  let p = {}
  if (Array.isArray(data)) {
    for (var i = 0; i < data.length; i++) {
      let c = data[i]
      let d = await jur_addons_item(c)
      p[c] = d
    }
  }
  return p
}
// export function
