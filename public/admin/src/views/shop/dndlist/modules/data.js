const type_data = {
  'btnGroup': {
    name: 'btnGroup',
    list: [{
      icon: window.location.origin + '/static/images/btns1.png',
      text: '按钮文字1',
      linkdata: {
        link: '',
        name: '',
        id: '',
        type: '',
        login_type: 1,
      },
    }, {
      icon: window.location.origin + '/static/images/btns2.png',
      text: '按钮文字2',
      linkdata: {
        link: '',
        name: '',
        id: '',
        type: '',
        login_type: 1,
      },
    }, {
      icon: window.location.origin + '/static/images/btns3.png',
      text: '按钮文字3',
      linkdata: {
        link: '',
        name: '',
        id: '',
        type: '',
        login_type: 1,
      },
    }, {
      icon: window.location.origin + '/static/images/btns4.png',
      text: '按钮文字4',
      linkdata: {
        link: '',
        name: '',
        id: '',
        type: '',
        login_type: 1,
      },
    }, {
      icon: window.location.origin + '/static/images/btns5.png',
      text: '按钮文字5',
      linkdata: {
        link: '',
        name: '',
        id: '',
        type: '',
        login_type: 1,
      },
    }, ],
    rows: 1,
    textcolor: '#565656',
    num: 5,
    type: 1,
    bgcolor: '#FFFFFF',
    width: 60,
    padding: {
      top: 16,
      lr: 24,
      bottom: 8
    },
    radius: {
      top: 10,
      bottom: 10
    },
  },
  "banner": {
    name: 'banner',
    bg_type: 1,
    bg_color: '#FFFFFF',
    bg_img: '',
    imglist: [{
      url: "",
      linkdata: {
        link: "",
        login_type: 1,
        name: "",
        id: "",
        type: 1
      }
    }, {
      url: "",
      linkdata: {
        link: "",
        login_type: 1,
        name: "",
        id: "",
        type: 1
      }
    }, ],
    type: 1,
    bg: '#FFFFFF',
    position: 'center',
    p_t: 8,
    p_lr: 12,
    p_b: 8,
    r_t: 12,
    r_b: 12,
    first_img: ''
  },
  "tabsContent": {
    name: 'tabsContent',
    currentIndex: 0,
    //tabs:[{name:'标签1'}],
    content: [{
      name: '标签1',
      type: 0,
      data: [{
        type: 0,
        typeName: '手动选择',
        limit: 20,
        config: {
          type: 'two',
          product: {
            probg: '#FFFFFF',
            titleColor: '#212121'
          },
          sell_price: {
            show: true,
            color: '#F56C6C'
          },
          line_price: {
            show: true,
            text: '原价',
            color: '#969696',
            isIg: false
          },
          sell_total: {
            show: true,
            text: '销量',
            color: '#969696'
          },
          add_button: {
            show: true,
            icon: 'el-sc-gouwuche2',
            color: '#F56C6C'
          }
        },
        list: []
      }, {
        type: 1,
        typeName: '选择分类',
        limit: 20,
        cateid: -1,
        order: 'comprehensive',
        config: {
          type: 'two',
          product: {
            probg: '#FFFFFF',
            titleColor: '#212121'
          },
          sell_price: {
            show: true,
            color: '#F56C6C'
          },
          line_price: {
            show: true,
            text: '原价',
            color: '#969696',
            isIg: false
          },
          sell_total: {
            show: true,
            text: '销量',
            color: '#969696'
          },
          add_button: {
            show: true,
            icon: 'el-icon-shopping-cart-full',
            color: '#F56C6C'
          }
        },
        list: [{
          id: 19,
          type: 1,
          name: '这是一个商品名称',
          subtitle: '这是一个商品副名称',
          master: window.location.origin + '/static/images/goods_col2.png',
          sell_price: '255.00',
          original_price: '500.00',
          sell: 0
        }]
      }, {
        type: 2,
        typeName: '选择分组',
        limit: 20,
        order: 'comprehensive',
        config: {
          type: 'two',
          product: {
            probg: '#FFFFFF',
            titleColor: '#212121'
          },
          sell_price: {
            show: true,
            color: '#F56C6C'
          },
          line_price: {
            show: true,
            text: '原价',
            color: '#969696',
            isIg: false
          },
          sell_total: {
            show: true,
            text: '销量',
            color: '#969696'
          },
          add_button: {
            show: true,
            icon: 'el-icon-shopping-cart-full',
            color: '#F56C6C'
          }
        },
        list: [{
          id: 15,
          type: 1,
          name: '这是一个商品名称',
          subtitle: '这是一个商品副名称',
          master: window.location.origin + '/static/images/goods_col2.png',
          sell_price: '255.00',
          original_price: '500.00',
          sell: 0
        }]
      }, {
        type: 3,
        typeName: '营销属性',
        markeName: 'hot',
        limit: 20,
        order: 'comprehensive',
        config: {
          type: 'two',
          product: {
            probg: '#FFFFFF',
            titleColor: '#212121'
          },
          sell_price: {
            show: true,
            color: '#F56C6C'
          },
          line_price: {
            show: true,
            text: '原价',
            color: '#969696',
            isIg: false
          },
          sell_total: {
            show: true,
            text: '销量',
            color: '#969696'
          },
          add_button: {
            show: true,
            icon: 'el-icon-shopping-cart-full',
            color: '#F56C6C'
          }
        },
        list: [{
          id: 15,
          type: 1,
          name: '这是一个商品名称',
          subtitle: '这是一个商品副名称',
          master: window.location.origin + '/static/images/goods_col2.png',
          sell_price: '255.00',
          original_price: '500.00',
          sell: 0
        }]
      }]
    }],
    config: {
      color: {
        normal: '#606266',
        active: '#303133',
      },
      marigin: {
        ic: 10,
        top: 10,
        bottom: 10,
        lr: 10
      },
      tabHeight: 30,
      radius: 10
    }
  },
  "line": {
    name: 'line',
    type: 'solid',
    color: '#E4E7ED',
    height: 8,
    margin: {
      tb: 10,
      lr: 10
    }
  },
  "blank": {
    name: 'blank',
    height: 100
  },
  "singlePics": {
    name: 'singlePics',
    imglist: [{
      url: '',
      linkdata: {
        link: '',
        name: '',
        id: '',
        type: '',
        login_type: 1,
      },
    }],
    margin: {
      top: 0,
      bottom: 0,
      lr: 0,
      pic: 0
    },
    radius: 0
  },
  "titleBar": {
    name: 'titleBar',
    titleBarType: 0,
    bar: {
      bg: "#fbda41",
      width: 5,
      height: 20
    },
    content: {
      left: '这是一个测试标题内容',
      left_icon: 'el-icon-edit-outline',
      right: '查看',
      right_icon: 'el-icon-arrow-right',
    },
    linkdata: {
      link: '',
      name: '',
      id: '',
      type: '',
      login_type: 1,
    },
    color: {
      left: '#606266',
      left_icon: '#606266',
      right: '#909399',
      right_icon: '#909399',
      bg: '#FFFFFF'
    },
    posAlign: 'flex-start',
    radius: 4,
    margin: {
      top: 0,
      bottom: 0,
      lr: 10
    }
  },
  "searchBar": {
    name: 'searchBar',
    placeholder: '请输入查询内容',
    bg_type: 1,
    bg_color: '#FFFFFF',
    bg_img: '',
    color: {
      text: '#C0C4CC',
      icon: '#C0C4CC',
      bg: '#FFFFFF',
      border: '#E4E7ED'
    },
    margin: {
      top: 0,
      bottom: 0
    },
    padding: {
      lr: 0
    },
    radius: 5
  },
  "shangpinzu": {
    name: 'shangpinzu',
    list: [],
    type: 0,
    cateid: 0,
    show_type: 'block',
    list_type: 'two',
    bg: '#FFFFFF',
    t_color: '#000000',
    limit: 20,
    p_t: 8,
    p_lr: 12,
    p_b: 8,
    r_t: 0,
    r_b: 0,
    order: "comprehensive",
    pro_price: {
      show: 1,
      color: '#FF3C29'
    },
    del_price: {
      show: 0,
      color: '#969696',
      title: ''
    },
    sales_num: {
      show: 0,
      color: '#969696',
      title: ''
    },
    add_button: {
      show: false,
      icon: 'el-sc-gouwuche2',
      color: '#F56C6C'
    }
  },
  'member': {
    name: 'member',
    avart_type: 1,
    bg_type: 1,
    bg: '#FF3C29',
    imgbg: null,
    namecolor: '#FFFFFF',
    finance: {
      btns: [0, 1, 2],
      numcolor: '#FFFFFF',
      titlecolor: '#FFFFFF',
    },
    sign: {
      show: 1,
      text: '签到赚金币',
      bg_color: '#EB4521',
      text_color: '#FFFFFF'
    },
    // level:{
    //   bgcolor: '#FFFFFF',
    //   titlecolor: '#FFFFFF'
    // },
    icon_right: {
      icon: '',
      color: '#FFFFFF'
    },
    btn: {
      show: 1,
      text_color: '#000000',
      icon_color: '#000000'
    },
    list: [{
      "icon": "el-sc-ziyuan1",
      "text": "未付款",
      "linkdata": {
        "link": "../order/list?tab=1",
        "type": 1,
        "id": 7,
        "name": "未付款订单",
        "login_type": 2
      }
    }, {
      "icon": "el-sc-daifahuo1",
      "linkdata": {
        "link": "../order/list?tab=2",
        "type": 1,
        "id": 8,
        "name": "待发货订单",
        "login_type": 2
      },
      "text": "待发货"
    }, {
      "icon": "el-sc-daishouhuo",
      "linkdata": {
        "link": "../order/list?tab=3",
        "type": 1,
        "id": 9,
        "name": "待收货订单",
        "login_type": 2
      },
      "text": "待收货"
    }, {
      "icon": "el-sc-pingjia-tianchong",
      "linkdata": {
        "link": "../order/list?tab=5",
        "type": 1,
        "id": 10,
        "name": "待评价订单",
        "login_type": 2
      },
      "text": "待评价"
    }, {
      "icon": "el-sc-wenzhang-copy",
      "linkdata": {
        "link": "../order/list",
        "type": 1,
        "id": 6,
        "name": "订单列表",
        "login_type": 2
      },
      "text": "我的订单"
    }]
  },
  'bindmobile': {
    name: 'bindmobile',
    title: {
      text: '绑定手机号，同步全渠道订单和优惠卷',
      color: '#212121',
    },
    icon: {
      url: '',
      color: "#FF3C29"
    },
    btn: {
      color: '#FF3C29'
    },
    padding: {
      top: 16,
      lr: 24,
      bottom: 8
    },
    radius: {
      top: 10,
      bottom: 10
    }
  },
  'listmenu': {
    name: 'listmenu',
    left: {
      iconcolor: '#212121',
      textcolor: '#212121'
    },
    right: {
      iconcolor: '#969696',
      textcolor: '#969696',
      icon: ''
    },
    bgcolor: '#FFFFFF',
    padding: {
      top: 16,
      lr: 24,
      bottom: 8
    },
    radius: {
      top: 10,
      bottom: 10
    },
    list: [{
      icon: '',
      linkdata: {
        link: '',
        name: '',
        id: '',
        type: '',
        login_type: 1,
      },
      right_text: '',
      left_text: '文字1',
    }],
  },
  'icongroup': {
    name: 'icongroup',
    list: [{
      icon: '',
      text: '按钮文字1',
      linkdata: {
        link: '',
        name: '',
        id: '',
        type: '',
        login_type: 1,
      },
    }, ],
    iconcolor: '#565656',
    textcolor: '#565656',
    num: 3,
    bgcolor: '#FFFFFF',
    padding: {
      top: 16,
      lr: 24,
      bottom: 8
    },
    radius: {
      top: 10,
      bottom: 10
    },
  },
  'detail_banner': {
    name: 'detail_banner',
    imglist: [{
      url: ""
    }, {
      url: ""
    }, ],
    type: 1,
    bg: '#FFFFFF',
    position: 'center',
  },
  'detail_info': {
    name: 'detail_info',
    padding: {
      top: 0,
      bottom: 20
    },
    showaddr:0,
    showsales:0,
    color: {
      bg: '#FFFFFF',
      title: '#212121',
      desc: '#565656',
      price: '#FF3C29',
      del_price: '#969696',
      express: '#565656',
      sales: '#565656',
      addr: '#565656'
    },
    share: {
      show: 1,
      text: '分享',
      icon: 'el-sc-fenxiang_2',
      color: '#666666'
    },
    collect: {
      show: 1,
      text: '收藏',
      icon: 'el-sc-shoucang11',
      color: '#E3713E',
      bfcolor: '#666666',
    },
    commission: {
      show: 1,
      text: '',
      icon: '',
    },
    memberprice: {
      show: 1,
      bg_color: '#F5F5F5',
      text_color: '#565656',
      text: '成为会员，享受会员价',
      price_color: '#FF3C29'
    }
  },
  'detail_sale': {
    name: 'detail_sale',
    color: {
      title: '#969696',
      text: '#212121',
      coupon: "#FF3C29",
      bg: '#FFFFFF'
    },
    active: {
      labelcolor: '#FF3C29',
      textcolor: '#FF3C29'
    },
    service: {
      labelcolor: '#FF3C29',
      textcolor: '#FF3C29'
    },
    padding: {
      top: 0,
      bottom: 20
    },
  },
  'detail_comment': {
    name: 'detail_comment',
    padding: {
      top: 0,
      bottom: 20
    },
    color: {
      title: "#212121",
      more: "#969696",
      username: "#212121",
      start: "#FF3C29",
      evaluate: "#212121",
      sku: "#969696",
      bg: "#FFFFFF",
    }
  },
  'advs': {
    name: 'advs',
    imglist: [{
      url: '',
      linkdata: {
        link: '',
        name: '',
        id: '',
        type: ''
      }
    }],
    showtime: 1,
    stime: 0,
    type: 0,
    showtype: 'top',
    bg: '#000000',
    opacity: 40,
    time: 3,
    c_btns: true
  },
  'detail_btns': {
    name: 'detail_btns',
    color: {
      bg: '#FFFFFF',
      icon: '#969696',
      text: '#969696',
      btn_l: '#212121',
      btn_r: '#FF3C29',
      badge: '#FF3C29'
    },
    btnlist: [{
      text: '加入购物车',
      type: 1,
      linkdata: {},
      show: 1
    }, {
      text: '立即购买',
      type: 2,
      linkdata: {},
      show: 1
    }],
    list: [{
      icon: "el-sc-shouye",
      linkdata: {
        link: "../index/index",
        type: 1,
        id: 1,
        name: "商城首页",
        login_type: 1
      },
      text: "首页"
    }, {
      icon: "el-sc-cuo1",
      text: "客服",
      linkdata: {
        link: "../customer/customer",
        type: 1,
        id: 25,
        name: "腾讯智服",
        login_type: 1
      }
    }, {
      icon: "el-sc-gouwuche3",
      text: "购物车",
      linkdata: {
        link: "../cart/cart",
        type: 1,
        id: 5,
        name: "购物车",
        login_type: 2
      }
    }, ]
  },
  'detail_pullup': {
    name: 'detail_pullup',
    bg: "#FFFFFF",
    mt: 0
  },
  'cube': {
    name: 'cube',
    type: 0,
    checklist: [{
      width: 50,
      height: 100,
      x: 0,
      y: 0,
      img: '',
      linkdata: {
        link: '',
        name: '',
        id: '',
        type: ''
      }
    }, {
      width: 50,
      height: 100,
      x: 50,
      y: 0,
      img: '',
      linkdata: {
        link: '',
        name: '',
        id: '',
        type: ''
      }
    }],
    height: 200,
    padding: {
      between: 0,
      top: 0,
      bottom: 0,
      lr: 0
    },
    r_t: 0,
    r_b: 0
  },
  'hot_area': {
    name: 'hot_area',
    img: '',
    list: [],
    padding: {
      top: 16,
      lr: 24,
      bottom: 8
    },
    radius: {
      top: 10,
      bottom: 10
    },
  },
  'layer': {
    width: 80,
    name: 'layer',
    link: {},
    img: '',
    position: 'rb',
    deviationa: 50,
    deviationb: 30
  },
  'danmu': {
    width: 80,
    name: 'danmu',
    type: 1,
    icon: 'el-icon-arrow-up',
    link: {},
    img: '',
    position: 'rb',
    deviationa: 50,
    deviationb: 30
  },
  'coupon': {
    list: [],
    textcolor: '#ffffff',
    bgcolor: '#D9BC8B',
    show_type: 'scroll',
    name: 'coupon',
    bg_div_color: '#ffffff'
  },
  'merchgroup': {
    name: 'merchgroup',
    list_type: 'product',
    show_type: '1',
    bg_color: '#FFFFFF',
    num_color: '#969696',
    title_color: '#212121',
    pr_color: '#212121',
    price_color: '#FF3C29',
    shownum: true,
    p_t: 0,
    p_lr: 0,
    p_b: 0,
    r_t: 12,
    r_b: 12,
    b_t: 20,
    logo_type: 'square',
    btn: {
      type: 1,
      title: '',
      titlecolor1: '#FF3C29',
      titlecolor2: '#FFFFFF',
      color: '#FF3C29',
      rd: 30,
      icon: 'el-sc-xiangyou',
      icon_color: '#FF3C29'
    },
    list: []
  },
  'notice': {
    name: 'notice',
    icon: 'el-sc-gonggao11',
    type: '1',
    num: '5',
    p_t: 8,
    p_lr: 12,
    p_b: 8,
    r_t: 0,
    r_b: 0,
    text_color: '#212121',
    bg_color: '#FFFFFF',
    icon_color: '#333333',
    interval: 3,
    list: [{
      title: '这是第一条公告',
      linkdata: {
        link: '',
        name: '',
        id: '',
        type: '',
        login_type: 1,
      },
    }, {
      title: '这是第二条公告',
      linkdata: {
        link: '',
        name: '',
        id: '',
        type: '',
        login_type: 1,
      },
    }]
  },
  'richtext': {
    name: 'richtext',
    p_t: 8,
    p_lr: 12,
    p_b: 8,
    r_t: 12,
    r_b: 12,
    bg_color: '#FFFFFF',
    p_i: 8
  },
  'merchshop': {
    name: 'merchshop',
    bg_color: '#FFFFFF',
    num_color: '#969696',
    title_color: '#212121',
    shownum: true,
    p_t: 8,
    p_b: 8,
    logo_type: 'square',
    btn: {
      type: 1,
      title: '',
      titlecolor1: '#FF3C29',
      titlecolor2: '#FFFFFF',
      color: '#FF3C29',
      rd: 30,
      icon: 'el-sc-xiangyou',
      icon_color: '#FF3C29'
    }
  },
  'diyform': {
    name: 'diyform',
    p_t: 8,
    p_lr: 12,
    p_b: 8,
    r_t: 12,
    r_b: 12,
    id: '',
    title: '',
    list: [{
      "icon": "el-sc-danhangwenben1",
      "name": "单行文本",
      "type": "text",
      "active": false,
      "params": {
        "tip": "请输入",
        "title": "单行文本",
        "value": "",
        "explain": "",
        "required": true
      },
      "visible": false
    }, ],
    input_value: '提交',
    input_color: '#FF3C29'
  },
  "toptabs": {
    name: "toptabs",
    bg_color: '#FFFFFF',
    text_color: "#212121",
    active_bg_color: '#FFFFFF',
    active_text_color: "#FF3C29",
    list: [{
      name: '首页',
      linkdata: {
        link: '../index/index',
        name: '商城首页',
        id: 1,
        type: 1,
        login_type: 1,
      }
    }, {
      name: '选项2',
      linkdata: {
        link: '',
        name: '',
        id: '',
        type: '',
        login_type: 1,
      }
    }, {
      name: '选项3',
      linkdata: {
        link: '',
        name: '',
        id: '',
        type: '',
        login_type: 1,
      }
    }, {
      name: '选项4',
      linkdata: {
        link: '',
        name: '',
        id: '',
        type: '',
        login_type: 1,
      }
    }, {
      name: '选项5',
      linkdata: {
        link: '',
        name: '',
        id: '',
        type: '',
        login_type: 1,
      }
    }, ]
  },
  "follow": {
    name: 'follow',
    radio: 1,
    content: {
      left: '欢迎访问商城，点击关注我们哦~',
      left_icon: 'el-icon-edit-outline',
      right: '点击关注',
      right_icon: 'el-sc-dianpu3',
    },
    linkdata: {
      link: '',
      name: '',
      id: '',
      type: '',
      login_type: 1,
    },
    color: {
      left: '#606266',
      right: '#fff',
      right_icon: '#fff',
      bg: '#FFFFFF',
      right_buttom_icon: '#000',
      right_buttom_bg: '#000'
    },
    posAlign: 'space-between',
    radius: {
      template_radius: 4,
      image_radius: 4,
      button_radius: 4
    },
    margin: {
      top: 0,
      bottom: 0,
      lr: 10
    },
    buttomRadio: 1,
    img: '',
    linkimg: ''
  },
  "seckill": {
    name: 'seckill',
    selectdata:{

    },
    list: [],
    type: 0,
    seckillType: 1,
    limit: 20,
    timer: [],
    content: {
      title: '限时秒杀',
      icon: 'el-sc-APPtubiao-',
    },
    color: {
      head_bg: '#ff3c29',
      commodity_bg: '#fff',
      commodity_title_color: '#000',
      commodity_price_color: '#ff3c29',
      head_icon_color: '#fff',
      head_txt_color: '#fff',
      head_more_color: '#fff',
      time_text_color: '#000000',
      time_num_color: '#FFFFFF',
      time_bg_color: '#FF6F29',
      bg: '#f00',
      price_title_bg: '#ff3c29',
      price_title_color: '#fff',
      old_price_title_color: '#999'
    },
    showMore: 'block',
    showoldPrice: 'block',
    showTime: 'block',
    radius: {
      template_radius: 4,
      image_radius: 4,
      button_radius: 4
    },
    margin: {
      top: 10,
      bottom: 10,
      lr: 20
    },
  },
  "presell": {
    name: 'presell',
    selectdata:{

    },
    list: [],
    type: 0,
    presellType: 1,
    limit: 20,
    timer: [],
    content: {
      title: '商品预售',
      icon: 'el-sc-APPtubiao-',
    },
    color: {
      head_bg: '#ff3c29',
      commodity_bg: '#fff',
      commodity_title_color: '#000',
      commodity_price_color: '#ff3c29',
      head_icon_color: '#fff',
      head_txt_color: '#fff',
      head_more_color: '#fff',
      time_text_color: '#000000',
      time_num_color: '#FFFFFF',
      time_bg_color: '#FF6F29',
      bg: '#f00',
      price_title_bg: '#ff3c29',
      price_title_color: '#fff',
      old_price_title_color: '#999'
    },
    showMore: 'block',
    showoldPrice: 'block',
    showTime: 'block',
    radius: {
      template_radius: 4,
      image_radius: 4,
      button_radius: 4
    },
    margin: {
      top: 10,
      bottom: 10,
      lr: 20
    },
  },

  "vipmember": {
    name: 'vipmember',
    avart_img: "https://cube.elemecdn.com/3/7c/3ea6beec64369c2642b92c6726f1epng.png",
    type: {
      avart_type: 1,
      card_type: 1,
      card_type_open: 1
    },
    card_img: "",
    card_img_open: "",
    is_open: 1,
    color: {
      bg_color: '#41444e',
      progress_color: '#846D4D',
      card_bg_color_f: "#a98550",
      card_bg_color_l: "#e2cc9d",
      card_num_color: '#fff',
      card_font_color: "#f9efd0",
      card_rule_color: "#52555e",
      card_rule_btn_color: '#f7eecf',
      card_round_color_f: "#C4A674",
      card_round_color_l: "#D9C191",
      card_grade_color: "#FFFFFF",
      card_bg_color_f_open: "#a98550",
      card_bg_color_l_open: "#e2cc9d",
      card_grade_color_open: "#FFFFFF",
    }
  },
  "vipshangpinzu": {
    name: 'vipshangpinzu',
    list: [],
    type: 0,
    cateid: 0,
    show_type: 'block',
    list_type: 'two',
    bg: '#FFFFFF',
    t_color: '#000000',
    limit: 20,
    p_t: 8,
    p_lr: 12,
    p_b: 8,
    r_t: 0,
    r_b: 0,
    order: "comprehensive",
    pro_price: {
      show: 1,
      color: '#FF3C29'
    },
    del_price: {
      show: 0,
      color: '#969696',
      title: ''
    },
    vip_price: {
      show: 0,
    },
    svip_price: {
      show: 0,
    },
    sales_num: {
      show: 0,
      color: '#969696',
      title: ''
    },
    add_button: {
      show: false,
      icon: 'el-sc-gouwuche2',
      color: '#F56C6C'
    }
  },
  "vipinterests": {
    name: 'vipinterests',
    style_type: 1,
    card_font_size: 16,
    title: {
      color: "#000",
      size: 16
    },
    currentIndex: 0,
    list: [
      [{
        icon: window.location.origin + '/static/images/btns1.png',
        text: '权益文字1',
        card_bg: '#019CFE',
        card_color: '#fff',
        linkdata: {
          link: '',
          name: '',
          id: '',
          type: '',
          login_type: 1,
        },
      }]
    ],
    rows: 1,
    textcolor: '#565656',
    num: 2,
    type: 1,
    bgcolor: '#FFFFFF',
    content_bgcolor: "#fff",
    width: 60,
    padding: {
      top: 16,
      lr: 24,
      bottom: 8
    },
    content_padding: {
      top: 16,
      lr: 24,
      bottom: 8
    },
    radius: {
      top: 10,
      bottom: 10
    },
  },
  'groupon': {
    name: 'groupon',
    list: [],
    selectdata:{

    },
    type: 0,
    seckillType: 1,
    limit: 20,
    timer: [],
    content: {
      title: '限时拼团',
      icon: 'el-sc-APPtubiao-',
    },
    color: {
      head_bg: '#ff3c29',
      commodity_bg: '#fff',
      commodity_title_color: '#000',
      commodity_price_color: '#ff3c29',
      head_icon_color: '#fff',
      head_txt_color: '#000000',
      head_more_color: '#ff3c29',
      time_text_color: '#000000',
      time_num_color: '#FFFFFF',
      time_bg_color: '#FF6F29',
      bg: '#ffffff',
      price_title_bg: '#ff3c29',
      price_title_color: '#fff',
      old_price_title_color: '#999'
    },
    commodity_price_title: '拼团价',
    old_price_title: "单买价",
    showMore: 'block',
    showoldPrice: 'block',
    showTime: 'block',
    showsell: 1,
    radius: {
      template_radius: 4,
      image_radius: 4,
      button_radius: 4
    },
    margin: {
      top: 10,
      bottom: 10,
      lr: 20
    },
    btns:{
      show:1,
      color:'#FFFFFF',
      bg:'#ff3c29',
      text:'马上抢'
    }
  },
  "liver": {
    name: 'liver',
    type: 'item_one',
    list:[],
    showtype:2,
    color: {
      bg:"#FFFFFF"
    },
    margin: {
      top: 16,
      lr: 24,
      bottom: 8
    },
    radius: {
      top: 10,
      bottom: 10
    },
  },
  "video":{
    name: 'video',
    type: 1,
    list:[],
    url:'',
    bg:'',
    margin: {
      top: 16,
      lr: 24,
      bottom: 8
    },
    radius: {
      top: 10,
      bottom: 10
    },
  }
}
export default type_data
