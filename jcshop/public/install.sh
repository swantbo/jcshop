#!/usr/bin/bash
cd [ROOT_PATH] &&
cd .. &&
chmod 777 ./public &&
chmod 777 ./runtime &&
cd public &&
> conf &&
chmod 777 conf &&
crontab -l 2>&1 | tee conf && echo -e \
"\n
1 0 * * * cd [PROJECT_PATH] && php think user_get_integral \n
0 0 * * * cd [PROJECT_PATH] && php think  commodity_fraction \n
2 0 * * * cd [PROJECT_PATH] && php think  agent_upgrade \n
0 0 * * * cd [PROJECT_PATH] && php think  order_end \n
0 0 * * * cd [PROJECT_PATH] && php think  coupon_be_overdue \n
0 12 * * * cd [PROJECT_PATH] && php think  receiving \n
59 23 * * * cd [PROJECT_PATH] && php think file_to_database \n
1 0 * * * cd [PROJECT_PATH] && php think agent_get_money \n
*/20 * * * * cd [PROJECT_PATH] && php think  cancel \n
*/1 * * * * cd [PROJECT_PATH] && php think group \n
" >> conf && crontab conf && rm -f conf &&
/bin/systemctl restart crond.service &&
rm -f [ROOT_PATH]/install.sh