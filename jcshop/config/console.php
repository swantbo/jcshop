<?php
// +----------------------------------------------------------------------
// | 控制台配置
// +----------------------------------------------------------------------
return [
    // 指令定义
    'commands' => [
        'commodity_fraction' => app\command\CommodityFraction::class,
        'agent_upgrade' => app\command\AgentUpgrade::class,
        'order_end' => app\command\OrderEnd::class,
        'total' => app\command\Total::class,
        'agent_get_money' => app\command\AgentGetMoney::class,
        'file_to_database' => app\command\FileToDatabase::class,
        'user_get_integral' => app\command\UserGetIntegral::class,
        'coupon_be_overdue' => app\command\couponBeOverdue::class,
        'receiving' => 'app\command\Receiving',
        'cancel' => 'app\command\Cancel',
        'test' => 'app\command\Test',
        'coupon' => 'app\command\Coupon',
        'group' => 'app\command\Group',
        'build_addons'=>app\command\BuildAddons::class,
        'remove_addons'=>app\command\RemoveAddons::class
    ],
];
