<?php

return [
    "订单支付通知" => [
        "商城名称" => "mall_name",
        "商品名称" => "commodity_name",
        "粉丝昵称" => "user_name",
        "订单号" => "order_no",
        "支付方式" => "pay_type",
        "订单金额" => "order_cash",
        "运费" => "ft_price",
        "购买者姓名" => "buyer_name",
        "购买者电话" => "buyer_mobile",
        "收货地址" => "buyer_address",
        "下单时间" => "order_create_time",
    ],
    "订单发货通知" => [
        "商城名称" => "mall_name",
        "商品名称" => "commodity_name",
        "订单号" => "order_no",
        "快递公司" => "express_name",
        "快递单号" => "express_no",
        "收货信息" => "buyer_info",//收件人 手机 地址
        "下单时间" => "order_create_time",
        "发货时间" => "order_sell_time",
    ],
    "订单手动退款通知" => [
        "商城名称" => "mall_name",
        "商品名称" => "commodity_refund_name",
        "订单号" => "order_no",
        "购买时间" => "order_create_time",
        "退款金额" => "refund_cash",
        "退款路径" => "refund_type",
        "退款时间" => "refund_time",
    ],
    "提现成功通知" => [
        "商城名称" => "mall_name",
        "提现金额" => "withdraw_cash",
        "提现时间" => "withdraw_time",
    ],
    "充值成功通知" => [
        "商城名称" => "mall_name",
        "充值账户" => "recharge_user",
        "支付方式" => "recharge_type",
        "充值金额" => "recharge_cash",
        "充值时间" => "recharge_time",
        "赠送金额" => "recharge_presenter",
        "实际到账" => "recharge_reality_cash",
    ],
    "积分变更通知" => [
        "商城名称" => "mall_name",
        "账户" => "integral_user",
        "积分变动" => "integral_change",
        "积分余额" => "integral_residue",
        "总积分" => "integral_total",
        "变动时间" => "integral_time",
        "变动原因" => "integral_reason",
    ],
    "优惠券发放通知" => [
        "商城名称" => "mall_name",
        "账户" => "coupon_user",
        "优惠券" => "coupon_name",
        "优惠券有效期" => "coupon_validity",
        "优惠券发放时间" => "coupon_time",
    ],
    "用户注册" => [
        "验证码" => "verification_code",
    ],
    "找回密码" => [
        "验证码" => "verification_code",
    ],
    "用户登录" => [
        "验证码" => "verification_code",
    ],
    "用户进行绑定手机号时发送" => [
        "验证码" => "verification_code",
    ],
    "订单付款通知" => [
        "订单号" => "order_no",
        "商品信息" => "order_commodity",
        "订单金额" => "order_cash",
        "运费" => "ft_price",
        "购买者姓名" => "buyer_name",
        "购买者电话" => "buyer_mobile",
        "收货地址" => "buyer_address",
        "下单时间" => "order_create_time",
    ],
    "订单收货通知" => [
        "粉丝昵称" => "user_name",
        "订单号" => "order_no",
        "订单金额" => "order_cash",
        "下单时间" => "order_create_time",
    ],
    "订单售后通知" => [
        "粉丝昵称" => "user_name",
        "订单号" => "order_no",
        "维权单号" => "safeguard_no",
        "维权类型" => "safeguard_type",
        "维权金额" => "safeguard_cash",
        "维权时间" => "safeguard_time",
    ]
];










//return [
//    "订单"=>[
//        "订单信息"=>[
//            'flag'=>1,
//            "商城名称"=>"merchant_name",
//            "用户"=>"user_name",
//            "金额"=>"cash"
//        ],
//        "售后相关"=>[
//            'flag'=>1,
//        ],
//        "订单状态更新"=>[
//            'flag'=>1,
//            "旧状态"=>"status"
//        ]
//    ],
//    "用户"=>[
//        'flag'=>1,
//        "用户名"=>"name",
//        "手机"=>"mobile",
//        "状态"=>"status"
//    ]
//];