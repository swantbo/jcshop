<?php

return [
    "订单支付通知" => [
        1 => "vF-JTs4y4AlMoci3-cTy5ERkIbLbgPzemyWDDLDvrOw",
        2 => "GN5r_0gUP47_oKFXb2J1KWO0YjkeQ3xYD--NtyCXHxQ",
    ],
    "订单发货通知" => [
        1 => "BSV34l5Ua4a-ZaDi0jgngNooBsWzv-bPBMrgtzC7rAg",
        2 => "ntjlzWTiABpgyCban0tF5-cmdXSCNv2Qkuz5WP17E-w",
    ],
    "订单手动退款通知" => [
        1 => "QEDa1ICUH2Wo6-h-v_lt1g5LJ0v67v5J_0Gt5BMvWu0",
    ],
    "提现成功通知" => [
        1 => "CBPVrs50OGaW6cF4_smmXnKiWpEIZsNqjcnV1vb1Npk",
        2 => "CPj6A4sozEtjxbWrOkT8_NhgiWFHXLiTyoJI9sx0K7o",
    ],
    "充值成功通知" => [
        1 => "bO10CTN8TisyeajYEe135VxHQP4cIPGVrmFwqmyLprc",
        2 => "z4SZvKIsDc7vieVx3SAaLzXlRbjVGa1WRYcZ2VYh9gc",
    ],
    "积分变更通知" => [
        1 => "3R5DYi4tF-2PNf2bh8p7TsLkPZRdHY4Qz36FggiUjFI",
    ],
    "优惠券发放通知" => [
        1 => "vtk_r0q64OnDGWyILLylcstHX6bcg_7GN8vYjVBYRfE",
    ],
    "订单付款通知" => [
        1 => "vrUDMlgvZQeGBzaFOkDEQ_BW7EvY7BIJzcmfuzWzV_g",
    ],
    "订单收货通知" => [
        1 => "S2gMCMjvb2yGdBLWyqxrUkVG7DbudctXO1QlVsTob3c",
    ],
    "订单售后通知" => [
        1 => "ASQIH4nUs4Z-ilB23o2CH-lAbZQe-roPEbRX1ACmHGM",
    ],
];