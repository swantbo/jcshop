<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */

use think\facade\Env;
use think\facade\Db;

function now()
{
    return date('Y-m-d H:i:s');
}

function createToken()
{
    $str = md5(uniqid(md5(microtime(true)), true));  //生成一个不会重复的字符串
    $str = sha1($str);  //加密
    return $str;
}

function checkRedisKey($key)
{
    global $mid;
    global $m_id;
    //$m_id 专门用来解决遗留BUG 比如shop端或mall端调用index端
    if(!empty($m_id)){
        $mall_id=$m_id;
    }else{
        $mall_id=$mid;
    }
    return "MALL:" . $mall_id . ":" . $key;
}

/**
 * 时间Y-m-d H:i:s -> Y/m/d H:i:s
 * @param $arr array 时间数组
 * @return array
 */
function changTime($arr)
{
    $arr = is_array($arr) ? $arr : $arr->toArray();
    foreach ($arr as $key => $value) {
        if (!is_array($value) && strtotime($value)) {
            $arr[$key] = str_ireplace('-', '/', $value);
        }
    }
    return $arr;
}

/**
 * 取数组的区间
 * @param $array
 * @return array
 */
function getRangeArr($array)
{
    $max = $array[0];
    $min = $array[0];
    for ($i = 1; $i < count($array); $i++) {
        if ($array[$i] > $max)
            $max = $array[$i];
        if ($array[$i] < $min)
            $min = $array[$i];
    }
    $array = [$min, $max];
    if ($min == $max) {
        $array = [$min];
    }
    return $array;
}

function checkAddons($id)
{
    $lessee = json_decode(Db::name("lessee")->where(['status' => 1])->value('addons'));
    if (is_array($lessee) && in_array($id, $lessee)) {
        return true;
    }
    return false;
}

