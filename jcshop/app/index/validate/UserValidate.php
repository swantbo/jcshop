<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types = 1);

namespace app\index\validate;

use think\Validate;

class UserValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
	protected $rule = [
        'name'          => 'require|length:2,16|chs',
        'mobile'        => 'require|mobile',
        'encryptedData' => 'require',
        'iv'            => 'require',
        'user_id '      => 'require',
        'password'      => 'require',
        'mall_id'       => 'require',
        'code'          => 'require',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [
        'name.require'              => '姓名必须填写',
        'name.length'               => '账号只能有2-16个字符组成',
        'name.alphaDash'            => '账号只能使用汉字',
        'mobile.require'            => '手机号必填',
        'mobile.mobile'             => '请输入正确的手机号',
        'encryptedData.require'     => '加密字段必传',
        'iv.require'                => '必传',
        'user_id.require'           => '必传',
        'password.require'          => '密码不能为空',
    ];

    protected $scene = [
        'saveUserInfo'   => ['user_id'],
        'userRegister' => ['mall_id', 'code', 'mobile'],
        'userLogin' => ['mall_id', 'mobile', 'password'],
    ];

}
