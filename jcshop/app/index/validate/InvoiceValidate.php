<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\validate;


use app\index\model\Order;
use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\Validate;

class InvoiceValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'    =>    ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'order_id' => 'require|number',
        'address' => 'require',
        'inoice_type' => 'require',
        'rise' => 'require',
        'phone' => 'require|mobile',
        'deposit_bank' => 'require',
        'bank_account' => 'require|number',
        'remake' => 'max:250'
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'    =>    '错误信息'
     *
     * @var array
     */
    protected $message = [
        'order_id.require' => '字段order_id必须传',
        'order_id.number' => 'order_id只能是数字',
        'address.requrie' => '地址必须传',
        'phone.requrie' => '手机号码必须传',
        'phone.mobile' => '手机号码格式不对',
        'deposit_bank.requrie' => '开户银行必须传',
        'bank_account.number' => '银行账户必须是数字',
        'bank_account.requrie' => '银行账户必须传',
        'remake.max' => '最大的长度不能超过250个字'
    ];

    protected $scene = [
        'create' => ['order_id', 'address', 'phone','remake'],
    ];

}