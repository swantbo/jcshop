<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\validate;


use think\Validate;

class AgentWithdrawValidate extends Validate
{
    protected $rule = [
        'page' => 'require|integer',
        'size' => 'require|integer',
        'amount' => 'require|number|egt:0.01',
        //'withdraw' => 'require|float|egt:100',
        //'type' => 'require|number|in:3,4',
        'card.name' => 'require',
        'card.bank_no' => 'require'
    ];
    protected $message = [
        'id' => '必传',
        'page' => '页数未传或不为整数',
        'size' => '每页数量未传或不未整数',
        'amount' => '金额必须上传且大于0.01',
        //'withdraw' => '提现金额必须上传且大于100',
        //'type' => '类型错误',
        'card.name' => '持卡人姓名或者支付宝用户名称未填写',
        'card.bank_no' => '银行卡号或者支付宝账号未填写'
    ];
    protected $scene = [
        'findAll' => ['page','size'],
        'read' => ['id'],
        'save' => ['amount'],
        'apply' => ['card']
    ];
}