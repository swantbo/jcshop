<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\validate;


use think\Validate;

class IntegralValidate extends Validate
{
    protected $rule = [
        'start_time' => 'dateFormat:Y-m-d H:i:s',
        'end_time' => 'dateFormat:Y-m-d H:i:s',
        'commodity' => 'require',
        'total_price' => 'require',
        'coupon' => 'array'
    ];
    protected $message = [
        'start_time.dateFormat' => '开始时间格式错误',
        'end_time.dateFormat' => '结束时间格式错误',
        'commodity.require' => '商品必选提交',
        'total_price.require' => '总金额必选上传',
        'coupon.array' => '优惠券参数错误'
    ];
    protected $scene = [
        'list' => ['start_time','end_time'],
        'judge' => ['commodity','total_price','coupon']
    ];
}