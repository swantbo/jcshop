<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\validate;

use think\Validate;

class PaymentValidate extends Validate
{
    protected $rule = [
        'type|支付类型' => 'require|in:1,2',
        'orderList|订单' => 'require',
        'coupon|优惠券' => 'integer',
        'money|充值金额' => 'require|float'
    ];

    protected $message = [

    ];

    protected $scene = [

        'pay' => [
            'type',
            'orderList',
            'coupon'
        ],
        'recharge' => [
            'money'
        ]
    ];

}