<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\validate;


use think\Validate;

class MerchantApplyValidate extends Validate
{
    protected $rule = [
        'logo|logo图' => 'require',
        'name|名称' => 'require',
        'mobile|手机' => 'require',
        'business_license|营业执照' => 'require',
    ];
    
    protected $scene = [
        'save' => ['logo','name','mobile','business_license']
    ];
}