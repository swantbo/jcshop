<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\model;


use app\SearchModel;
use think\Model;
use think\model\concern\SoftDelete;

class OrderCommodityAttach extends SearchModel
{

    //use SoftDelete;
    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (isset($data['status'])) {
            self::append(['status_text']);
        }
    }

    public function getStatusTextAttr($value, $data)
    {
        //订单状态: 1-待支付 2-待发货 3-已发货 4-已完结 5-已评价 6-删除 7-已收货 8-维权中 9-已维权 10-维权驳回 11-已关闭
        $status = [
            1 => '待支付',
            2 => '待发货',
            3 => '已发货',
            4 => '已完结',
            5 => '已评价',
            6 => '删除',
            7 => '已收货',
            8 => '维权中',
            9 => '已维权',
            10 => '维权驳回',
            11 => '已关闭'
        ];
        if (!empty($data['store_id']) && $data['status'] == 2){
            unset($status[2]);
            $status[2] = "待核销";
        }
        return $status[$data['status']];
    }

    public function commodity()
    {
        return $this->hasOne(Commodity::class, 'id', 'commodity_id');
    }

    public function commodityHidden()
    {
        return $this->hasOne(Commodity::class, 'id', 'commodity_id')
            ->bind(['classify_id']);
    }

}