<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\model\view;


use app\index\model\Store;

class AgentStatistics extends \app\SearchModel
{

    protected $table = "jiecheng_agent_statistics";

    public function getSumFigureAttr($value){
        return sprintf('%.2f',$value);
    }

}