<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\model;

use think\facade\Db;
use think\Model;
use think\model\concern\SoftDelete;

use app\index\model\Collage;
use app\index\model\Group;
use app\index\model\GroupCommodity;
use app\index\model\GroupCommoditySku;


class Commodity extends BaseModel
{
    use SoftDelete;

    protected $json = ['label'];

    protected $jsonAssoc = true;

    public function getTypeAttr(int $name)
    {
        //0:实体商品,1:虚拟商品
        $type = [0 => "实体商品", 1 => "虚拟商品"];
        return $type[$name];
    }
    public function withTrashedData(bool $withTrashed)
    {
        $this->withTrashed = $withTrashed;
        return $this;
    }
    public function classify()
    {
        //'name'=>'classify_name'  属性别名不生效???
        return $this->belongsTo('Classify', 'classify_id', 'id')->bind(['name']);
    }

    //模板
    public function ft()
    {
        return $this->belongsTo('Ft', 'ft_id', 'id');
    }

    public function sku()
    {
        return $this->hasMany(Sku::class, 'commodity_id', 'id');
    }

    public function merchant()
    {
        return $this->belongsTo('Merchant', 'merchant_id', 'id')->visible(['name', 'mobile', 'company','logo','is_self']);
    }

    public function evaluate()
    {
        return $this->hasMany('OrderEvaluate','commodity_id','id')
            ->where('state',2)
            ->hidden(['order_id','remake','update_time','delete_time']);
    }
    //form
    public function formTemplate(){
        return $this->hasOne(OrderFormTemplate::class,'id','template_id')->hidden(['create_time','delete_time','update_time'])->where(['status'=>1]);
    }

    /**
     * 是否参与拼团
     * @param $id
     * @return GroupCommodity|array|bool|Model|null mix 数据
     */
    public static function isGroup($id)
    {
        $group = GroupCommodity::where('status', 'in', '0,1')
            ->where('commodity_id', $id)
            ->find();
        if (!is_null($group) && ($group['show_time'] == null || $group['show_time'] < now())) {
            $collages = "";
            GroupCommodity::finishCollage($group['id']);
            $groupConfig = Configuration::where(['type' => "group"])->value("configuration");
            if (!empty($groupConfig)) {
                $is_open = json_decode($groupConfig, true)['is_open'];
                if ($is_open == 1) {
                    $collages = Collage::with('user')->where('status', 1)
                        ->where('gc_id', $group['id'])
                        ->limit(10)
                        ->select();
                    foreach ($collages as $ck => $cv) {
                        $collages[$ck] = changTime($cv);
                    }
                }
            }
            if ($group['has_sku'] == 1) {
                $group['sku'] = GroupCommoditySku::where('gc_id', $group['id'])->select();
            }
            $group['start_time'] = Group::find($group['group_id'])['start_time'];
            if ($group['start_time'] < now() || ($group['show_time'] != null && $group['show_time'] < now())) {
                return [changTime($group), $collages];
            }
        }
        return [false, false];
    }

    /**
     * 是否为秒杀
     * @param $id
     * @return SeckillCommodity|array|bool|Model|null mix 数据
     */
    public static function isSeckill($id)
    {
        $seckill = SeckillCommodity::where('status', 'in', '0,1')
            ->where('commodity_id', $id)
            ->find();
        if (!is_null($seckill) && ($seckill['show_time'] == null || $seckill['show_time'] < now())) {
            $seckill['sku'] = SeckillCommoditySku::where('sc_id', $seckill['id'])->select();
            $seckill['start_time'] = Seckill::find($seckill['seckill_id'])['start_time'];
            if ($seckill['start_time'] < now() || ($seckill['show_time'] != null && $seckill['show_time'] < now())) {
                return changTime($seckill);
            }
        }
        return false;
    }

    /**
     * 是否为预售
     * @param $id
     */
    public static function isPresell($id)
    {
        $presell = PresellCommodity::where('status', 'in', '0,1')
            ->where('commodity_id', $id)
            ->find();
        if (!is_null($presell)) {
            $presell['sku'] = PresellCommoditySku::where('pc_id', $presell['id'])->select();
            $presell['start_time'] = Presell::find($presell['seckill_id'])['start_time'];
            if ($presell['start_time'] < now()) {
                return changTime($presell);
            }
        }
        return false;
    }
}