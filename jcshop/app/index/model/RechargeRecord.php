<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\model;

use app\SearchModel;
use think\db\Query;
use think\Model;
use think\model\concern\SoftDelete;


class RechargeRecord extends SearchModel
{
    use SoftDelete;

    protected $globalScope = ['userId'];

    private $user;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        global $user;
        $this->user = $user;
    }

    public function scopeUserId(Query $query)
    {
        $query->where('user_id', $this->user['id']);
    }

    public function searchNameAttr(Query $query, $value, $data)
    {
        $query->where('name', 'like', "%" . $value . "%");
    }

}
