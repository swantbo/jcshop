<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\model;


use app\SearchModel;
use think\Model;
use think\model\concern\SoftDelete;

class OrderAfter extends SearchModel
{
    public static function buildAfterNo()
    {
        $no = 'SH' . date('YmdHis', time()) . rand(10000, 99999);
        return $no;
    }

    public function getAfterAddressIdAttr($value)
    {
        $res = ReturnAddress::field('linkman,phone,concat(district,address) as address')->find($value);
        return $res;
    }
    public function oc()
    {
        return $this->hasMany(AfterOrderCommodity::class, 'order_after_id', 'id');
    }

    public function address()
    {
        //->bind(['address', 'consignee', 'iphone']) after_address_id
        return $this->belongsTo(AfterAddress::class, 'after_address_id', 'id');
    }

//    public function getStateAttr($value)
//    {
//        1-等待商家处理 2-同意申请 3-驳回申请 4-商家已发货，等待客户收货 5-手动退款 6-确认发货 7-退款退货完成 8-退款完成 9-换货完成 10-退回物品,等待退款  11-同意退款 12-等待买家退回商品 13-买家退回物品，等待退款 14
//        $arr = [
//            1 => "发起申请,等待商家处理",
//            2 => "商家已同意申请",
//            3 => "商家已拒绝",
//            4 => "卖家已发货,请注意查收",
//            5 => "手动退款",
//            6 => "确认发货",
//            7 => "退款退货完成",
//            8 => "退款完成",
//            9 => "换货完成",
//            10 => "退回物品,等待退款",
//            11 => "同意退款",
//            12 => "商家通过审核",
//            13 => "买家退回物品，等待退款",
//            14 => "已退货,等待商家确认收货"
//        ];
//        return $arr[$value];
//    }

}