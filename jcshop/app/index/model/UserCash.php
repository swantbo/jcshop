<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\model;

use app\SearchModel;
use think\Model;

class UserCash extends SearchModel
{
    protected $pk = 'user_id';

    /**
     * 积分赠送
     * @param $user_id
     * @param $num
     * @param $type
     */
    public static function giveIntegral($user_id, $num, $type, $mid)
    {
        UserCash::where('user_id', $user_id)
            ->inc('integral_total', $num)
            ->inc('integral', $num)
            ->update(['update_time' => date('Y-m-d H:i:s')]);
        IntegralRecord::create([
            'mall_id' => $mid,
            'user_id' => $user_id,
            'type' => $type,
            'status' => 1,
            'integral' => $num,
            'create_time' => date('Y-m-d H:i:s', time())
        ]);
    }
}
