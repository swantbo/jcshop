<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\model;

use think\facade\Cache;

class MallBase extends BaseModel
{

    private static $base;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        //设置商城基础数据
        if (Cache::has(checkRedisKey("MALL_BASE"))) {
            self::$base = Cache::get(checkRedisKey("MALL_BASE"));
        } else {
            $configuration = Configuration::where(['type' => 'mallBase', 'status' => 1])->value('configuration');
            if (!empty($configuration)){
                $configuration = json_decode($configuration, true);
                self::$base = $configuration['mall_name'];
                Cache::set(checkRedisKey("MALL_BASE"), $this->base);
            }
        }
    }

    public static function getBase()
    {
        return self::$base;
    }


}