<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\model;


use app\SearchModel;
use think\db\Query;
use think\Model;

class Classify  extends SearchModel
{
    protected $globalScope = ['status'];

    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }

    public function scopeStatus(Query $query)
    {
        $query->where('status', 1);
    }

    public function children()
    {
        return $this->hasMany(Classify::class,"pid",'id')
            ->field('id,name,pid,img');
    }

}