<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\controller;

use app\index\service\ServiceFactory;
use think\Exception;
use think\facade\Db;


class Configuration extends BaseAction
{


    public function read(string $type)
    {
        $permit = [
            'mallBase',
            'mallShare',
            'mallContact',
            'rightsSetting',
            'agentExplain',
            'integralBalance',
            'tradeSetting',
            'customerService',
            'mallPayMode',
            'oss',
            'lottery',
            'memberLevel',
            'withholdPassword',
            'assettransfer'
        ];
        if (!in_array($type, $permit))
            throw new Exception('
            无权访问', HTTP_NOTACCEPT);

        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $type);

        if ($type == 'mallBase' && $this->baseIn == 'wechata') {
            global $mid;
            $plugins = Db::name('wechata_plugins')->where(['mall_id' => $mid])->value('plugins');
            return json(['msg' => $msg, 'plugins' => $plugins], $code);
        }

        return json(['msg' => $msg], $code);
    }

    public function withholdPassword()
    {

        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }

    public function ossConfig()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }

}
