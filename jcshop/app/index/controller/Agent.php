<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\controller;


use app\index\model\CommissionActivity;
use app\index\model\CommissionAssessment;
use app\index\model\Configuration;
use app\index\model\UserCash;
use app\index\service\ServiceFactory;
use app\madmin\model\AgentGrade;
use app\madmin\model\Coupon;
use app\madmin\service\CommissionActivityService;
use app\madmin\service\CommissionAssessmentService;
use think\App;
use think\Exception;
use think\facade\Cache;
use think\response\Json;

class Agent extends BaseAction
{
    protected $agent_id = 0;
    protected $is_open = 1;
    protected $apply = 0;
    protected $agentModel;
    public function __construct(App $app)
    {
        parent::__construct($app);
        if (!$this->user)
            throw new Exception("请先登录", HTTP_UNAUTH);
        $this->user = \app\index\model\User::find($this->user['id']);
        if (empty($this->user))
            throw new Exception("请先登录", HTTP_UNAUTH);
        $this->user = $this->user->toArray();
        $configuration = Configuration::where(['type' => "agentExplain"])
            ->where('mall_id',$this->user['mall_id'])
            ->value("configuration");
        if (!$configuration)throw new Exception("未开启分销",HTTP_INVALID);
        if ($this->is_open != 0) {
            $configuration = json_decode($configuration, true);
            if (empty($configuration['open']))throw new Exception("未开启分销", HTTP_INVALID);
        }

        $this->agentModel = \app\index\model\Agent::where('user_id',$this->user['id'])->find();
        if ($this->agentModel){
            $this->agent=$this->agentModel->id;
        }

        if (empty($this->agent)) {
            $user = \app\index\model\User::find($this->user['id']);
            $status = \app\index\model\AgentApply::field('id,status')->where('user_id', $this->user['id'])->find();
            if (empty($status)) $this->apply = 0;
            elseif ($status->status == 2) $this->apply = 2;
            elseif ($status->status == 0) $this->apply = 1;
            $user = $user->toArray();
            $token = request()->header('Authorization');
            Cache::store('redis')->set("INDEX:" . $token, json_encode($user), 60 * 60 * 24 * 30);
            $this->user = $user;
        }

    }


    public function findAll(): Json
    {
        if (!empty($this->apply))
            return json(['msg' => ['type' => $this->apply]],HTTP_SUCCESS);
        if (empty($this->agent))
            return json(['msg' => ['is_agent' => 0]],HTTP_SUCCESS);
        $page = input('get.page', 1);
        $size = input('get.size', 10);
        $level = input('level', 0);

        list($code, $list) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $page, $size, $level);
        return json(['msg' => $list], $code);
    }


    public function read(): Json
    {
        if (!empty($this->apply))
            return json(['msg' => ['type' => $this->apply]],HTTP_SUCCESS);
        if (empty($this->agent))
            return json(['msg' => ['is_agent' => 0]],HTTP_SUCCESS);
        list($code, $list) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $list], $code);
    }

    public function order() : Json{
        if (!empty($this->apply))
            return json(['msg' => ['type' => $this->apply]],HTTP_SUCCESS);
        $page = input('page');
        $size = input('size');
        $data = input('post.');
        list($code,$list) = ServiceFactory::getResult('AgentService','agent_order',$page,$size,$data);
        return json(['msg' => $list],$code);
    }
    public function agentGrade() : Json{
        if (!empty($this->apply))
            return json(['msg' => ['type' => $this->apply]],HTTP_SUCCESS);
        list($code,$list) = ServiceFactory::getResult('AgentService','agentGrade');
        return json(['msg' => $list],$code);
    }

    public function ActivityList()
    {
        $service=new CommissionActivityService();
        $service->close();
        $page = $this->request->get('page',1);
        $size = $this->request->get('size',10);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $page, $size);
        return json(['msg' => $msg], $code);

    }
    public function ActivityInfo($id) : Json{

        if (!empty($this->apply))
            return json(['msg' => ['type' => $this->apply]],HTTP_SUCCESS);
        $service=new CommissionActivityService();
        $service->close();
        list($code,$list) = ServiceFactory::getResult('AgentService','ActivityInfo',$id);
        return json(['msg' => $list],$code);
    }
    public function AssessmentInfo() : Json{

        if (!empty($this->apply))
            return json(['msg' => ['type' => $this->apply]],HTTP_SUCCESS);
        $service=new CommissionAssessmentService();
        $service->close();
        list($code,$list) = ServiceFactory::getResult('AgentService','AssessmentInfo');
        return json(['msg' => $list],$code);
    }


}