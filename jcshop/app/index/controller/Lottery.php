<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\controller;


use app\index\service\ServiceFactory;
use think\App;
use think\Exception;

class Lottery extends BaseAction
{
    protected $class_name = 'LotteryService';
    public function __construct(App $app)
    {
        parent::__construct($app);
        if (!$this->user)
            throw new Exception("请先登录", HTTP_UNAUTH);
    }

    public function list(){
        list($code,$list) = ServiceFactory::getResult($this->class_name,__FUNCTION__);
        return json(['msg' => $list],$code);
    }

    public function read(int $id){
        list($code,$list) = ServiceFactory::getResult($this->class_name,__FUNCTION__,$id);
        return json(['msg' => $list],$code);
    }

    public function lottery(){
        $style = input('style');
        $update_time = input('update_time');
        $type = input('type');
        list($code,$data) = ServiceFactory::getResult($this->class_name,__FUNCTION__,(int)$style,$update_time,$type);
        return json(['msg' => $data],$code);
    }

    public function lottery_commodity_receive(int $id){
        $name = input('name');
        $mobile = input('mobile');
        $address = input('address');
        $type = input('type');
        $store_id = input('store_id',null);
        list($code,$data) = ServiceFactory::getResult($this->class_name,__FUNCTION__,$id,(string)$name,(int)$mobile,(string)$address,(int)$type,$store_id);
        return json(['msg' => $data],$code);
    }


    public function lottery_log_list(){
        $page = input('page');
        $size = input('size');
        list($code,$list) = ServiceFactory::getResult($this->class_name,__FUNCTION__,$page,$size,input('post.'));
        return json(['msg' => $list],$code);
    }

    public function lottery_log_read(int $id){
        list($code,$data) = ServiceFactory::getResult($this->class_name,__FUNCTION__,$id);
        return json(['msg' => $data],$code);
    }

    public function lottery_goods_receiving(int $id){
        list($code,$data) = ServiceFactory::getResult($this->class_name,__FUNCTION__,$id);
        return json(['msg' => $data],$code);
    }

    public function lottery_goods_qrcode(int $id){
        list($code,$data) = ServiceFactory::getResult($this->class_name,__FUNCTION__,$id);
        return json(['msg' => $data],$code);
    }

    public function lottery_get_number_limit(int $style){
        $is_bool = ServiceFactory::getResult($this->class_name,'lotteryGetNumber',$style);
        return json(['msg' => $is_bool],HTTP_SUCCESS);
    }
}