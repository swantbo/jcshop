<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\controller;


use app\index\service\ServiceFactory;
use app\Request;
use app\utils\Addons;
use think\App;
use think\Exception;
use think\response\Json;

class OrderForm extends BaseAction
{

    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    public function getTemplates(Request $request) : Json{
        $ids=$request->get('ids',[]);
        list($code,$data) = ServiceFactory::getResult('OrderFormService','getTemplates',$ids);
        return json(['msg' => $data],$code);
    }

    public function getOrderForm(Request $request) :Json{
        list($code,$data) = ServiceFactory::getResult('OrderFormService','getOrderForm');
        return json(['msg' => $data],$code);
    }
}