<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\controller;


use app\index\service\ServiceFactory;
use app\utils\Addons;
use think\App;
use think\Exception;
use think\response\Json;

class Form extends BaseAction
{
    private $is_bool;
    public function __construct(App $app)
    {
        parent::__construct($app);
        $mid = request()->header('mid');
        $this->is_bool = Addons::check($mid,13);
    }

    public function read(int $id) : Json{
        if (!$this->is_bool)return json(['msg' => []],HTTP_SUCCESS);
        list($code,$data) = ServiceFactory::getResult('FormService','read',$id);
        return json(['msg' => $data],$code);
    }

    public function list() : Json{
        $page = input('page');
        $size = input('size');
        if (!$this->is_bool)return json(['msg' => []],HTTP_SUCCESS);
        list($code,$list) = ServiceFactory::getResult('FormService','findAll',$page,$size);
        return json(['msg' => $list],$code);
    }


    public function answer() : Json{
        if (!$this->is_bool)return json(['msg' => []],HTTP_SUCCESS);
        if (!$this->user)
            throw new Exception("请先登录", HTTP_UNAUTH);
        $answer = input('answer','');
        $template_id = input('template_id',0);
        list($code,$answer) = ServiceFactory::getResult('FormService','answer',$answer,$template_id);
        return json(['msg' => $answer],$code);
    }


    public function answerList() : Json{
        if (!$this->is_bool)return json(['msg' => []],HTTP_SUCCESS);
        if (!$this->user)
            throw new Exception("请先登录", HTTP_UNAUTH);
        $page = input('page');
        $size = input('size');
        list($code,$answer) = ServiceFactory::getResult('FormService','answerList',$page,$size);
        return json(['msg' => $answer],$code);
    }


    public function answerRead(int $id) : Json{
        if (!$this->is_bool)return json(['msg' => []],HTTP_SUCCESS);
        list($code,$answer) = ServiceFactory::getResult('FormService','answerRead',$id);
        return json(['msg' => $answer],$code);
    }
}