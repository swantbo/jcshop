<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\controller;


use app\index\service\ServiceFactory;
use think\App;
use think\Exception;

class NewKidGift extends BaseAction
{
    private $is_bool;
    public function __construct(App $app)
    {
        parent::__construct($app);
        if (empty($this->user))
            throw new Exception('请先登录',HTTP_UNAUTH);
        $this->is_bool = \app\utils\Addons::check($this->user['mall_id'],1);
    }


    public function read(){
        if (!$this->is_bool)return json(['msg' => []],HTTP_SUCCESS);
        list($code,$data) = ServiceFactory::getResult('NewKidGiftService','read');
        return json(['msg' => $data],$code);
    }

    public function receive(int $id){
        if (!$this->is_bool)return json(['msg' => []],HTTP_SUCCESS);
        list($code,$msg) = ServiceFactory::getResult('NewKidGiftService','receive',$id);
        return json(['msg' => $msg],$code);
    }
}