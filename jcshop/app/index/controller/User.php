<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\controller;

use app\index\service\ServiceFactory;
use think\Exception;
use think\facade\Cache;
use app\utils\SendMsg;


class User extends BaseAction
{

    public function agent($agentId)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $agentId);
        return json(['msg' => $msg], $code);
    }


    public function read(int $id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }

    public function register()
    {
        $data = input('post.');
        $id = $this->user['id'];
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data, $id);
        return json(['msg' => $msg], $code);
    }


    public function tokenToUserInfo()
    {
        $token = request()->header('Authorization');
        if (empty($token) || $token == "null") {
            throw new Exception("请先登录", HTTP_UNAUTH);
        }
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $token);
        return json(['msg' => $msg], $code);
    }

    public function bindingPhone()
    {
        $phone = input('phone');
        $code = input('code');
        list($code, $result) = ServiceFactory::getResult('UserService', 'bindingPhone', $phone, $code, $this->user['id']);
        return json(['msg' => $result], $code);
    }

    public function UserCenter()
    {
        list($code, $result) = ServiceFactory::getResult('UserService', __FUNCTION__);
        return json(['msg' => $result], $code);
    }

    public function setPid()
    {
        $pid = input('post.pid');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $pid);
        return json(['msg' => $msg], $code);
    }


    public function upUserData(int $id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }

    public function getMorePhone()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }

    public function upUser()
    {
        $data = input('post.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data);
        return json(['msg' => $msg], $code);
    }

    /**
     * 手机号注册用户
     * @return \think\response\Json
     */
    public function userRegister()
    {
        $data = input('post.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data);
        return json(['msg' => $msg], $code);
    }

    /**
     * 发送验证码
     * @param int $mobile
     * @return array
     * @throws
     */
    public function getSmsCode(int $mobile)
    {
        $code = rand(100000, 999999);
        $sendMsg = new SendMsg("用户注册");
        $user = (Object)['mobile' => $mobile, 'userWx' => (Object)['type' => 3]];
        $res = $sendMsg->send($user, ['verification_code' => $code]);
        if ($res) {
            Cache::set(checkRedisKey((string)$mobile), $code, 300);
            return json(['msg' => "发送成功！"], HTTP_SUCCESS);
        } else {
            return json(['msg' => "发送失败，请稍后再试！"], HTTP_SERVERERROR);
        }
    }

    /**
     * 手机号登录
     * @return \think\response\Json
     */
    public function login()
    {
        $data = input('post.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data);
        return json(['msg' => $msg], $code);
    }

}
