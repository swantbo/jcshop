<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\controller;


use app\index\service\ServiceFactory;
use think\App;
use think\Exception;

class Qrcode extends BaseAction
{

    public function __construct(App $app)
    {
        parent::__construct($app);

    }

    public function qrcode()
    {
        $path = input("get.path");
        $data = input("get.data");
        $scene = input("get.scene");
        $page = input("get.page");
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $path,$data, $scene, $page);
        return json(['msg' => $msg], $code);
    }


    public function agent()
    {
        $data = input("get.");
        if (!$this->user)
            throw new Exception("请先登录", HTTP_UNAUTH);
        $agent_id = \app\index\model\Agent::where('user_id',$this->user['id'])->value('id');
        if (empty($agent_id))
            throw new Exception("请先成为推广员", HTTP_NOTACCEPT);
//        if (empty($agentId))
//            throw new Exception("请先升级为推广员", HTTP_NOTACCEPT);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $agent_id, $data);
        return json(['msg' => $msg], $code);
    }

    public function commodity($commodityId)
    {
        $data = input("get.");
        $agent_id = \app\index\model\Agent::where('user_id',$this->user['id'])->value('id');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $commodityId, $agent_id, $data);
        return json(['msg' => $msg], $code);
    }
}