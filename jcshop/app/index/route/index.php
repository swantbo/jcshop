<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use app\index\validate\AcceptValidate;
use app\index\validate\ArticleTypeValidate;
use app\index\validate\ArticleValidate;
use app\index\validate\PaymentValidate;
use app\index\validate\TrolleyValidate;
use app\index\validate\WithdrawValidate;
use think\facade\Route;
use think\facade\View;

//Route::rest('create',['POST','/list','findAll']);
//Route::resource("user","sadmin/user");    资源路由 存在无法使用验证器问题
Route::any('', 'index/index');
Route::any('test', 'index/test');
//登录返回数据
Route::group('login', function () {
    Route::any('sign', 'login')
        ->allowCrossDomain([
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Methods' => 'GET, POST, PATCH, PUT, DELETE',
            'Access-Control-Allow-Headers' => "Authorization, Content-Type, If-Match, If-Modified-Since, If-None-Match, If-Unmodified-Since, X-Requested-With"
        ]);
    Route::get('getBaseInfo', 'getBaseInfo')
        ->allowCrossDomain([
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Methods' => 'GET, POST, PATCH, PUT, DELETE',
            'Access-Control-Allow-Headers' => "Authorization, Content-Type, If-Match, If-Modified-Since, If-None-Match, If-Unmodified-Since, X-Requested-With"
        ]);
    Route::get('getUserOpenId', 'getUserOpenId');
    Route::get('text', 'text');
    Route::get('user', 'user');
    Route::post('userInfo', 'saveUserInfo')
        ->validate(app\index\validate\UserValidate::class, "saveUserInfo");//用户
    Route::get('getTokenUser', 'getTokenUser');
    Route::get('code/:phone', 'getSmsCode');
    Route::get('verification/:phone/:code', 'verificationSmsCode');
    Route::post('getUserPhone', 'getUserPhone'); // 废弃
    Route::post('getPhone', 'getPhone');
    Route::post('division', 'division');
})->prefix('login/');

Route::group('user', function () {
    Route::post('upUser', 'upUser');
    Route::post('register', 'register');
    Route::post('setPid', 'setPid');
    Route::get('tokenToUserInfo', 'tokenToUserInfo');
    Route::get('bindingMobile/:phone/:code', 'bindingPhone');
    Route::get('UserCenter', 'UserCenter');
    Route::get('agent/:agentId', 'agent');
    Route::get('getMorePhone', 'getMorePhone');
    Route::put('upUserData', 'upUserData');
    Route::post('userRegister', 'userRegister');
    Route::post('getSmsCode', 'getSmsCode'); // 发送验证码
    Route::post('login', 'login'); // 手机号登录
    Route::post('userLogin', 'userLogin'); // 验证码登录
})->prefix('user/')->pattern(['agentId' => '\d+']);
//分销商
Route::group('agent', function () {
    Route::post('list', 'findAll');//分销列表
    Route::get('', 'read');//分销列表
    Route::post('order', 'order');
    Route::get('agentGrade', 'agentGrade');
    Route::get('ActivityInfo/:id', 'ActivityInfo');
    Route::get('ActivityList', 'ActivityList');
    Route::get('AssessmentInfo', 'AssessmentInfo');
})->prefix('agent/')->pattern(['id' => '\d+']);
Route::group('agent/withdraw', function () {
    //提现:余额
    Route::put('total', 'total');
    Route::post('list', 'findAll')
        ->validate(app\index\validate\AgentWithdrawValidate::class, 'findAll');
    Route::get('info', 'read')
        ->validate(app\index\validate\AgentWithdrawValidate::class, 'read');
    Route::post('', 'save')
        ->validate(app\index\validate\AgentWithdrawValidate::class, 'save');
    Route::post('apply', 'apply')
        ->validate(app\index\validate\AgentWithdrawValidate::class, 'apply');
    Route::put('weixin', 'weixin');
})->prefix('agent_withdraw/');
//商品
Route::group('goods', function () {
    Route::post('id', 'findByIds');
    Route::post('list', 'goodsList')
        ->validate(app\index\validate\CommodityValidate::class, "goodsList");//商品列表
    Route::get(':id', 'goodsInfo')
        ->validate(app\index\validate\CommodityValidate::class, "goodsInfo");//商品详情
    Route::get('inventory/:commodityId', 'inventory');
    Route::get('evaluate', 'evaluate');
    Route::post('getManyGoods', 'getManyGoods');
    Route::get('commodity', 'commodity')
        ->validate(app\index\validate\AgentValidate::class, 'commodity');
})->prefix('commodity/')->pattern(['id' => '\d+']);

//商户申请
Route::group("merchant/apply", function () {
    Route::post('', 'save')
        ->validate(app\index\validate\MerchantApplyValidate::class, 'save');
    Route::get('', 'read');
    Route::put(':id', 'update')
        ->validate(app\index\validate\MerchantApplyValidate::class, 'update');
    Route::delete(':id', 'delete');
})->prefix('merchant_apply/')->pattern(['id' => '\d+']);

//商户
Route::group("merchant", function () {
    Route::post('commodity', 'getMerchantAndCommodityByIds');
    Route::post('list', 'findAll')
        ->validate(app\index\validate\MerchantValidate::class, 'list');
    Route::get(':id', 'read');
})->prefix('merchant/')->pattern(['id' => '\d+']);

//商户门店
Route::group("merchant/store", function () {
    Route::post('list', 'findAll');
//        ->validate(app\index\validate\StoreValidate::class,'list');
    Route::get(':id', 'read');
})->prefix('store/')->pattern(['id' => '\d+']);

//推广员申请
Route::group("agent/apply", function () {
    Route::post('', 'save')
        ->validate(app\index\validate\AgentApplyValidate::class, 'save');
    Route::get('', 'read');
    Route::put(':id', 'update');
    Route::delete(':id', 'update');
})->prefix('agent_apply/')->pattern(['id' => '\d+']);

//二维码
Route::group("qrcode", function () {
    Route::get('', 'qrcode');
    Route::get('agent', 'agent');
    Route::get('commodity/:commodityId', 'commodity');
})->prefix('qrcode/')->pattern(['commodityId' => '\d+']);

//购物车
Route::group('trolley', function () {
    Route::get('count', 'count');
    Route::get('', 'index');
    Route::post('', 'save')
        ->validate(TrolleyValidate::class, 'save');
    Route::put('', 'update')
        ->validate(TrolleyValidate::class, 'update');
    Route::delete('', 'delete');
    Route::delete('more', 'deleteMore');
    Route::delete('all', 'deleteAll');
})->prefix('trolley/');

Route::post('createOrder', 'text/createOrder'); //订单测试

//订单
Route::group('order', function () {
    Route::get('userAddressList', 'userAddressList');
    Route::post('createOrder', 'createOrder');
    Route::post('addUserAddress', 'addUserAddress');
    Route::post('shopCartFreight', 'shopCartFreight');
    Route::post('imgUpdate', 'imgUpdate');
    Route::post('afterSales', 'afterSales');
    Route::post('evaluate', 'evaluate')
        ->validate(app\index\validate\EvaluateValidate::class, 'evaluate');
    Route::delete('orderDel/:id', 'orderDel');
    Route::delete(':id', 'delUserAddress');
    Route::put('saveUserAddress', 'saveUserAddress');
    Route::put('setDefaultAddress/:id', 'setDefaultAddress');
    Route::put('confirm', 'confirm');
    Route::get('orderList', 'orderList');
    Route::get('readOrder/:id', 'orderInfo');
    Route::get('logistics', 'logistics');
    //Route::post('balancePay/:id','OrderBalancePay');
    Route::get('text', 'text');
    Route::get('orderRefund', 'orderRefund');
    Route::post('cityFreight', 'cityFreight');
    Route::post('goodsCityFreight', 'goodsCityFreight');
})->prefix('order/');

//订单
Route::group('payment', function () {
    Route::post('pay', 'pay')
        ->validate(PaymentValidate::class, 'pay');
    Route::post('recharge', 'recharge')
        ->validate(PaymentValidate::class, 'recharge');
    Route::post('membership', 'membership');
})->prefix('payment/');

//售后
Route::group('aftersale', function () {
    Route::post('apply', 'apply');
    Route::post('submitExpress', 'submitExpress');
    Route::get('afterDetails', 'afterDetails');
    Route::get('fillExpress', 'fillExpress');
    Route::post('cancelOrder', 'cancelOrder');
    Route::post('upImg', 'upImg');
    Route::post('againApply', 'againApply');
    Route::put('confirmGoods', 'confirmGoods');
    Route::put('modifyExpress', 'modifyExpress');
})->prefix('after_sale/');

//商品分类
Route::group("classify", function () {
    Route::get('', 'index');
})->prefix('classify/')->pattern(['id' => '\d+']);


//買家订单核销
Route::group('order/verification', function () {
    Route::get('', 'index');
    Route::post('', 'qrcode');
})->prefix('verification/')->pattern(['id' => '\d+']);

//賣家订单核销
Route::group('order/accept', function () {
    Route::get('', 'index')
        ->validate(AcceptValidate::class);
    Route::post('', 'confirm')
        ->validate(AcceptValidate::class);
    Route::get('wxaQr', 'wxaQr');
})->prefix('accept/')->pattern(['id' => '\d+']);
//抽奖奖品核销
Route::group('lottery/accept', function () {
    Route::post('list', 'LotteryGoodsList');
    Route::post('confirm', 'LotteryConfirm');
})->prefix('accept/');
//积分商城核销
Route::group('integral/shop/accept',function (){
    Route::post('list','integralOrderList');
    Route::post('confirm','integralOrderConfirm');
})->prefix('accept/');
//底部菜单
Route::group("fitment/bottom_menu", function () {
    Route::get('', 'index');
})->prefix('bottom_menu/')->pattern(['id' => '\d+']);

// 悬浮菜单
Route::group("fitment/float_menu", function () {
    Route::get('', 'index');
})->prefix('float_menu/')->pattern(['id' => '\d+']);

//装修DIY
Route::group("renovation", function () {
    Route::get('getRenovationInfo', 'getRenovationInfo');
    Route::get('custom', 'custom');
    Route::get('advert', 'advert');
    Route::get('text', 'text');
    Route::get('getRecommendConfig','getRecommendConfig');
    Route::get('getRecommendGoods','getRecommendGoods');
})->prefix('Renovation/');

//公告
Route::group("notice", function () {
    Route::post('list/:merchantId', 'findAll');
    Route::get(':id/:merchantId', 'read');
})->prefix('notice/')->pattern(['id' => '\d+', 'merchantId' => '\d+']);

//配置文件
Route::group("configuration", function () {
    Route::get('withholdPassword', 'withholdPassword');
    Route::get(':type', 'read');
    Route::get('ossConfig', 'ossConfig');
})->prefix('configuration/')->pattern(['type' => '[a-zA-Z]+']);

//优惠券
Route::group("coupon", function () {
    Route::post('list', 'findAll');
    Route::post('id', 'findById');
    Route::get('merchant/:merchantId', 'findByMerchant');
    Route::get('commodity/:commodityId', 'findByCommodity');
    Route::get(':id', 'read');
})->prefix('coupon/')->pattern(['id' => '\d+']);

//用户优惠券
Route::group("user/coupon", function () {
    Route::post('list', 'findAll');
    Route::get('order/:order', 'findByOrder');
    Route::post('order', 'findAllByOrder');
    Route::post('commodity', 'findByCommodity');
    Route::post('', 'save');
    Route::get(':id', 'read');
})->prefix('user_coupon/')->pattern(['id' => '\d+', 'order' => '[a-zA-Z0-9]+']);

//用户卡
Route::group("user/card", function () {
    Route::post('list', 'findAll');
    Route::post('', 'save');
    Route::get(':id', 'read');
    Route::put(':id', 'update');

    Route::delete(':id', 'delete');
})->prefix('user_card/')->pattern(['id' => '\d+']);

//微信工具
Route::group("wx/tool", function () {
    Route::post('sign', 'sign');
    Route::get('wxa/message/:type', 'subscribeMessage');
    Route::get('check', 'check');
    Route::post('wxa/message/result', 'getSubcribeMessageResult');
})->prefix('wx_tool/')->pattern(['id' => '\d+', 'type' => '[a-zA-Z0-9]+']);

//微信支付回调
Route::group("notify", function () {
    Route::any('commodity', 'commodity');
    Route::any('recharge', 'recharge');
    Route::any('membership', 'membership');
})->prefix('notify/')->pattern(['id' => '\d+']);
//积分商城
Route::group('integral/shop',function(){
    Route::any('class','classList');
    Route::post('cart/:option','shopCart');
    Route::group('goods',function (){
        Route::post('list','goodsList');
        Route::get(':id','goodsFind');
    });
    Route::group('order',function (){
        Route::get('qrcode/:id','orderQrcode');
        Route::get(':id','orderFind');
        Route::post('create','orderCreate');
        Route::post('pay/:id','orderPay');
        Route::post('list','orderList');
        Route::post('receiving/:id','orderReceiving');
        Route::post('store','storeList');
    });
})->prefix('integral_shop/');
//积分
Route::group('integral', function () {
    Route::get('share/:user_id', 'share_integral');
    Route::post('list', 'integral_list')
        ->validate(\app\index\validate\IntegralValidate::class, 'list');
    Route::post('judge', 'integral_judge')
        ->validate(\app\index\validate\IntegralValidate::class, 'judge');
})->prefix('integral/');

//文章分类
Route::group("article/type", function () {
    Route::get('', 'index')
        ->validate(ArticleTypeValidate::class);
    Route::post('list', 'findAll')
        ->validate(ArticleTypeValidate::class);
})->prefix('article_type/')->pattern(['id' => '\d+']);

//文章
Route::group("article", function () {
    Route::post('list', 'findAll')
        ->validate(ArticleValidate::class, 'findAll');
    Route::get(':id', 'read')
        ->validate(ArticleValidate::class, 'read');
})->prefix('article/')->pattern(['id' => '\d+']);

//协议
Route::group("protocol", function () {
    Route::get(':type', 'read');
})->prefix('protocol/')->pattern(['type' => '\d+']);

//收藏
Route::group("user/collect", function () {
    Route::post('list', 'findAll');
    Route::post(':commodityId', 'save');
    Route::get(':commodityId', 'read');
    Route::delete(':commodityId', 'delete');
    Route::delete('', 'deleteAll');
})->prefix('my_collect/')->pattern(['id' => '\d+', 'commodityId' => '\d+']);




//前台用户金额记录
Route::group("recharge/record", function () {
    Route::post('list', 'findAll');
})->prefix('recharge_record/')->pattern(['id' => '\d+']);

//前台用户金额记录
Route::group("accept/record", function () {
    Route::post('list', 'findAll');
})->prefix('accept_record/')->pattern(['id' => '\d+']);

//搜索关键字
Route::group("search/keyword", function () {
    Route::get('', 'index');
})->prefix('search_keyword/');

//用户提现
Route::group("user/withdraw", function () {
    Route::post('list', 'findAll')
        ->validate(WithdrawValidate::class, 'findAll');
    Route::post('', 'save')
        ->validate(WithdrawValidate::class, 'save');
})->prefix('withdraw/')->pattern(['id' => '\d+']);
//统计数据
Route::group('total', function () {
    Route::post('uvpv', 'uvpv');
})->prefix('total/');
//自定义表单
Route::group('form', function () {
    Route::group('template', function () {
        Route::get(':id', 'read');
        Route::post(':page/:size', 'list');
    });
    Route::group('answer', function () {
        Route::get(':id', 'answerRead');
        Route::post(':page/:size', 'answerList');
        Route::put('', 'answer');
    });
})->prefix('form/');
//新手礼物
Route::group('receive', function () {
    Route::get('', 'read');
    Route::post(':id', 'receive');
})->prefix('new_kid_gift/');

//发票
Route::group('invoice', function () {
    Route::post('create', 'create')
        ->validate(\app\index\validate\InvoiceValidate::class, 'create');
    Route::get('riseList', 'riseList');
    Route::get(':id', 'info');
    Route::get('list', 'list');
    Route::get('riseGain', 'riseGain');
    Route::get('riseRead', 'riseRead');
    Route::post('setRiseDefault', 'setRiseDefault');
    Route::post('riseCreate', 'riseCreate');
    Route::put('riseUp', 'riseUp');
    Route::put('editInvoice', 'editInvoice');
    Route::delete(':id', 'riseDel');
})->prefix('invoice/');
//签到
Route::group('signin', function () {
    Route::get('', 'sign');
    Route::get('config', 'config');
    Route::post('list', 'list');
})->prefix('sign_in/');

//插件
Route::group('addons', function () {
    Route::get('', 'index');
})->prefix('addons/');
//版权
Route::group('copyright', function () {
    Route::get('', 'index');
})->prefix('copyright/');

//抽奖
Route::group('lottery', function () {
    Route::get('find/:id', 'read');
    Route::post('list', 'list');
    Route::post('limit/:style','lottery_get_number_limit');
    Route::group('log', function () {
        Route::get(':id', 'lottery_log_read');
        Route::post('list', 'lottery_log_list');
    });
    Route::post('lottery', 'lottery');
    Route::group('user', function () {
        Route::post('receive/:id', 'lottery_commodity_receive');
        Route::post('receiving/:id', 'lottery_goods_receiving');
        Route::post('qrcode/:id', 'lottery_goods_qrcode');
    });
})->prefix('lottery/');

//会员等级
Route::group('membership/level', function () {
    Route::get('', 'index');
    Route::get(':id', 'read');
    Route::get('get_level_data','getLevelData');
})->prefix('membership_level/')->pattern(['id' => '\d+']);

//会员记录
Route::group('membership/record', function () {
    Route::get('', 'index');
    Route::get(':id', 'read');
})->prefix('membership_record/')->pattern(['id' => '\d+']);

//同城配送
Route::group('distribution',function(){
    Route::post('store','store');
})->prefix('distribution/');

// 微信客服
Route::get('wx/cs/:id', 'wx_cs/read');

Route::get('theme', 'theme/read');

Route::group("order/form", function () {
    Route::get('getTemplates', 'getTemplates');
    Route::get('getOrderForm', 'getOrderForm');
})->prefix('order_form/')->pattern(['id' => '\d+']);
//
Route::group("test1", function () {
    Route::get('', 'index');
})->prefix('test/')->pattern(['id' => '\d+']);

Route::group("live/room", function () {
    Route::get('list', 'list');
})->prefix('live_room/')->pattern(['id' => '\d+']);

// 拼团
Route::group('collage', function () {
    Route::get('group/:id', 'group');
    Route::get('', 'index');
    Route::get(':id', 'read');
})->prefix('collage/');

//Route::group("weixin/app", function () {
//    Route::post('save', 'save')->validate(\app\index\validate\WeixinAppValidate::class, 'save');
//})->prefix('weixin_app/')->pattern(['id' => '\d+']);

#分销考核提现
Route::group('commission/assessment/withdraw', function () {
    //提现:余额
    Route::put('total', 'total');
    Route::post('list', 'findAll')
        ->validate(app\index\validate\AgentWithdrawValidate::class, 'findAll');
    Route::get('info', 'read')
        ->validate(app\index\validate\AgentWithdrawValidate::class, 'read');
    Route::post('', 'save')
        ->validate(app\index\validate\AgentWithdrawValidate::class, 'save');
    Route::post('apply', 'apply')
        ->validate(app\index\validate\AgentWithdrawValidate::class, 'apply');
    Route::put('weixin', 'weixin');
})->prefix('commission_assessment_withdraw/');

//资产转赠日志
Route::group("assettransfer", function () {
    //查看转赠记录
    Route::get('list', 'findAll')->validate(app\index\validate\AssettransferValidate::class, 'findAll');;
    //查询用户
    Route::get('userList', 'userList');
    //转赠积分
    Route::post('integral', 'sendIntegral')->validate(app\index\validate\AssettransferValidate::class, 'amount');
    //转赠余额
    Route::post('amount', 'sendAmount')->validate(app\index\validate\AssettransferValidate::class, 'amount');
})->prefix('assettransfer/')->pattern(['id' => '\d+']);

// 充值奖励
Route::group("recharge", function () {
    Route::get('reward', 'reward');
})->prefix('recharge/')->pattern(['id' => '\d+']);