<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\service;


class StepCode
{
    public static $khsqwq = "客户申请维权";
    public static $clwq = "处理维权";
    public static $tkcg = "退款成功";
    public static $khthwp = "客户退回物品";
    public static $thtkwc = "退货退款完成";
    public static $sjcxfh = "商家重新发货";
    public static $hhcg = "换货成功";
    public static $tksb = "退款失败";
    public static $hhsb = "换货失败";
    public static $thtksb = "退货退款失败";
}