<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\service;

use app\index\model\OrderFormTemplate AS template;
use app\index\model\OrderFormAnswer AS answer;
use app\Request;
use think\Exception;

class OrderFormService
{
    protected $user;
    protected $merchant_id;
    public function __construct(){
        global $user;
        $this->user=$user;
    }

    public function getTemplates($ids){
        if(is_string($ids)){
            $ids=explode(",",$ids);
        }
        $templates=template::where(['mall_id'=>$this->user['mall_id'],'status'=>1])->where('id','in',$ids)->select();
        return [HTTP_SUCCESS,$templates];
    }
    public function getOrderForm(){
        $template=template::where(['mall_id'=>$this->user['mall_id'],'status'=>1,'source'=>0])->find();
        return [HTTP_SUCCESS,$template];
    }
    public function answer($order,$answers){
        foreach($answers as $answer_info){
            $template = template::find($answer_info['template_id']);
            if (empty($template)){
                continue;
            }
            answer::create([
                'order_id'=>$order->id,
                'mall_id'=> $this->user['mall_id'],
                'user_id' => $this->user['id'],
                'template_id' => $template->id,
                'answer' => json_encode($answer_info['answer']) ?? json_encode([]),
                'commodity_id' => !empty($answer_info['commodity_id']) ?$answer_info['commodity_id'] : 0,
                'source'=>$template->source ?? 0,
                'merchant_id' => $this->user['merchant_id'] ?? 0,
                'create_time' => now(),
                'update_Time' => now()
            ]);
        }
        return true;
    }
    public function verify($data){
        //todo 验证表单中数据待写
    }
}