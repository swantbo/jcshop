<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\service;

use app\index\model\Order;
use app\index\model\ReceiptsTask;
use app\index\model\ReceiptsTemplate;
use app\shop_admin\model\ReceiptsPrinter;
use app\utils\Addons;
use app\utils\Receipts;
use app\utils\RedisUtil;

class ReceiptsService extends Receipts
{
    private $is_bool;
    private $task;
    public $printer;

    public function __construct(){
        global $mid;
        $this->is_bool = Addons::check($mid,10);
        $select = ReceiptsTask::where('mall_id',$mid)->select();
        $task = [];
        foreach ($select AS $key => $value)$task[$value['id']] = $value;
        $this->task = $task;
        $printer = ReceiptsPrinter::select()->toArray();
        $this->printer = (object)[];
        foreach ($printer AS $key => $value){
            $id = $value['id'];
            $this->printer->$id = $value;
        }
        $template = ReceiptsTemplate::select()->toArray();
        $this->template = (object)[];
        foreach ($template AS $key => $value){
            $id = $value['id'];
            $this->template->$id = $value;
        }
    }

    public function printer(int $order_id, string $type = 'place',string $pay_no = '') : void{
        if (!$this->is_bool)return;
        $field = 'o.id,o.count,o.type,o.receiving_time,o.money,o.order_id,(CASE WHEN o.sku_id IS NULL THEN c.name ELSE CONCAT(c.name,\'(\',(select pvs_value FROM jiecheng_sku WHERE id=o.sku_id),\')\') END) AS name,store_id,`group`';
        if ($type == 'receiving'){//收货状态
            $order = Order::where('id',$order_id)
                ->with([
                    'order_commodity' => function($sql) use ($field){
                        $sql->field($field)
                            ->alias('o')
                            ->join('commodity c','c.id=o.commodity_id','left')
                            ->where('o.status',7);
                        },
                    'user' => function($sql){
                        $sql->field('id,nickname,name,mobile');
                    }]);
        }else{
            if (empty($order_id) && !empty($pay_no))
                $order_id = Order::where('pay_no',$pay_no)->value('id');
            $order = Order::where('id',$order_id)
                ->with([
                    'order_commodity' => function($sql) use ($field){
                        $sql->field($field)
                            ->alias('o')
                            ->join('commodity c','c.id=o.commodity_id','left');
                    },
                    'user' => function($sql){
                        $sql->field('id,nickname,name,mobile');
                    }]);
        }
        $order = $order->find();
        if (empty($order)) return ;
        if (empty($this->task))return ;
        foreach ($this->task AS $key => $value){
            if (!empty($value->merchant_id))
                if ($value->merchant_id != $order->merchant_id) continue;
            if (!empty($value->type)){
                $order2 = $order;
                $order_commodity = $order->order_commodity->toArray();
                $is_continue = false;
                if ($value->type == 2) {
                    $store_id = array_column($order_commodity, 'store_id');
                    if (!in_array($value->store_id, $store_id)) continue;
                    foreach ($order_commodity AS $index => $item) {
                        if (!empty($value->store_id)) {
                            if ($item['store_id'] != $value->store_id) {
                                $is_continue = true;
                                unset($order2->order_commodity[$index]);
                            }
                        }
                    }
                }
                foreach ($order_commodity AS $index => $item) {
                    if ($item['type'] != $value->type){
                        $is_continue = true;
                        unset($order2->order_commodity[$index]);
                    }
                }
                if ($is_continue)continue;
            }
            $config = json_decode($value->configure);
            if(empty($config->$type))continue;
            $printer_id = $value->printer_id;
            if (empty($this->printer->$printer_id))continue;
            $template_id = $value->template_id;
            if (empty($this->template->$template_id))continue;
            $printer = (object)$this->printer->$printer_id;
            $template = (object)$this->template->$template_id;
            if (empty($template->config))continue;
            $template = json_decode($template->config);
            $this->template = $template;
            $this->client_secret = $printer->client_secret;
            $post['client_id'] = $printer->client_id;
            $post['machine_code'] = $printer->printer_no;
            $post['timestamp'] = time();
            $post['id'] = md5(uniqid().rand(1000,9999));
            $post['origin_id'] = $order->order_no;
            $post['access_token'] = $this->getAccessToken($post);
            $post['content'] = $this->getContent($value->template_id,$order2??$order,$value->number);
            $post['sign'] = $this->getSign($post);
            if (empty($post))continue;
            $this->post($post, $this->printf_url);
        }
    }
}