<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\service;

use app\index\model\Configuration;
use app\index\model\RechargeRecord;
use app\index\model\UserCash;
use app\index\model\Withdraw;
use app\utils\CheckWithdraw;
use app\utils\TrimData;
use think\Exception;

class WithdrawService
{
    private $mid;

    private $user;

    private $serialNum;

    private $integralBalance;

    public function __construct()
    {
        global $user, $mid;
        $this->mid = $mid;
        $this->user = $user;
        $this->serialNum = md5(uniqid() . time());

    }

    public function findAll(array $data, int $page = 1, int $size = 10)
    {
        $admin = RechargeRecord::where(['user_id' => $this->user['id']]);
        $admin = TrimData::searchDataTrim($admin, $data, ['begin_date', 'end_date']);
        $list = $admin->where($data)->order('id', 'desc')->paginate(['page' => $page, 'list_rows' => $size]);
        return [HTTP_SUCCESS, $list];
    }


    public function save($data, ?string $remark = '')
    {

        $integralBalance = Configuration::where(['type' => "integralBalance"])->value("configuration");
        if (empty($integralBalance))
            throw new Exception("未完善支付配置",HTTP_NOTACCEPT);
        $this->integralBalance = json_decode($integralBalance, true);
        if (!isset($this->integralBalance['withdraw']) || empty($this->integralBalance['withdraw']))
            throw new Exception("暂不支持提现",HTTP_NOTACCEPT);


        $checkWithdraw = new CheckWithdraw($this->integralBalance);
        $serviceCharge = $checkWithdraw->total($data['amount']);

        $userCash = UserCash::where(['user_id' => $this->user['id']])->lock(true)->find();

        $realityCash = bcadd((string)$data['amount'], (string)$serviceCharge);

        if ($userCash->total < $data['amount']) {
            throw new Exception("可提现金额不足", HTTP_NOTACCEPT);
        }
        $userCash->dec("total", $realityCash)
            ->inc("withdraw", $realityCash)
            ->update();

        $rechargeRecordData = [
            "user_id" => $this->user['id'],
            'mall_id' => $this->mid,
            "base_in" => "admin",
            "source_type" => 2,
            "source" => "用户提现提现申请",
            "is_online" => $data['is_online'],
            "type" => 1,
            "card" => isset($data['card']) ? $data['card'] : null,
            "money" => $data['amount'],
            "service_charge" => $serviceCharge,
            "remark" => $remark,
            "trade" => $this->serialNum,
            "old_cash" => $userCash->total,
            "new_cash" => bcsub((string)$userCash->total, $realityCash),
            "status" =>1
        ];
        RechargeRecord::create($rechargeRecordData);

        Withdraw::create([
            "user_id" => $this->user['id'],
            'mall_id' => $this->mid,
            "withdraw" => $data['amount'],
            "service_charge" => $serviceCharge,
            "is_online" => $data['is_online'],
            "type" => 1,
            "card" => isset($data['card']) ? $data['card'] : null,
            "status" => 0,
            'remark' => $remark,
            'serial_num' => $this->serialNum,
            'receipt_img'=>$data['receipt_img']?? ''
        ]);

        return [HTTP_CREATED, "发起提现申请成功"];
    }



}