<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\service;

use app\Oss;
use think\facade\Filesystem;
use app\index\model\User;

class UpdateImgService
{
    //上传头像用
    const avatar=1;

    public function images($files)
    {
        global $mid, $user;
        $path = "storage/" . Filesystem::disk('public')->putFile('index', $files);
        $oss = new Oss();
        $up = $oss->up($path, $path, (int)$mid);
        if (!is_null($up)) {
            !empty($up) && @unlink($path);
            $saveName = $up;
        } else {
            $saveName = request()->server("REQUEST_SCHEME") . '://' .$_SERVER['HTTP_HOST'] . '/' . $path;
        }
        $source= request()->header('source');
        if(is_numeric($source) && $source ==1 ){
            User::update(['picurl' => $saveName, 'id' => $user['id']]);
        }

        return [HTTP_SUCCESS, [$saveName]];
    }
}