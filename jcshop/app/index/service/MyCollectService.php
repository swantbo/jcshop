<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\service;

use app\index\model\MyCollect as admin;
use app\utils\TrimData;
use think\model\Relation;

class MyCollectService extends BaseService
{

    public function findAll(array $data, int $page = 1, int $size = 10)
    {

        $admin = admin::withJoin(['commodity' => function (Relation $query) {
            $query->withField(['id', 'name', 'master', 'sell_price', 'status']);
        }]);
        $admin = TrimData::searchDataTrim($admin, $data, ['begin_date', 'end_date']);

        $list = $admin->where(['user_id' => $this->user['id']])
            ->where($data)
            ->hidden(['commodity_id','user_id'])
            ->paginate(['page' => $page, 'list_rows' => $size]);
        return [HTTP_SUCCESS, $list];
    }

    public function save($commodityId)
    {
        admin::create(['user_id' => $this->user['id'], 'commodity_id' => $commodityId]);
        return [HTTP_CREATED, "添加成功"];
    }

    public function read($commodityId)
    {
        $model = admin::where(['user_id' => $this->user['id'], 'commodity_id' => $commodityId])->find();
        $result = $model ? true : false;
        return [HTTP_SUCCESS,$result];
    }

    public function delete(int $commodityId)
    {
        admin::destroy(['commodity_id'=>$commodityId,'user_id'=>$this->user['id']]);
        return HTTP_NOCONTEND;
    }

    public function deleteAll()
    {
        admin::destroy(['user_id' =>$this->user['id']]);
        return HTTP_NOCONTEND;
    }

}
