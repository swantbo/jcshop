<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\service;

use think\facade\Cache;

class ClassifyService
{

    public function index()
    {
        Cache::store("redis")->has(checkRedisKey("MALL:CLASSIFY_LEVEL")) ?
            $level = Cache::store("redis")->get(checkRedisKey("MALL:CLASSIFY_LEVEL")) :
            $level = 2;
        Cache::store("redis")->has(checkRedisKey("MALL:CLASSIFY_TYPE")) ?
            $type = Cache::store("redis")->get(checkRedisKey("MALL:CLASSIFY_TYPE")) :
            $type = 1;
        $list = Cache::store("redis")->get(checkRedisKey("MALL:CLASSIFY"));
        if(json_encode($list) == "[]") {
            $data = [];
        } else {
            $data = $this->checkData($list, $level);
        }

        return [HTTP_SUCCESS, ["data" => $data, "level" => $level, "type" => $type]];
    }


    private function checkData($data, $level)
    {
        if ($level == 1) {
            foreach ($data as $k => $v) {
                if ($v['status'] == 0) {
                    unset($data[$k]);
                    continue;
                }
                unset($data[$k]['status'], $data[$k]['status_text'], $data[$k]['children']);
            }
            $data = array_values($data);
        } elseif ($level == 2) {
            foreach ($data as $k => $v) {
                if ($v['status'] == 0) {
                    unset($data[$k]);
                    continue;
                }
                unset($data[$k]['status'], $data[$k]['status_text']);
                foreach ($data[$k]['children'] as $i => $j) {
                    if ($j['status'] == 0) {
                        unset($data[$k]['children'][$i]);
                        continue;
                    }
                    unset($data[$k]['children'][$i]['status'], $data[$k]['children'][$i]['status_text'], $data[$k]['children'][$i]['children']);
                }
                $data[$k]['children'] = array_values($data[$k]['children']);
            }
            $data = array_values($data);
        } elseif ($level == 3) {
            foreach ($data as $k => $v) {
                if ($v['status'] == 0) {
                    unset($data[$k]);
                    continue;
                }
                unset($data[$k]['status'], $data[$k]['status_text']);
                foreach ($data[$k]['children'] as $i => $j) {
                    if ($j['status'] == 0) {
                        unset($data[$k]['children'][$i]);
                        continue;
                    }
                    unset($data[$k]['children'][$i]['status'], $data[$k]['children'][$i]['status_text']);
                    foreach ($data[$k]['children'][$i]['children'] as $x => $y) {
                        if ($y['status'] == 0) {
                            unset($data[$k]['children'][$i]['children'][$x]);
                            continue;
                        }
                        unset($data[$k]['children'][$i]['children'][$x]['status'], $data[$k]['children'][$i]['children'][$x]['status_text'], $data[$k]['children'][$i]['children'][$x]['children']);
                    }
                    $data[$k]['children'][$i]['children'] = array_values($data[$k]['children'][$i]['children']);
                }
                $data[$k]['children'] = array_values($data[$k]['children']);
            }
            $data = array_values($data);
        }

        return $data;
    }
}
