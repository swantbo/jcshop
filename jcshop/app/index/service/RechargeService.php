<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\service;

use app\index\model\ActivityRecharge;
use app\index\model\Coupon;

class RechargeService
{
    /**
     * 查询列表
     * @param int $page
     * @param int $size
     * @param array $data
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function reward()
    {
        $list = ActivityRecharge::where(['status' => 1])->select();
        foreach ($list as $key => $value) {
            $coupon = json_decode($value->coupon, true);
            $list[$key]['coupon'] = Coupon::where('id', "in", $coupon)->select();
        }
        return [HTTP_SUCCESS, $list];
    }

}
