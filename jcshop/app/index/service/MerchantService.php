<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\service;

use app\index\model\Merchant as admin;
use app\utils\TrimData;
use think\model\Relation;

class MerchantService
{

    public function getMerchantAndCommodityByIds($ids)
    {
        $list = admin::with(['commodity' => function (Relation $query) {
            $query->field('id,merchant_id,name,master,sell_price')
                ->hidden(['merchant_id'])
                ->withLimit(3);
        }])
            ->field("id,name,logo")
            ->where(['id' => $ids])
            ->withCount(['commodity' => function (Relation $query) {
                $query->where(['status' => 1]);
            }])
            ->select();
        return [HTTP_SUCCESS, $list];
    }


    public function findAll(array $data, int $page = 1, int $size = 10)
    {

        $admin = admin::field('id,name,mobile,district,address,longitude,latitude,remark,content,business_license,type,is_self');
        $admin = TrimData::searchDataTrim($admin, $data, ['name']);

        unset($data['name']);
        $list = $admin->where($data)->paginate(['page' => $page, 'list_rows' => $size]);
        return [HTTP_SUCCESS, $list];
    }


    public function read(int $id)
    {
        $model = admin::field('id,name,mobile,district,address,longitude,latitude,remark,content,business_license,type,logo,customer_service_type,customer_service_mobile,is_self,wx_customer')
            ->find($id)
            ->append(['customerService'])
            ->hidden(['customer_service_mobile']);
        return [HTTP_SUCCESS, $model];
    }

}
