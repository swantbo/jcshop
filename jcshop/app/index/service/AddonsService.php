<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\service;

use app\index\model\Addons;
use think\facade\Db;

class AddonsService
{

    public function index()
    {
        global $mid;
        $addonsList = Db::table("jiecheng_lessee")
            ->where("id", $mid)
            ->value("addons");
        if ($addonsList){
            $addonsList = array_unique(json_decode($addonsList, true));
        }
        return [HTTP_SUCCESS, $addonsList];
//        $list = Addons::withoutGlobalScope(['status'])->field('addons_id')->select();
//        if ($list){
//            $list = $list->toArray();
//            $list = array_column($list, 'addons_id');
//        }
//
//        return [HTTP_SUCCESS, $list];
    }

}
