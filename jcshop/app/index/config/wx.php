<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


return [
    "code_url"=>'https://api.weixin.qq.com/sns/jscode2session?',
    "accesstoken_url"=>'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&',
    "local_url"=>'https://open.weixin.qq.com/connect/oauth2/authorize?',
    "oauth2_token_url"=>'https://api.weixin.qq.com/sns/oauth2/access_token?',
    "userinfo_url"=>'https://api.weixin.qq.com/sns/userinfo?',
];