<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\util;


use think\helper\Str;

class CreateNo
{
    public static function buildTransfersNo()
    {
        $order_no = "TR".Str::random(29);
        return $order_no;
    }

    public static function buildOrderNo($id)
    {
        $order_no = self::msectime() . $id . rand(10000, 99999);
        return $order_no;
    }

    public static function msectime()
    {
        list($msec, $sec) = explode(' ', microtime());
        return (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);
    }

}