<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\util;


use app\utils\RedisUtil;
use think\facade\Env;


class Redis
{
    private static $_instance;

    private function __construct()
    {

    }

    public function __clone()
    {
        trigger_error('Clone is not allow!', E_USER_ERROR);
    }

    public static function getInstance(array $options = [])
    {
        if (!empty($options)) {
            $options = array_merge($options, ['password' => env('redis.redispassword', null)]);
            self::$_instance = new RedisUtil($options);
        } elseif (!(self::$_instance instanceof RedisUtil))
            self::$_instance = new RedisUtil(['password' => env('redis.redispassword', null)]);
        return self::$_instance;
    }
}