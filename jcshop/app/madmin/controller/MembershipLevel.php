<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types = 1);

namespace app\madmin\controller;

use app\madmin\service\ServiceFactory;
use app\utils\Addons;
use think\App;
use think\Exception;
use think\Request;

class MembershipLevel extends BaseAction
{

    public function __construct(App $app)
    {
        parent::__construct($app);
    }


    public function findAll()
    {
        $page = $this->request->get('page');
        $size = $this->request->get('size');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service',__FUNCTION__,
            $this->data, $page, $size);
        return json(['msg' => $msg], $code);
    }


    public function save(Request $request)
    {
        $check = Addons::check($this->user->mall_id, 20);
        if (!$check)
            throw new Exception("请先激活插件", HTTP_NOTACCEPT);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $request->post());
        return json(['msg' => $msg], $code);
    }


    public function read(int $id)
    {
        $check = Addons::check($this->user->mall_id, 20);
        if (!$check)
            throw new Exception("请先激活插件", HTTP_NOTACCEPT);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }

 
    public function update(int $id)
    {
        $check = Addons::check($this->user->mall_id, 20);
        if (!$check)
            throw new Exception("请先激活插件", HTTP_NOTACCEPT);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service',
            __FUNCTION__, $id, $this->request->put());
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }


    public function delete(int $id)
    {
        $code = ServiceFactory::getResult(class_basename($this) . 'Service',
            __FUNCTION__, $id);
        return response()->code((int)$code);
    }

    public function setUserLevel(int $id){
        $check = Addons::check($this->user->mall_id, 20);
        if (!$check)
            throw new Exception("请先激活插件", HTTP_NOTACCEPT);
        $level = input('level');
        $level_time = input('level_time',null);
        $password = input('password');
        try{
            $code = ServiceFactory::getResult(class_basename($this).'Service',__FUNCTION__,$id,$level,$level_time,$password);
        }catch (\Exception $e){
            return json(['msg' => $e->getMessage()], $e->getCode());
        }
        return json(['msg' => "变更会员等级成功"], HTTP_SUCCESS);
    }


    public function contentList(int $page, int $size){
        $check = Addons::check($this->user->mall_id, 20);
        if (!$check)
            throw new Exception("请先激活插件", HTTP_NOTACCEPT);
        list($code,$list) = ServiceFactory::getResult(class_basename($this).'Service',__FUNCTION__,$page,$size,input('post.'));
        return json(['msg' => $list],$code);
    }

    public function contentRead(int $id){
        $check = Addons::check($this->user->mall_id, 20);
        if (!$check)
            throw new Exception("请先激活插件", HTTP_NOTACCEPT);
        list($code,$list) = ServiceFactory::getResult(class_basename($this).'Service',__FUNCTION__,$id);
        return json(['msg' => $list],$code);
    }
    
    public function contentSave(){
        $check = Addons::check($this->user->mall_id, 20);
        if (!$check)
            throw new Exception("请先激活插件", HTTP_NOTACCEPT);
        $post = input('post.');
        list($code,$data) = ServiceFactory::getResult(class_basename($this).'Service',__FUNCTION__,$post);
        return json(['msg' => $data],$code);
    }
    
    public function contentUpdate(int $id){
        $check = Addons::check($this->user->mall_id, 20);
        if (!$check)
            throw new Exception("请先激活插件", HTTP_NOTACCEPT);
        $put = input('put.');
        list($code,$data) = ServiceFactory::getResult(class_basename($this).'Service',__FUNCTION__,$id,$put);
        return json(['msg' => $data],$code);
    }

    public function contentDelete(int $id){
        $check = Addons::check($this->user->mall_id, 20);
        if (!$check)
            throw new Exception("请先激活插件", HTTP_NOTACCEPT);
        $code = ServiceFactory::getResult(class_basename($this).'Service',__FUNCTION__,$id);
        return response()->code((int)$code);
    }
}
