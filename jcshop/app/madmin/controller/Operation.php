<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types = 1);

namespace app\madmin\controller;

use app\madmin\service\ServiceFactory;
use think\facade\Cache;
use think\facade\Config;
use think\facade\Env;

/**
 * 登录退出
 * @package app\madmin\controller
 */
class Operation extends BaseAction
{


    public function login()
    {
        $ip = getIp();
        $username = request()->post('username');
        $password = request()->post('password');

        list($code, $msg) = ServiceFactory::getResult('OperationService','check',
            $ip,$username,$password);
        return json(['msg' => $msg], $code);
    }


    public function logout()
    {
        global $token;
        Cache::store('redis')->delete("mall_token:" . $token);
        return json(['msg'=>"缓存已清除"],HTTP_SUCCESS);
    }

}
