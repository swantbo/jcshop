<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\controller;

use app\index\service\ServiceFactory;
use app\madmin\service\LesseeService;
use app\utils\Addons;
use app\utils\LiveErrorCode;
use app\utils\Wechat;
use app\utils\Weixin1;
use app\utils\WeixinApp;
use GuzzleHttp\Client;
use think\App;
use think\Exception;
use think\facade\Db;
use think\facade\Request;
class Weixin extends BaseAction
{

    //创建小程序接口
    public function save()
    {

        $check = Addons::check($this->user->mall_id, 28);
        if (!$check)
            throw new Exception("请先激活快速注册企业小程序插件", HTTP_NOTACCEPT);

        $data = request()->post();
        $res = WeixinApp::create(json_encode($data, JSON_UNESCAPED_UNICODE));


        if($res['errcode']!=0){
            #因为微信错误码都是不相同的所以可以直接公用
            $err_msg = LiveErrorCode::getError($res);
            return json(['error_msg'=>$err_msg ?? ''],HTTP_NOTACCEPT);
        }
        return json(['msg'=>"成功"],HTTP_SUCCESS);
    }

    public function listen_event($id){
        try{
            $weixin= new \app\utils\Weixin($id);
            $weixin->component_verify_ticket();
        }catch (\Exception $e){
            #dd($e->getMessage());
        }
        return 'success';
        return "<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[success]]></return_msg></xml>";

    }
    public function listen_msg($id,$appid){

        $postdata=file_get_contents('php://input');
        $path2=public_path()."556677.txt";
        $path3=public_path()."debugha.txt";
        if (!empty(\request()->post())){
            file_put_contents($path2,\request()->url().PHP_EOL.json_encode(\request()->post().PHP_EOL,FILE_APPEND));
        }
        if (!empty($postdata)){
            file_put_contents($path3, \request()->url().PHP_EOL.json_encode($postdata).PHP_EOL,FILE_APPEND);
        }
        return 'success';
        return "<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[success]]></return_msg></xml>";
    }

}
