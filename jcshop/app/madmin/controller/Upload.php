<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\controller;


use app\madmin\service\ServiceFactory;
use think\exception\ValidateException;
use think\facade\Filesystem;
class Upload extends BaseAction
{
    public function images()
    {
        $files = request()->file();
        try {
            validate(['images' => 'fileSize:1024|fileExt:jpg,jpeg,gif,png,webp,bmp|fileMime:image/jpeg,image/gif,image/png,image/webp,image/bmp'])
                ->check($files);
        } catch (ValidateException $e) {
            return json(['msg' => $e->getMessage()], HTTP_NOTACCEPT);
        }
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . "Service", __FUNCTION__, $files);
        return json(['msg' => $msg], $code);

    }
    public function licenseTxt()
    {
        $file = request()->file("license");
        if ($_FILES['license']['type'] != 'text/plain')
            throw new \think\Exception('上传文件类型错误',HTTP_NOTACCEPT);
        $savename = Filesystem::disk('license')->putFileAs( '/', $file,$file->getOriginalName());
        return json(['msg' => $savename], HTTP_SUCCESS);
    }


}