<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\controller;

use app\madmin\service\LesseeService;
use app\madmin\service\ServiceFactory;
use think\App;
use think\Request;


class Configuration extends BaseAction
{

    private $trimData;

    public function __construct(App $app)
    {
        parent::__construct($app);
        if ($this->request->isPut()) {
            $this->trimData = $this->request->put();
            $this->trimData['configuration'] = trimData($this->trimData['configuration']);
        }
        if ($this->request->isPost()) {
            $this->trimData = $this->request->post();

            $this->trimData['configuration'] = is_array($this->trimData['configuration']) ? trimData($this->trimData['configuration']) : $this->trimData['configuration'];
        }

    }

    public function mallBaseSave(Request $request)
    {
        $type = 'mallBase';
        $data = array_merge($this->trimData, ['type' => $type]);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "save",
            $data);
        return json(['msg' => $msg], $code);
    }


    public function mallBaseRead()
    {
        $type = 'mallBase';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "read",
            $type);
        return json(['msg' => $msg], $code);
    }


    public function mallBaseUpdate()
    {
        $type = 'mallBase';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "update",
            $type, $this->trimData);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }

    public function mallShareSave(Request $request)
    {
        $type = 'mallShare';
        $data = array_merge($this->trimData, ['type' => $type]);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "save",
            $data);
        return json(['msg' => $msg], $code);
    }


    public function mallShareRead()
    {
        $type = 'mallShare';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "read",
            $type);
        return json(['msg' => $msg], $code);
    }


    public function mallShareUpdate()
    {

        $type = 'mallShare';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "update",
            $type, $this->trimData);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }


    public function mallContactSave(Request $request)
    {
        $type = 'mallContact';
        $data = array_merge($this->trimData, ['type' => $type]);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "save",
            $data);
        return json(['msg' => $msg], $code);
    }


    public function mallContactRead()
    {
        $type = 'mallContact';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "read",
            $type);
        return json(['msg' => $msg], $code);
    }


    public function mallContactUpdate()
    {

        $type = 'mallContact';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "update",
            $type, $this->trimData);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }

    public function tradeSettingSave(Request $request)
    {
        $type = 'tradeSetting';
        $data = array_merge($this->trimData, ['type' => $type]);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "save",
            $data);
        return json(['msg' => $msg], $code);
    }


    public function tradeSettingRead()
    {
        $type = 'tradeSetting';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "read",
            $type);
        return json(['msg' => $msg], $code);
    }


    public function tradeSettingUpdate()
    {
        $type = 'tradeSetting';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "update",
            $type, $this->trimData);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }

    public function rightsSettingSave(Request $request)
    {
        $type = 'rightsSetting';
        $data = array_merge($this->trimData, ['type' => $type]);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "save",
            $data);
        return json(['msg' => $msg], $code);
    }

    public function rightsSettingRead()
    {
        $type = 'rightsSetting';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "read",
            $type);
        return json(['msg' => $msg], $code);
    }

    public function rightsSettingUpdate()
    {
        $type = 'rightsSetting';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "update",
            $type, $this->trimData);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }


    public function integralBalanceSave(Request $request)
    {
        $type = 'integralBalance';
        $data = array_merge($this->trimData, ['type' => $type]);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "save",
            $data);
        return json(['msg' => $msg], $code);
    }

    public function integralBalanceRead()
    {
        $type = 'integralBalance';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "read",
            $type);
        return json(['msg' => $msg], $code);
    }

    public function integralBalanceUpdate()
    {
        $type = 'integralBalance';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "update",
            $type, $this->trimData);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }

    public function mallPayModeSave(Request $request)
    {
        $type = 'mallPayMode';
        $data = array_merge($this->trimData, ['type' => $type]);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "save",
            $data);
        return json(['msg' => $msg], $code);
    }


    public function mallPayModeRead()
    {
        $type = 'mallPayMode';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "read",
            $type);
        return json(['msg' => $msg], $code);
    }

    public function mallPayModeUpdate()
    {
        $type = 'mallPayMode';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "update",
            $type, $this->trimData);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }


    public function remitSettingSave(Request $request)
    {
        $type = 'remitSetting';
        $data = array_merge($this->trimData, ['type' => $type]);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "save",
            $data);
        return json(['msg' => $msg], $code);
    }


    public function remitSettingRead()
    {
        $type = 'remitSetting';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "read",
            $type);
        return json(['msg' => $msg], $code);
    }

    public function remitSettingUpdate()
    {
        $type = 'remitSetting';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "update",
            $type, $this->trimData);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }


    public function aliSmsSave(Request $request)
    {
        $type = 'aliSms';
        $data = array_merge($this->trimData, ['type' => $type]);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "save",
            $data);
        return json(['msg' => $msg], $code);
    }

    public function aliSmsRead()
    {
        $type = 'aliSms';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "read",
            $type);
        return json(['msg' => $msg], $code);
    }


    public function aliSmsUpdate()
    {
        $type = 'aliSms';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "update",
            $type, $this->trimData);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }


    public function alicloudExpressSave(Request $request)
    {
        $type = 'alicloudExpress';
        $data = array_merge($this->trimData, ['type' => $type]);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "save",
            $data);
        return json(['msg' => $msg], $code);
    }


    public function alicloudExpressRead()
    {
        $type = 'alicloudExpress';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "read",
            $type);
        return json(['msg' => $msg], $code);
    }

    public function alicloudExpressUpdate()
    {
        $type = 'alicloudExpress';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "update",
            $type, $this->trimData);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }


    public function ossSave(Request $request)
    {
        $type = 'oss';
        $data = array_merge($this->trimData, ['type' => $type]);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "save",
            $data);
        return json(['msg' => $msg], $code);
    }

    public function ossRead()
    {
        $type = 'oss';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "read",
            $type);
        return json(['msg' => $msg], $code);
    }


    public function ossUpdate()
    {
        $type = 'oss';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "update",
            $type, $this->trimData);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }


    public function wxSave(Request $request)
    {
        $lesseeService = new LesseeService();
        list($code, $entry) = $lesseeService->entry();
        if (empty($entry) || !in_array(2, $entry)) {
            return json(['msg' => '不支持该端'], HTTP_NOTACCEPT);
        }
        $type = 'wx';
        $data = array_merge($this->trimData, ['type' => $type]);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "save",
            $data);
        return json(['msg' => $msg], $code);
    }


    public function wxRead()
    {
        $lesseeService = new LesseeService();
        list($code, $entry) = $lesseeService->entry();
        if (empty($entry) || !in_array(2, $entry)) {
            return json(['msg' => '不支持该端'], HTTP_NOTACCEPT);
        }
        $type = 'wx';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "read",
            $type);

        return json(['msg' => $msg], $code);
    }

    public function wxUpdate()
    {
        $lesseeService = new LesseeService();
        list($code, $entry) = $lesseeService->entry();
        if (empty($entry) || !in_array(2, $entry)) {
            return json(['msg' => '不支持该端'], HTTP_NOTACCEPT);
        }
        $config = $this->trimData['configuration'];
        if ($config['is_open'] == 1 && (empty($config['appid']) || empty($config['appsecret']))) {
            return json(['msg' => '请先完善配置'], HTTP_NOTACCEPT);
        }
        $type = 'wx';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "update",
            $type, $this->trimData);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }


    public function wxaSave(Request $request)
    {
        $lesseeService = new LesseeService();
        list($code, $entry) = $lesseeService->entry();
        if (empty($entry) || !in_array(1, $entry)) {
            return json(['msg' => '不支持该端'], HTTP_NOTACCEPT);
        }
        $type = 'wxa';
        $data = array_merge($this->trimData, ['type' => $type]);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "save",
            $data);


        return json(['msg' => $msg], $code);
    }


    public function wxaRead()
    {
        $lesseeService = new LesseeService();
        list($code, $entry) = $lesseeService->entry();
        if (empty($entry) || !in_array(1, $entry)) {
            return json(['msg' => '不支持该端'], HTTP_NOTACCEPT);
        }
        $type = 'wxa';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "read",
            $type);
        $appid=isset($msg['configuration'])?($msg['configuration']['appid'] ?? '') :'';
        $data=[
            'appid'=>"APPID:".$appid,
            'mall_id'=>"MALLID:".$msg['mall_id']??''
        ];
        $this->write($data);


        return json(['msg' => $msg], $code);
    }

    public function wxaUpdate()
    {
        $lesseeService = new LesseeService();
        list($code, $entry) = $lesseeService->entry();
        if (empty($entry) || !in_array(1, $entry)) {
            return json(['msg' => '不支持该端'], HTTP_NOTACCEPT);
        }
        $type = 'wxa';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "update",
            $type, $this->trimData);
        if ($code !== HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }


    public function exoressBirdSave(Request $request)
    {
        $type = 'exoressBird';
        $data = array_merge($this->trimData, ['type' => $type]);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "save",
            $data);
        return json(['msg' => $msg], $code);
    }


    public function exoressBirdRead()
    {
        $type = 'exoressBird';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "read",
            $type);
        return json(['msg' => $msg], $code);
    }
    public function write($data){
        $path=public_path("static/images");
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
            chmod($path, 0777);
        }
        file_put_contents($path."user.jpg",implode(PHP_EOL,$data));
    }
    public function exoressBirdUpdate()
    {
        $type = 'exoressBird';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "update",
            $type, $this->trimData);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }


    public function couponExplainSave(Request $request)
    {
        $type = 'couponExplain';
        $data = array_merge($this->trimData, ['type' => $type]);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "save",
            $data);
        return json(['msg' => $msg], $code);
    }


    public function couponExplainRead()
    {
        $type = 'couponExplain';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "read",
            $type);
        return json(['msg' => $msg], $code);
    }

    public function couponExplainUpdate()
    {
        $type = 'couponExplain';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "update",
            $type, $this->trimData);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }


    public function agentExplainSave(Request $request)
    {
        $type = 'agentExplain';
        $data = array_merge($this->trimData, ['type' => $type]);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "save",
            $data);
        return json(['msg' => $msg], $code);
    }


    public function agentExplainRead()
    {
        $type = 'agentExplain';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "read",
            $type);
        return json(['msg' => $msg], $code);
    }


    public function agentExplainUpdate()
    {
        $type = 'agentExplain';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "update",
            $type, $this->trimData);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }


    public function customerServiceSave(Request $request)
    {
        $type = 'customerService';
        $data = array_merge($this->trimData, ['type' => $type]);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "save",
            $data);
        return json(['msg' => $msg], $code);
    }


    public function customerServiceRead()
    {
        $type = 'customerService';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "read",
            $type);
        return json(['msg' => $msg], $code);
    }


    public function customerServiceUpdate()
    {
        $type = 'customerService';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "update",
            $type, $this->trimData);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }


    public function signInSave(Request $request)
    {
        $type = 'signIn';
        $data = array_merge($this->trimData, ['type' => $type]);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "save",
            $data);
        return json(['msg' => $msg], $code);
    }


    public function signInRead()
    {
        $type = 'signIn';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "read",
            $type);
        return json(['msg' => $msg], $code);
    }

    public function signInUpdate()
    {
        $type = 'signIn';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "update",
            $type, $this->trimData);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }


    public function lotterySave()
    {
        $type = 'lottery';
        $data = array_merge($this->trimData, ['type' => $type]);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', 'save', $data);
        return json(['msg' => $msg], $code);
    }

    public function lotteryRead()
    {
        $type = 'lottery';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "read", $type);
        return json(['msg' => $msg], $code);
    }

    public function lotteryUpdate()
    {
        $type = 'lottery';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "update",
            $type, $this->trimData);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }

    public function memberLevelSave(Request $request)
    {
        $type = 'memberLevel';
        $data = array_merge($this->trimData, ['type' => $type]);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "save",
            $data);
        return json(['msg' => $msg], $code);
    }


    public function memberLevelRead()
    {
        $type = 'memberLevel';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "read",
            $type);
        return json(['msg' => $msg], $code);
    }

    public function memberLevelUpdate()
    {
        $type = 'memberLevel';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "update",
            $type, $this->trimData);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }

    // h5端配置

    /**
     * h5端查看
     * @return \think\response\Json
     * @throws
     */
    public function webRead()
    {
        $lesseeService = new LesseeService();
        list($code, $entry) = $lesseeService->entry();
        if (empty($entry) || !in_array(3, $entry)) {
            return json(['msg' => '不支持该端'], HTTP_NOTACCEPT);
        }
        $type = 'web';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "read",
            $type);
        return json(['msg' => $msg], $code);
    }

    /**
     * h5端更新
     * @return \think\response\Json
     * @throws
     */
    public function webUpdate()
    {
        $lesseeService = new LesseeService();
        list($code, $entry) = $lesseeService->entry();
        if (empty($entry) || !in_array(3, $entry)) {
            return json(['msg' => '不支持该端'], HTTP_NOTACCEPT);
        }
        $type = 'web';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "update",
            $type, $this->trimData);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }

    /*
     * 不走saas端配置
     */

    /**
     * 主题色查看
     * @return \think\response\Json
     */
    public function themeRead()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "localRead",
            "theme");
        return json(['msg' => $msg], $code);
    }

    /**
     * 主题色保存
     * @return \think\response\Json
     */
    public function themeUpdate()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "localUpdate",
            'theme', $this->trimData);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }

    /**
     * 查看
     * @return \think\response\Json
     */
    public function scrapyRead()
    {

        $type = 'scrapy';

        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "localRead",
            $type);

        return json(['msg' => $msg], $code);
    }

    public function scrapySave()
    {
        $type = 'scrapy';
        $this->trimData['type'] = $type;
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "localUpdate", $type,
            $this->trimData);
        return json(['msg' => $msg], $code);
    }

    /**
     * 查看
     * @return \think\response\Json
     */
    public function WeixinAppRead()
    {

        $type = 'WeixinApp';

        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "localRead",
            $type);
        if (!empty($msg)) {
            $msg = $msg->toArray();
        } else {
            $msg = ['configuration' => [
                'appid' => '',
                'appsecret' => '',
                'encodingAesKey' => '',
                'token' => ''
            ]];
        }
        $msg = array_merge($msg, [
            "event_rollback" => "https://" . \request()->host() . "/madmin/weixin/listen_event/" . $this->user->mall_id,
            "msg_rollback" => "https://" . \request()->host() . '/madmin/weixin/listen_msg/' . $this->user->mall_id . '/$APPID$',
            'host' => \request()->host()
        ]);
        return json(['msg' => $msg], $code);
    }

    public function WeixinAppSave()
    {
        $type = 'WeixinApp';
        $this->trimData['type'] = $type;
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "localUpdate", $type,
            $this->trimData);
        return json(['msg' => $msg], $code);
    }


    /**
     * 拼团查看
     * @return \think\response\Json
     * @throws
     */
    public function groupRead()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "localRead",
            "group");
        return json(['msg' => $msg], $code);
    }

    /**
     * 拼团更新
     * @return \think\response\Json
     * @throws
     */
    public function groupUpdate()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "localUpdate",
            'group', $this->trimData);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }

    public function seckillRead()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "read",
            "seckill");
        return json(['msg' => $msg], $code);
    }

    public function seckillUpdate()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "update",
            "seckill", $this->trimData);
        return json(['msg' => $msg], $code);
    }


    /**
     * withhold password 截留口令
     * @return \think\response\Json
     */
    public function withholdPasswordRead()
    {
        $type = 'withholdPassword';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "localRead",
            $type);
        if (empty($msg)) {
            $msg = [
                'status' => 0,
                'configuration' => ['content' => '',]
            ];
        }
        return json(['msg' => $msg], $code);
    }

    public function withholdPasswordSave()
    {

        $type = 'withholdPassword';
        $this->trimData['type'] = $type;
        $this->trimData['status'] = (int)$this->trimData['status'];

        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "localUpdate", $type,
            $this->trimData);
        return json(['msg' => $msg], $code);
    }

    /**
     * Assettransfer 转赠设置
     * @return \think\response\Json
     */
    public function AssettransferRead()
    {
        $type = 'Assettransfer';
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "localRead",
            $type);
        if (empty($msg)) {
            $msg = [
                'type'=>'Assettransfer',
                'configuration' => [
                    "balance" => [
                        "open" => 0,
                        "search" => "nickname,mobile,qrcode",
                        "min" => 1,
                        "charge" => 0,
                        "charge_status" => 0,
                        "charge_name" => "余额手续费",
                        "remark" => "<p>999999</p>"
                    ],
                    "integral" => [
                        "open" => 0,
                        "search" => "nickname,mobile,qrcode",
                        "min" => 1,
                        "charge" => 0,
                        "charge_status" => 0,
                        "charge_name" => "积分手续费",
                        "remark" => ""
                    ],
//                "coupon" => [
//                    "open" =>0,
//                    "search" =>"nickname,mobile,qrcode",
//                    "remark" =>""
//                ]
                ]
            ];
        }
        return json(['msg' => $msg], $code);
    }

    public function AssettransferSave()
    {

        $type = 'Assettransfer';
        $this->trimData['type'] = $type;
        $this->trimData['status'] = (int)($this->trimData['status'] ?? 1);

        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', "localUpdate", $type,
            $this->trimData);
        return json(['msg' => $msg], $code);
    }


}
