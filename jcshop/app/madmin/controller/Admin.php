<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\controller;

use app\madmin\service\ServiceFactory;
use think\Exception;
use think\Request;


class Admin extends BaseAction
{


    public function findAll()
    {
        $page = $this->request->get('page');
        $size = $this->request->get('size');
        list($code, $msg) = ServiceFactory::getResult('OperationService', 'findAll',
            $this->data, $page, $size);
        return json(['msg' => $msg], $code);
    }


    public function save(Request $request)
    {
        $post = $request->post();
        $dischargedArr = [
            'root{10,1000}',
            "administrator",
            "system",
            'guest{0,1000}',
            123123,
            123456,
            123456789,
            1234567,
            'admin{0,1000}',
            456456,
            789789,
        ];

        if (in_array($post['username'], $dischargedArr)){
            throw new Exception("账号不可用",HTTP_NOTACCEPT);
        }
        $post['password'] = hash('sha256', $post['password']);
        list($code, $msg) = ServiceFactory::getResult('OperationService', 'save', $post);
        return json(['msg' => $msg], $code);
    }


    public function read($id)
    {
        list($code, $msg) = ServiceFactory::getResult('OperationService', 'read', $id);
        return json(['msg' => $msg], $code);
    }


    public function update($id)
    {
        $put = $this->request->put();
        $dischargedArr = [
            'root{10,1000}',
            "administrator",
            "system",
            'guest{0,1000}',
            123123,
            123456,
            123456789,
            1234567,
            'admin{0,1000}',
            456456,
            789789,
        ];

        if (in_array($put['username'], $dischargedArr)){
            throw new Exception("账号不可用",HTTP_NOTACCEPT);
        }
        if (isset($put['password'])) {
            $put['password'] = hash('sha256', $put['password']);
        }
        list($code, $msg) = ServiceFactory::getResult('OperationService',
            'update', $id, $put);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }


    public function delete(int $id)
    {
        $code = ServiceFactory::getResult('OperationService',
            __FUNCTION__, $id);
        return response()->code($code);
    }


    public function selfUp($id)
    {
        $put = $this->request->put();
        list($code, $msg) = ServiceFactory::getResult('OperationService', __FUNCTION__, $id, $put);
        return json(['msg' => $msg], $code);
    }
}
