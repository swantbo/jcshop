<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\controller;


use app\madmin\service\ServiceFactory;
use Psr\SimpleCache\InvalidArgumentException;
use think\App;
use think\facade\Cache;

class FloatMenu extends BaseAction
{

    /**
     * 悬浮菜单
     * @return \think\response\Json 数据
     */
    public function getConfig()
    {

        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, json_decode($this->user, true));
        return json(['msg' => $msg], $code);
    }

    /**
     * 悬浮菜单保存
     * showdoc
     * @catalog 悬浮菜单保存-创建配置
     * @title 创建配置
     * @description 创建配置的接口
     * @method post
     * @url PickGoods/getConfig
     * @return "data":[{"page_name":"daasddsadsa","page_type":2,"status":2,"small_img":"tertertretre","qrcode":"D:\/phpstudy_pro\/WWW\/jinzhe\/app\/qrcode\/5fab8a554b957.png","update_time":"2020-11-11 14:53:09"},{"page_name":"商城首页","page_type":1,"status":1,"small_img":"http:\/\/jingzhe.com\/storage\/index\/20201023\/58fef29a8c301731aff1debc6d71f4eb.webp","qrcode":"D:\/phpstudy_pro\/WWW\/jinzhe\/app\/qrcode\/5fade9ff695b6.png","update_time":"2020-11-13 10:05:52"}]}}
     * @return_param msg string 对象信息,报错则为错误信息
     * @number 1
     */
    public function save()
    {
        $data = input('post.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data, json_decode($this->user, true));
        return json(['msg' => $msg], $code);
    }
}