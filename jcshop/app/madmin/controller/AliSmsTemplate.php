<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\controller;

use app\madmin\service\ServiceFactory;
use app\utils\Addons;
use think\App;
use think\Exception;
use think\exception\InvalidArgumentException;
use think\facade\Config;
use think\Request;

class AliSmsTemplate extends BaseAction
{

    private $scene;

    public function __construct(App $app)
    {
        parent::__construct($app);
        $check = Addons::check($this->user->mall_id, 4);
        if (!$check)
            throw new Exception("请先激活插件", HTTP_NOTACCEPT);
        if (in_array(request()->action(), ['save', 'update'])) {
            $this->scene = Config::load('extension/scene', 'scene');
        }
    }


    public function index()
    {
        $scene = input("get.scene");
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . "Service", __FUNCTION__,$scene);
        return json(['msg' => $msg], $code);
    }

    public function findAll()
    {
        $page = $this->request->get('page');
        $size = $this->request->get('size');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . "Service", __FUNCTION__,
            $this->data, $page, $size);
        return json(['msg' => $msg], $code);
    }

    public function save(Request $request)
    {
        $post = $request->post();
        //数据校验
        $this->checkData($post);

        //提交
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . "Service", __FUNCTION__, $post);
        return json(['msg' => $msg], $code);
    }

    private function checkData($data)
    {
        $sceneAttr = $this->scene[$data['scene']];

        if (isset($data['attr'])) {
            foreach ($data['attr'] as $v) {
                if (!isset($sceneAttr[$v])) {
                    throw new InvalidArgumentException("商城变量未定义", HTTP_INVALID);
                }
            }
        }
    }


    public function read($id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . "Service", __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }


    public function update($id)
    {
        $put = $this->request->put();
        $this->checkData($put);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . "Service", __FUNCTION__,
            $id, $put);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }


    public function delete(int $id)
    {
        $code = ServiceFactory::getResult(class_basename($this) . "Service", __FUNCTION__,
            $id);
        return response()->code($code);
    }
}
