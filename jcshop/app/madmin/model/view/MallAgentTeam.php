<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\model\view;


use app\madmin\model\BaseModel;

class MallAgentTeam extends BaseModel
{

    protected $table = 'jiecheng_mall_agent_team';

    public function getSumFigureAttr($value){
        return sprintf('%.2f',$value);
    }

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (isset($data['status'])){
            self::append(['status_text']);
        }
    }

    public function getStatusTextAttr($value,$data)
    {
        $status = [
            0 => '未启用',
            1 => '正常',
            2 => '过期',
            3 => '限制',
            4 => '删除'
        ];
        return $status[$data['status']];
    }
}