<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\model;
use think\Model;

class Template extends Model
{
    protected $hidden = ['page_key'];

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (isset($data['page_type'])) self::append(['page_type_text']);
    }

    public function getPageTypeTextAttr($value, $data)
    {
        // 1-商城首页 2-商品详情 3-会员中心 4-自定义页面
        $arr = [1 => "商城首页", 2 => "商品详情", 3 => "会员中心", 4 => "自定义页面"];
        return $arr[$data['page_type']];
    }
}