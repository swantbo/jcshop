<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types = 1);

namespace app\madmin\model;

use think\db\Query;
use think\Model;

class AgentApply extends BaseModel
{

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (isset($data['status'])){
            self::append(['status_text']);
        }
    }

    public function getStatusTextAttr($value,$data)
    {
        $status = [0=>'申请中',1=>'通过',2=>'不通过'];
        return $status[$data['status']];
    }


    public function user()
    {
        return $this->hasOne(Admin::class, 'id', 'user_id')->bind(['user_name'=>'name']);
    }

    public function agent()
    {
        return $this->hasOne(Agent::class, 'id', 'pid')->bind(['p_name'=>'name']);
    }
}
