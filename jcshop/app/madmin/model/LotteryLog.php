<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\model;

use think\Model;

class LotteryLog extends Model
{
    protected $autoWriteTimestamp = 'datetime';
    public function User(){
        return $this->hasOne(User::class,'id','user_id');
    }

    public function Integral(){
        return $this->hasOne(IntegralRecord::class,'order_id','id');
    }

    public function prize(){
        return $this->hasOne(LotteryPrize::class,'uniqid','uniqid');
    }

    public function Logistics(){
        return $this->hasOne(LotteryLogistics::class,'lottery_log_id','id');
    }
}