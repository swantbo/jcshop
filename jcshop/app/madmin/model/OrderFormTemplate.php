<?php


namespace app\index\model;


use think\Model;
use think\model\concern\SoftDelete;

class OrderFormTemplate extends Model
{
    use SoftDelete;
}