<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\model;

use think\db\Query;
use think\Model;
use think\model\concern\SoftDelete;
use think\facade\Db;

class Agent extends BaseModel
{
    use SoftDelete;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (isset($data['status'])){
            self::append(['status_text']);
        }
    }

    public function getStatusTextAttr($value,$data)
    {
        $status = [
            0 => '未启用',
            1 => '正常',
            2 => '过期',
            3 => '限制',
            4 => '删除'
        ];
        return $status[$data['status']];
    }

    public function user()
    {
        return $this->hasOne(Admin::class, 'id', 'user_id')->bind(['user_name'=>'name']);
    }

    public function agent()
    {
        return $this->hasOne(Agent::class, 'id', 'pid')->bind(['p_name'=>'name']);
    }
    public function grade()
    {
        return $this->hasOne(AgentGrade::class, 'id', 'grade_id');
    }
    /**
     * 获取分销商的绑定用户
     * @param $id int 分销商id
     * @param $mid
     * @return int 下级用户人数
     */
    public static function getUsers($id, $mid)
    {
        $users = Db::name("user")
            ->where('pid', $id)
            ->where('mall_id', $mid)
            ->where('delete_time', null)
            ->count();
        return $users;
    }

    /**
     * 所得佣金
     * @param $id int 分销商id
     * @param $mid
     * @return float 获取的佣金总额
     */
    public static function getCommissions($id, $mid)
    {
        $commission = Db::name("agent_bill_temporary")
            ->where('agent_id', $id)
            ->where('status', 1)
            ->where('mall_id', $mid)
            ->where('delete_time', null)
            ->sum("amount");
        return $commission;
    }

    /**
     * 已提现佣金总额
     * @param $user_id int
     * @param $id int
     * @param $mid int
     * @return float 已提现佣金总额
     * @throws
     */
    public static function getDraw($user_id, $id, $mid)
    {
//        $withdraw = Db::name('agent_withdraw')
//            ->field("withdraw")
//            ->where('agent_id', $id)
//            ->where('status', 1)
//            ->where('mall_id', $mid)
//            ->where('delete_time', null)
//            ->sum("withdraw");
//        $service_charge =Db::name('agent_withdraw')
//            ->field("service_charge")
//            ->where('agent_id', $id)
//            ->where('status', 1)
//            ->where('mall_id', $mid)
//            ->where('delete_time', null)
//            ->sum("service_charge");
        $draw = 0;
        $draws = Db::name("user_cash")
            ->field("agent_withdraw")
            ->where('user_id', $user_id)
            ->where('status', 1)
            ->where('mall_id', $mid)
            ->where('delete_time', null)
            ->find();
        if ($draws) {
            $draw = $draws['agent_withdraw'];
        }
        return $draw;
    }

    /**
     * 下级用户消费金额
     * @param $id int 分销商id
     * @param $mid
     * @return float
     */
    public static function getPayment($id, $mid)
    {
        $money = Db::name("agent_bill_temporary")
            ->where('agent_id', $id)
            ->where('status', 1)
            ->where('mall_id', $mid)
            ->where('delete_time', null)
            ->sum("money");
        return $money;
    }
    /**
     * 下级用户订单数
     * @param $id int 分销商id
     * @param $mid
     * @return float
     */
    public static function getOrderCount($id, $mid)
    {
        $count = Db::name("agent_bill_temporary")
            ->Distinct(true)->field('order_no')
            ->where('agent_id', $id)
            ->where('status', 1)
            ->where('mall_id', $mid)
            ->where('delete_time', null)
            ->count();
        return $count;
    }
    /**
     * 下级分销商人数
     * @param $id int 分销商id
     * @param $mid
     * @return int
     */
    public static function getAgents($id, $mid)
    {
        $agents = Db::name("agent")
            ->where('pid', $id)
            ->where('status', 1)
            ->where('mall_id', $mid)
            ->where('delete_time', null)
            ->count();
        return $agents;
    }
}
