<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\model;

use think\Model;
use think\facade\Config;

class Configuration extends BaseModel
{

    protected $pk = 'type';

    protected $json = ['configuration'];
    protected $jsonAssoc = true;
    public static $integralBalance = '{"share": {"new": 0, "old": 0}, "is_open": 0, "withdraw": 1, "deduction": 1, "daily_limit": 0, "is_free_fee": 0, "balance_cash": 0.1, "balance_text": "余额", "integral_text": "积分", "withdraw_type": ["1"], "agent_withdraw": 1, "integral_upper": 0, "service_charge": "6.5", "withdraw_limit": 0, "balance_setting": 0, "free_fee_section": ["1", "10"], "withdraw_service": 0, "agent_is_free_fee": 0, "get_integral_type": 0, "merchant_withdraw": 1, "get_integral_ratio": 1, "agent_withdraw_type": ["2", "1"], "withdraw_limit_cash": "100.00", "agent_service_charge": 5.5, "agent_withdraw_limit": 0, "integral_upper_limit": 0, "merchant_is_free_fee": 0, "withdraw_type_outline": ["2", "1", "3"], "agent_free_fee_section": [0, 0], "agent_withdraw_service": 0, "merchant_withdraw_type": ["1", "2"], "merchant_service_charge": 5.5, "merchant_withdraw_limit": 0, "agent_withdraw_limit_cash": "100.00", "merchant_free_fee_section": [0, 0], "merchant_withdraw_service": 0, "agent_withdraw_type_outline": ["1", "2", "3"], "merchant_withdraw_limit_cash": "100.00", "merchant_withdraw_type_outline": ["1", "2", "3"]}';
    public static $signIn = '{"info": "<p></p>", "type": 1, "style": {}, "value": "1", "signIn": false, "pattern": "day", "special": [], "is_remind": false, "continuity": [], "cumulative": []}';
    public static $tradeSetting = '{"invoice":["1","2"],"deal_enhance":1,"audit_evaluate":1,"order_evaluate":1,"order_is_close":1,"receiving_time":"7","is_show_evaluate":1,"order_close_time":"20","is_auto_receiving":1,"agent_get_money_time":0}';
    private function setConfigurationToFile(string $type, array $data)
    {
        $configuration = Config::get($type);

        $fopen = fopen("../config/{$type}.php", 'r+');
        try {
            if (flock($fopen, LOCK_EX)) {
                ftruncate($fopen, 0);
                $str = "<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */" . "\r\n" . "return [" . "\r\n";
                foreach ($data as $k => $v) {
                    $str .= "\t\"$k\"=>" . "\"$v\"," . "\r\n";
                }
                $str .= "];";
                fwrite($fopen, $str);
                flock($fopen, LOCK_UN);
            }
            fclose($fopen);
        } catch (\Exception $e) {
            fclose($fopen);
        }
    }

}
