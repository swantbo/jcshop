<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\model;

use think\model\concern\SoftDelete;

/**
 * 优惠券
 * @package app\madmin\model
 */
class RechargeRecord extends BaseModel
{
    use SoftDelete;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id')
            ->field('id,nickname,picurl,name,mobile,agent_id,merchant_id,pid');
    }

}
