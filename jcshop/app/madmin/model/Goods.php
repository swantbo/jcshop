<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\model;

use app\shop_admin\model\GroupCommodity;
use app\shop_admin\model\Merchant;
use app\shop_admin\model\Sku;
use think\db\Query;
use think\Model;
use think\model\concern\SoftDelete;

class Goods extends Model
{
    protected $name="commodity";
    protected $table="jiecheng_commodity";

    public function sku()
    {
        return $this->hasMany(Sku::class, 'commodity_id', 'id')->hidden(['commodity_id']);
    }

    public function liveGood(){
        return $this->hasOne(\app\shop_admin\model\LiveGood::class,'goods_id','id');
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class, 'merchant_id', 'id');
    }

    public function groupCommodity()
    {
        return $this->hasMany(GroupCommodity::class, 'commodity_id', 'id');
    }
}
