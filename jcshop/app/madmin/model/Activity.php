<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types = 1);

namespace app\madmin\model;

use think\Exception;
use app\madmin\validate\RechargeValidate;
use think\model\concern\SoftDelete;

class Activity extends BaseModel
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
//    protected $globalScope = 'mallId';

    private static $rules = [
        'single_recharge' => '0', // 充值金额
        'rewards_type' => '[1, 2]', // 1优惠券，2积分，3余额
        'balance' => '0', // 赠送余额
        'coupon' => '[ids]', // 优惠券ID
        'credit' => '0', // 优惠券ID
    ];

    private static function getRules($rules)
    {
        $w = array_diff(self::$rules, array_intersect_key(self::$rules, $rules));
        if (count($w) > 0) {
            throw new Exception(implode(",", array_keys($w)) . "不可为空", HTTP_NOTACCEPT);
        }
        if (is_array($rules['rewards_type'])) {
            $rules['rewards_type'] = json_encode($rules['rewards_type']);
        }
        if (is_array($rules['coupon'])) {
            $rules['coupon'] = json_encode($rules['coupon']);
        }
         return $rules;
    }

    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }

    public function recharge()
    {
        return $this->hasMany(ActivityRecharge::class, "aid", "id");
    }

    /**
     * @param $data
     * @return mixed
     * @throws
     */
    public static function check($data, $id = 0)
    {
        // 活动状态
        $data['status'] = 1;
        $data['type'] = 1;
        $date = date("Y-m-d H:i:s", time());
        if ($data['end_time'] < $date) {
            // $data['status'] = 2;
            throw new Exception("结束时间必须在".$date."之后！", HTTP_NOTACCEPT);
        } else if ($data['start_time'] > $date) {
            $data['status'] = 0;
        }
        // 是否冲突时间
        $activities = Activity::where('type', 1)->where([['status', 'in', [0,1]]])->where('id', "<>", $id)->select();
        foreach ($activities as $key => $value) {
            if ($data['start_time'] <= $value['start_time'] && $data['end_time'] >= $value['start_time']) {
                throw new Exception("“".$value['start_time']."-".$value['end_time']."”时间冲突！", HTTP_NOTACCEPT);
            }
            if ($data['start_time'] >= $value['start_time'] && $data['end_time'] <= $value['end_time']) {
                throw new Exception("“".$value['start_time']."-".$value['end_time']."”时间冲突！", HTTP_NOTACCEPT);
            }
            if ($data['start_time'] <= $value['end_time'] && $data['end_time'] >= $value['end_time']) {
                throw new Exception("“".$value['start_time']."-".$value['end_time']."”时间冲突！", HTTP_NOTACCEPT);
            }
        }
        // 验证规则设置
        $rules = [];
        foreach ($data['rules'] as $key => $value) {
            $value['status'] = $data['status'];
            $value['end_time'] = $data['end_time'];
            $rules[$key] = self::getRules($value);
        }
        // 处理数据
        $data['rules'] = json_encode($data['rules']);
        return ['rules' => $rules, 'activity' => $data];
    }
}
