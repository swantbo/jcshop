<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\service;

use app\madmin\model\Log as admin;
use app\utils\TrimData;

class LogService
{

    /**
     * @param array $data
     * @param int $page
     * @param int $size
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function list(int $page = 1, int $size = 10)
    {
        $data=[];
        $admin = admin::field("id,mall_id,action,title,create_time,url,code,platform,ip,admin_name,method");
        $admin = TrimData::searchDataTrim($admin, $data, ['title']);
        $list = $admin->where($data)->order('create_time','desc')->paginate(['page' => $page, 'list_rows' => $size]);
        return [HTTP_SUCCESS, $list];
    }
}
