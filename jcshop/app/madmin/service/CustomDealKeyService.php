<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\service;


use app\madmin\model\Configuration;
use think\Exception;
use think\facade\Cache;
use WeChat\Contracts\BasicWeChat;

class CustomDealKeyService
{
    protected $access_token,$user;
    public function __construct(){
        global $user;
        $this->user = $user;
        $this->access_token = $this->getAccessToken();

    }

    public function getAccessToken(){
        $token = Cache::get(checkRedisKey('wxa_access_token'));
        if (empty($token)) {
            $config = Configuration::where('mall_id', $this->user['mall_id'])
                ->where('type', 'wxa')
                ->value('configuration');
            if (empty($config))
                throw new Exception('没有配置小程序', HTTP_INVALID);
            $this->access_token = BasicWeChat::instance($config)->getAccessToken();
            Cache::set(checkRedisKey('wxa_access_token'),$this->access_token,7000);
        }else{
            $this->access_token = $token;
        }
    }

    public function register(){
        $url = 'https://api.weixin.qq.com/shop/register/apply?access_token='.$this->access_token;
        $result = GetHttpResult($url,[]);

    }

    public function access(){


    }

    public function upload(){

    }

    public function getList(){

    }

    public function getFind(){

    }
}