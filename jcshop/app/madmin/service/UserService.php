<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\service;

use app\index\model\UserWx;
use app\madmin\model\User as admin;
use app\madmin\model\UserCoupon;
use app\madmin\model\view\UserTotalCount;
use app\utils\TrimData;
use think\facade\Cache;

class UserService
{

    /**
     * 查询列表
     * @param int $page
     * @param int $size
     * @param array $data
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function findAll(array $data, int $page = 1, int $size = 10)
    {

        $admin = admin::with(['cash','wxType'])->field('id,nickname,picurl,name,REPLACE(mobile,SUBSTR(mobile,4,4),\'****\') AS mobile,status,create_time,update_time');
        $admin = TrimData::searchDataTrim($admin, $data, ['name', 'mobile', 'nickname', 'begin_date', 'end_date']);
        $list = $admin->where('is_merge IS NULL')->order("update_time", "DESC")->paginate(['page' => $page, 'list_rows' => $size]);
        foreach ($list as $key => $value) {
            if (is_null($value['type'])) {
                $list[$key]['type'] = 3;
            }
        }
        return [HTTP_SUCCESS, $list];
    }

    /**
     * 读取一条数据
     * @param int $id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function read(int $id)
    {
        $model = admin::with(['cash','wxType'])->find($id);
        if (is_null($model['type'])) {
            $model['type'] = 3;
        }
        $couponCount = UserCoupon::where(['user_id' => $id, 'status' => 0])->count('id');
        $model->coupon = $couponCount;
        return [HTTP_SUCCESS, $model];
    }


    /**
     * 更新数据
     * @param int $id
     * @param array $data
     * @return array
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function update(int $id, array $data)
    {
        $update_time = $data['update_time'];
        unset($data['update_time']);
        $admin = admin::update($data, ['update_time' => $update_time, 'id' => $id]);
        $user_wx = UserWx::where('user_id', $admin['id'])->find();
//        $str = $user_wx['type'] = 1 ? "TOKEN:" . $admin['id'] : checkRedisKey("TOKEN:" . $id);
//        $token = Cache::store('redis')->get($str);
//        if ($data['status'] == 2) Cache::store('redis')->delete("INDEX:" . $token . "-" . $user_wx['mall_id']);
        return [HTTP_CREATED, $admin];
    }

    /**
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        admin::destroy($id);
        return HTTP_NOCONTEND;
    }

    public function total(int $id): array
    {
        $total = UserTotalCount::where('user_id', $id)
            ->find();
        return [HTTP_SUCCESS, $total];
    }
}
