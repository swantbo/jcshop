<?php


namespace app\madmin\service;

use app\madmin\model\OrderFormTemplate as template;
use app\madmin\model\OrderFormAnswer AS answer;
use think\Exception;

class OrderFormService
{
    protected $user;
    public function __construct(){
        global $user;
        $this->user = $user;
    }

    /**
     * 创建自定义表单模板
     * @param string $title
     * @param string $config
     * @param int $tries_limit
     * @return array
     */
    public function create(string $title, string $config, int $source,string $image) : array{
        $template = template::create([
            'title' => $title,
            'config' => $config,
            'image' => $image,
            'source'=>$source,
            'create_time' => date('Y-m-d H:i:s'),
            'update_time' => date('Y-m-d H:i:s'),
            'mall_id' => $this->user['mall_id'],
            'merchant_id' => $this->user['merchant_id'],
            'status' => 1
        ]);
        if (!empty($template->id))
            return [HTTP_SUCCESS,$template];
        return [HTTP_INVALID,'创建表单失败'];
    }

    /**
     * 更新自定义模板
     * @param int $id
     * @param string $title
     * @param string $config
     * @param int $tries_limit
     * @return array
     * @throws Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function update(int $id, array $data)  : array {
        $template = template::find($id);

        if (empty($template))
            return [HTTP_INVALID,'模板不存在'];
        if ($template->source==0 && $data['status']==1){
            template::where(['source'=>0])->where('id','<>',$id)->update(['status'=>0]);
        }
        $template->save($data);
        return [HTTP_SUCCESS,template::find($id)];
    }

    public function status($data){
        $id=$data['id'];
        $status=$data['status'];
        $template = template::find($id);
        if (empty($template))
            return [HTTP_INVALID,'模板不存在'];
        if(!in_array($status,[0,1])){
            return [HTTP_INVALID,'参数异常'];
        }
        if ($template->source==0 && $status==1){
            template::where(['source'=>0])->where('id','<>',$id)->update(['status'=>0]);
        }
        $template->status=$status;
        $template->save();
        return [HTTP_SUCCESS,'变更成功'];
    }

    /**
     * 删除自定义模板
     * @param int $id
     * @return array
     * @throws Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function delete(int $id) : array{
        $template = template::find($id);
        if (empty($template))
            throw new Exception('模板不存在',HTTP_INVALID);
        $is_bool = $template->delete();
        if ($is_bool)
            return [HTTP_SUCCESS,'删除成功'];
        else
            return [HTTP_INVALID,'删除失败'];
    }

    /**
     * 自定义表单列表
     * @param int $page
     * @param int $size
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function findAll(int $page, int $size) : array{
        $where=[];
        $source=request()->get('source');
        $status=request()->get('status');
        $title=request()->get('title');
        if(is_numeric($source)) $where[]=['source','=',$source];
        if(is_numeric($status)) $where[]=['status','=',$status];
        if($title) $where[]=['title','like',"%{$title}%"];

        $list = template::field('id,title,image,source,status,config')
            ->where('mall_id',$this->user['mall_id'])
            ->where($where)
            ->withCount('answers')
            ->order('create_time DESC')
            ->paginate(['page' => $page,'list_rows' => $size]);
        return [HTTP_SUCCESS,$list];
    }

    /**
     *
     * @param int $id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function read(int $id) : array{
        $read = template::find($id);
        return [HTTP_SUCCESS,$read];
    }

    /**
     * 答案列表
     * @param int $template_id
     * @param int $page
     * @param int $size
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function answerList(int $order_id, int $page, int $size) : array{

        $where=['order_id'=>$order_id];
        $goods_id=request()->get('commodity_id');
        if($goods_id){
            $where['commodity_id']=$goods_id;
        }
        $list = answer::where($where)
            ->hidden(['delete_time'])
            ->order('id DESC')
            ->select();
//            ->paginate(['page' => $page,'list_size' => $size]);

        return [HTTP_SUCCESS,$list];
    }
    public function templateAnswerList($id,int $page,int $size){
        $where=['template_id'=>$id];
        $order_no=input('order_no');
        $nickname=input('nickname');
        $where['template_id']=$id;
        $query = answer::where($where)
            ->hidden(['delete_time'])
            ->order('id DESC')
            ->with(['user','order']);
        if($nickname){
            $query->haswhere('user',function($query) use ($nickname){
                $query->where('nickname','like',"%{$nickname}%");
            });
        }
        if($order_no){
            $query->haswhere('order',function($query) use ($order_no){
                $query->where('order_no','like',"%{$order_no}%");
            });
        }
        $list=$query->paginate(['page' => $page,'list_size' => $size]);

        return [HTTP_SUCCESS,$list];
    }
    public function answerInfo($id){
        $answer= answer::with(['user','order'])->find($id);
        return [HTTP_SUCCESS,$answer];
    }
}