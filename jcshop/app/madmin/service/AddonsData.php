<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\service;

use app\madmin\model\Addons;

class AddonsData
{

    public static function is_support_installd($addons_id)
    {
        $list = [4, 6, 7, 8, 9, 15, 17, 18,29, 33];
        if (in_array($addons_id, $list)) {
            return false;
        }
        if (!is_numeric($addons_id) || $addons_id < 1 || $addons_id > 40) {
            return false;
        }
        return true;
    }

    public static function removeAll()
    {

        $list = [1, 2, 3, 5, 10, 11, 12, 13, 14, 16, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 30, 31, 32, 34, 35, 36];
        $service = new AddonsService();

        foreach ($list as $addons_id) {
            try {
                $service->uninstall($addons_id);
            } catch (\Exception $e) {
                continue;
            }
        }
    }

    //新人专属
    public function addons_1()
    {
        return [
            'app/index/model/NewKidGift.php',
            'app/index/model/NewKidGiftConfig.php',
            'app/index/service/NewKidGiftService.php',
            'app/madmin/controller/NewKidGift.php',
            'app/madmin/model/NewKidGift.php',
            'app/madmin/model/NewKidGiftConfig.php',
            'app/madmin/service/NewKidGiftService.php',
        ];
    }

    //文章
    public function addons_2()
    {
        return [
            'app/index/controller/Article.php',
            'app/index/controller/ArticleType.php',
            'app/index/model/Article.php',
            'app/index/model/ArticleType.php',
            'app/index/service/ArticleService.php',
            'app/index/service/ArticleTypeService.php',
            'app/madmin/controller/Article.php',
            'app/madmin/controller/ArticleType.php',
        ];
    }

    //优惠券
    public function addons_3()
    {
        return [
            'app/index/controller/Coupon.php',
            'app/index/controller/UserCoupon.php',
//            'app/index/model/Coupon.php',
//            'app/index/model/UserCoupon.php',
            'app/index/service/CouponService.php',
            'app/index/service/UserCouponService.php',
            'app/madmin/controller/Coupon.php',
            'app/madmin/controller/UserCoupon.php',
            'app/madmin/model/Coupon.php',
//            'app/madmin/model/UserCoupon.php',
            'app/madmin/service/CouponService.php',
            'app/madmin/service/UserCouponService.php',
        ];
    }

    //消息通知
    public function addons_4()
    {
        return;
    }

    //签到
    public function addons_5()
    {
        return [
            'app/index/controller/SignIn.php',
            'app/index/model/SignIn.php',
            'app/index/service/SignInService.php',
            'app/madmin/controller/SignIn.php',
            'app/madmin/model/SignIn.php',
            'app/madmin/service/SignInService.php',
        ];
    }

    //积分规划
    public function addons_6()
    {
        return;
    }

    //抵扣设置
    public function addons_7()
    {
        return;
    }

    //远程附件
    public function addons_8()
    {
        return;
    }

    //腾讯智服
    public function addons_9()
    {
        return;
    }

    //小票助手
    public function addons_10()
    {
        return [
//            'app/index/model/ReceiptsTask.php',
//            'app/index/model/ReceiptsTemplate.php',
//            'app/index/service/ReceiptsService.php',
            'app/index/util/Receipts.php',
//            'app/shop_admin/model/ReceiptsPrinter.php',
            'app/shop_admin/model/ReceiptsTask.php',
            'app/shop_admin/model/ReceiptsTemplate.php',
            'app/shop_admin/service/ReceiptsService.php',
            'app/shop_admin/validate/ReceiptsValidate.php',
        ];
    }

    //门店
    public function addons_11()
    {
        return [
            'app/index/controller/Accept.php',
            'app/index/controller/AcceptRecord.php',
//            'app/index/controller/Store.php',
            'app/index/controller/Verification.php',
            'app/index/service/AcceptRecordService.php',
            'app/index/service/AcceptService.php',
            'app/index/service/StoreService.php',
            'app/index/service/VerificationService.php',
            'app/madmin/controller/Store.php',
            'app/madmin/service/StoreService.php',
            'app/shop_admin/controller/AcceptRecord.php',
            'app/shop_admin/controller/ShopAssistant.php',
            'app/shop_admin/controller/Store.php',
            'app/shop_admin/model/AcceptRecord.php',
            'app/shop_admin/model/ShopAssistant.php',
            'app/shop_admin/service/AcceptRecordService.php',
            'app/shop_admin/service/ShopAssistantService.php',
            'app/shop_admin/service/StoreService.php',
            'app/shop_admin/validate/ShopAssistantValidate.php',
            'app/shop_admin/validate/StoreValidate.php',
        ];
    }

    //商户
    public function addons_12()
    {
        return [
            'app/index/controller/Merchant.php',
            'app/index/controller/MerchantApply.php',
//            'app/index/model/Merchant.php',
            'app/index/model/MerchantApply.php',
            'app/index/model/MerchantCash.php',
            'app/index/service/MerchantApplyService.php',
            'app/madmin/controller/Merchant.php',
            'app/madmin/controller/MerchantApply.php',
//            'app/madmin/model/Merchant.php',
            'app/madmin/model/MerchantApply.php',
            'app/madmin/model/MerchantCash.php',
            'app/madmin/service/MerchantApplyService.php',
            'app/madmin/service/MerchantService.php',
//            'app/madmin/service/MerchantSettlementService.php',
//            'app/madmin/service/MerchantWithdrawService.php',
        ];
    }

    //自定义表单
    public function addons_13()
    {
        return [
            'app/index/controller/Form.php',
            'app/index/model/FormAnswer.php',
            'app/index/model/FormTemplate.php',
            'app/index/service/FormService.php',
            'app/madmin/controller/Form.php',
            'app/madmin/model/FormAnswer.php',
            'app/madmin/model/FormTemplate.php',
            'app/madmin/service/FormService.php',
        ];
    }

    //快递助手
    public function addons_14()
    {
        return [
            'app/shop_admin/controller/Invoice.php',
            'app/shop_admin/controller/InvoiceManage.php',
            'app/shop_admin/model/Invoice.php',
            'app/shop_admin/model/InvoiceTemplate.php',
//            'app/shop_admin/service/InvoiceManageService.php',
            'app/shop_admin/service/InvoiceService.php',
        ];
    }

    //消费返积分
    public function addons_15()
    {
        return;
    }

    //同城配送
    public function addons_16()
    {
        return [
            'app/index/controller/Distribution.php',
            'app/index/model/Distribution.php',
            'app/shop_admin/controller/DaDa.php',
            'app/shop_admin/controller/Distribution.php',
            'app/shop_admin/model/Dada.php',
            'app/shop_admin/model/Distribution.php',
        ];
    }

    //九宫格抽奖
    public function addons_17()
    {
        return;
    }

    //刮刮乐抽奖
    public function addons_18()
    {
        return;
    }

    //积分商城
    public function addons_19()
    {
        return [
            'app/index/controller/IntegralShop.php',
//            'app/index/model/IntegralGoods.php',
            'app/index/model/IntegralOrder.php',
            'app/index/model/IntegralOrderCommodity.php',
//            'app/index/model/IntegralRecord.php',
            'app/index/model/IntegralSku.php',
            'app/index/model/IntegralSkuInventory.php',
//            'app/index/service/IntegralService.php',
            'app/index/service/IntegralShopSerivce.php',
            'app/madmin/controller/IntegralShop.php',
            'app/madmin/model/IntegralAttrName.php',
            'app/madmin/model/IntegralAttrValue.php',
            'app/madmin/model/IntegralClassify.php',
            'app/madmin/model/IntegralGoods.php',
            'app/madmin/model/IntegralOrder.php',
            'app/madmin/model/IntegralOrderCommodity.php',
//            'app/madmin/model/IntegralRecord.php',
            'app/madmin/model/IntegralSku.php',
            'app/madmin/model/IntegralSkuInventory.php',
            'app/madmin/service/IntegralShopSerivce.php',
        ];
    }

    //会员中心
    public function addons_20()
    {
        return [
            'app/index/controller/MembershipLevel.php',
            'app/index/controller/MembershipRecord.php',
            'app/index/model/MembershipLevel.php',
            'app/index/model/MembershipLevelContent.php',
            'app/index/model/MembershipLevelContentRelation.php',
            'app/index/service/MembershipLevelService.php',
            'app/index/service/MembershipRecordService.php',
//            'app/madmin/controller/MembershipLevel.php',
            'app/madmin/controller/MembershipRecord.php',
//            'app/madmin/model/MembershipLevel.php',
            'app/madmin/model/MembershipLevelContent.php',
            'app/madmin/model/MembershipLevelContentRelation.php',
//            'app/madmin/service/MembershipLevelService.php',
            'app/madmin/service/MembershipRecordService.php',
//            'app/madmin/validate/MembershipLevelValidate.php',
        ];
    }

    //猜你喜欢
    public function addons_21()
    {
        return [
            'app/madmin/controller/Pick.php',
            'app/madmin/service/PickService.php',
        ];
    }

    //微信客服
    public function addons_22()
    {
        return [
            'app/index/controller/WxCs.php',
            'app/index/model/WxCompany.php',
            'app/index/model/WxCustomer.php',
            'app/index/service/WxCsService.php',
            'app/madmin/controller/WxCompany.php',
            'app/madmin/controller/WxCustomer.php',
            'app/madmin/model/WxCompany.php',
            'app/madmin/model/WxCustomer.php',
            'app/madmin/service/WxCompanyService.php',
            'app/madmin/service/WxCustomerService.php',
            'app/shop_admin/controller/WxCs.php',
        ];
    }

    #系统表单
    public function addons_23()
    {
        return [
            'app/madmin/controller/OrderForm.php',
//            'app/madmin/model/OrderFormAnswer.php',
            'app/madmin/model/OrderFormTemplate.php',
            'app/madmin/service/OrderFormService.php',
//            'app/index/controller/OrderForm.php',
//            'app/index/model/OrderFormAnswer.php',
//            'app/index/model/OrderFormTemplate.php',
//            'app/index/model/OrderFormTemplate.php',
        ];
    }


    #限时拼团
    public function addons_24()
    {
        return [
            'app/madmin/controller/Collage.php',
            'app/madmin/controller/Group.php',
            'app/madmin/model/Collage.php',
            'app/madmin/model/CollageItem.php',
            'app/madmin/model/Group.php',
            'app/madmin/model/GroupCommodity.php',
            'app/madmin/model/GroupCommoditySku.php',
            'app/madmin/service/CollageService.php',
            'app/madmin/service/GroupService.php',
            'app/madmin/validate/GroupValidate.php',
            'app/shop_admin/model/Group.php',
            'app/shop_admin/model/GroupCommodity.php',
        ];
    }

    #限时秒杀
    public function addons_25()
    {
        return [
            'app/madmin/controller/Seckill.php',
            'app/madmin/model/Seckill.php',
            'app/madmin/model/SeckillCommodity.php',
            'app/madmin/model/SeckillCommoditySku.php',
            'app/madmin/service/SeckillService.php',
            'app/madmin/validate/SeckillValidate.php',
            'app/shop_admin/model/Seckill.php',
            'app/shop_admin/model/SeckillCommodity.php',
        ];
    }

    #商品预售
    public function addons_34()
    {
        return [
            'app/index/model/Presell.php',
            'app/index/model/PresellCommodity.php',
            'app/index/model/PresellCommoditySku.php',
            'app/madmin/controller/Presell.php',
            'app/madmin/model/Presell.php',
            'app/madmin/model/PresellCommodity.php',
            'app/madmin/model/PresellCommoditySku.php',
            'app/madmin/service/PresellService.php',
            'app/madmin/validate/PresellValidate.php',
            'app/shop_admin/model/PresellCommodity.php',
        ];
    }

    #充值奖励
    public function addons_36()
    {
        return [
            'app/index/controller/Recharge.php',
            'app/index/model/ActivityRecharge.php',
            'app/index/service/RechargeService.php',
            'app/madmin/controller/Recharge.php',
            'app/madmin/model/Activity.php',
            'app/madmin/model/ActivityRecharge.php',
            'app/madmin/service/RechargeService.php',
            'app/madmin/validate/RechargeValidate.php',
        ];
    }

    #商品采集
    public function addons_26()
    {
        return [
            'app/madmin/model/ScrapyLog.php',
            'app/utils/scrapy/Base.php',
            'app/utils/scrapy/Jd.php',
            'app/utils/scrapy/Pdd.php',
            'app/utils/scrapy/Suning.php',
            'app/utils/scrapy/Taobao.php',
            'app/utils/scrapy/Taobao1688.php',
            'app/utils/scrapy/Tmall.php',
        ];
    }

    #小程序直播
    public function addons_27()
    {
        return [
            'app/index/controller/LiveRoom.php',
            'app/index/controller/WeixinApp.php',
//            'app/index/model/LiveGood.php',
            'app/index/model/LiveRoom.php',
            'app/index/model/LiveRoomHasGood.php',
            'app/index/service/LiveRoomService.php',
            'app/index/service/WeixinAppService.php',
            'app/madmin/controller/LiveGood.php',
            'app/madmin/controller/LiveRoom.php',
            'app/madmin/controller/LiveRoomGoods.php',
            'app/madmin/model/LiveGood.php',
            'app/madmin/model/LiveLog.php',
            'app/madmin/model/LiveRoom.php',
            'app/madmin/model/LiveRoomHasGood.php',
            'app/madmin/service/LiveGoodService.php',
            'app/madmin/service/LiveRoomService.php',
            'app/madmin/validate/WeixinAppValidate.php',
//            'app/shop_admin/model/LiveGood.php',
            'app/utils/LiveErrorCode.php',
            'app/utils/LiveGoods.php',
            'app/utils/LiveRoom.php',
//            'app/utils/Wechat.php',
        ];
    }

    #快速注册企业小程序
    public function addons_28()
    {
        return [
            'app/index/controller/WeixinApp.php',
            'app/index/service/WeixinAppService.php',
            'app/utils/WeixinApp.php',
        ];
    }

    #分销达标
    public function addons_30()
    {
        return [
            'app/index/model/CommissionActivity.php',
            'app/madmin/controller/CommissionActivity.php',
            'app/madmin/model/CommissionActivity.php',
            'app/madmin/model/CommissionActivityAward.php',
            'app/madmin/service/CommissionActivityService.php',
        ];
    }

    #分销考核
    public function addons_31()
    {
        return [
            'app/index/controller/CommissionAssessmentWithdraw.php',
//            'app/index/model/CommissionAssessment.php',
            'app/index/model/CommissionAssessmentWithdraw.php',
            'app/index/service/CommissionAssessmentWithdrawService.php',
            'app/madmin/controller/CommissionAssessment.php',
            'app/madmin/controller/CommissionAssessmentWithdraw.php',
            'app/madmin/model/CommissionAssessment.php',
            'app/madmin/model/CommissionAssessmentDegrade.php',
            'app/madmin/model/CommissionAssessmentOrder.php',
            'app/madmin/model/CommissionAssessmentWithdraw.php',
            'app/madmin/service/CommissionAssessmentService.php',
            'app/madmin/service/CommissionAssessmentWithdrawService.php',
        ];
    }

    #小程序外部跳转
    public function addons_32()
    {
        return [
            'app/madmin/controller/UrlLink.php',
            'app/madmin/model/Link.php',
            'app/madmin/model/LinkClick.php',
            'app/madmin/service/UrlLinkService.php',
        ];
    }

    #资产转赠
    public function addons_35()
    {
        return [
            'app/madmin/controller/AssettransferLog.php',
            'app/madmin/model/AssettransferLog.php',
            'app/madmin/service/AssettransferLogService.php',
            'app/index/controller/Assettransfer.php',
            'app/index/model/AssettransferLog.php',
            'app/index/service/AssettransferService.php',
            'app/index/validate/AssettransferValidate.php',
        ];
    }
}
