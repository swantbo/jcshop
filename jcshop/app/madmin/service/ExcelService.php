<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\service;


use app\madmin\model\Agent;
use app\madmin\model\User;
use PhpOffice\PhpSpreadsheet\Exception;
class ExcelService
{
    public function agentExport()
    {
        $agent = Agent::alias('a')
            ->join('agent_withdraw g', 'a.id=g.agent_id', 'LEFT')
            ->field('a.name,a.mobile,g.withdraw,a.create_time')
            ->select()
            ->toArray();
        $headArr = ['名字', '电话', '金额', '注册时间'];
        $this->excel($agent, $headArr, '代理', '代理列表');
    }

    public function userExport()
    {

        $user = User::alias('a')
            ->join('user_cash g', 'a.id=g.user_id', 'LEFT')
            ->field('a.nickname,a.picurl,IFNULL(a.name,"暂无") as name ,IFNULL(a.mobile,"暂无") as mobile,g.total,a.create_time')
            ->select()
            ->toArray();
        $headArr = ['昵称', '头像', '真实姓名', '电话', '账户总额', '注册时间'];
        $this->excel($user, $headArr, '用户', '用户列表');
    }

    private function excel($data, $headArr, $filename, $title)
    {
        try {
            $newdata = [];
            foreach ($data as $value) {
                $newdata[] = $value;
            }
            $excel = new \app\shop_admin\service\ExcelService();
            $excel->excelExport($filename, $title, $headArr, $newdata);
        } catch (Exception $e) {
            throw new \PDOException($e->getMessage(), HTTP_INVALID);
        }finally{

        }
    }
}