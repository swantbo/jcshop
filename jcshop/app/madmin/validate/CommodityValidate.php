<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\validate;


use think\Validate;

class CommodityValidate extends Validate
{
    protected $rule = [
        'page' => 'require|integer',
        'size' => 'require|integer',
        'name' => 'max:32',
        'begin_date' => 'dateFormat:Y-m-d',
        'end_date' => 'dateFormat:Y-m-d',
        'update_time' => 'require|dateFormat:Y-m-d H:i:s',
        'audit' => 'integer'
    ];
    protected $message = [
        'page.require' => '页数必传',
        'page.integer' => '页数必为整数',
        'size.require' => '显示数必传',
        'size.integer' => '显示数必须是整数',
        'name.max' => '姓名不得超过32个字符',
        'begin_date' => '开始时间必须是日期格式',
        'end_date' => '结束时间必须是日期格式',
        'update_time.require' => '更新时间不能为空',
        'update_time.dateFormat' => '更新时间必须是日期格式',
        'audit.integer' => '审核状态不正确'
    ];
    protected $scene = [
        'list' => ['page','size','name','begin_date','end_date'],
        'update' => ['update_time','audit']
    ];
}