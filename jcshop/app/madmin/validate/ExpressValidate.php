<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\validate;


use think\Validate;

class ExpressValidate extends Validate
{
    protected $rule = [
        'name' => 'require|max:32',
        'code' => 'require',
        'key' => 'require',
        'ali_key' => 'require',
        'r_datas' => 'require'
    ];
    protected $message = [
        'name' => '快递名称必填',
        'code' => '快递码必填',
        'key' => '快递key必填',
        'ali_key' => '阿里快递key必填',
        'r_datas' => '风格必填'
    ];
    protected $scene = [
        'save' => ['name', 'code', 'key', 'ali_key', 'r_datas']
    ];
}