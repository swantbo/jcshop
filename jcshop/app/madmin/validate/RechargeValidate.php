<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
namespace app\madmin\validate;

use think\Validate;

class RechargeValidate extends Validate
{
    protected $rule = [
        'page' => 'require|integer',
        'size' => 'require|integer',

        'title' => 'require|max:20',
        'start_time' => 'require|date',
        'end_time' => 'require|date',
        'client_type' => 'require',
        'rules' => 'require',

        'activity_stock' => 'require',
        'activity_price' => 'require',
        'has_sku' => 'require'
    ];
    protected $message = [
        'page' => '页数必须上传',
        'size' => '显示数量必须上传',

        'title.require' => '名称未上传',
        'title.max' => '名称超过最大限定20个字符',
        'start_time.require' => '开始时间必填',
        'end_time.require' => '结束时间必填',
        'client_type.require' => '渠道必选',

        'activity_stock' => '库存必填',
        'activity_price' => '秒杀价必填',
    ];
    protected $scene = [
        'index' => ['page', 'size'],
        'save' => ['title', 'start_time', 'end_time', 'client_type', 'rules'],
        'update' => ['end_time']
    ];
}