<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\validate;


use think\Validate;

class ArticleValidate extends Validate
{
    protected $rule = [
        'page' => 'require|integer',
        'size' => 'require|integer',
        'type' => 'require|integer',
        'title' => 'require|max:64',
        'subhead' => 'require|max:128',
        'content' => 'require'
    ];
    protected $message = [
        'page.require' => '页数必传',
        'page.integer' => '页数必为整数',
        'size.require' => '显示数必传',
        'size.integer' => '显示数必为整数',
        'type.require' => '类型未提交',
        'type.integer' => '类型错误',
        'title.require' => '标题未提交',
        'title.max' => '标题最大限定64个字符',
        'subhead.require' => '副标题未提交',
        'subhead.max' => '副标题最大限定128个字符',
        'content' => '内容未提交'
    ];
    protected $scene = [
        'list' => ['page', 'size'],
        'save' => ['type', 'title', 'subhead', 'content']
    ];
}