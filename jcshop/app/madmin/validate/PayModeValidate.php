<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\validate;


use think\Validate;

class PayModeValidate extends Validate
{
    protected $rule = [
        'name|支付模板名称'=>'require|length:1,32',
        'mode|方式'=>'require|in:1,2,3',
        'type|类型'=>'require|in:1,2,3,4',
    ];

}