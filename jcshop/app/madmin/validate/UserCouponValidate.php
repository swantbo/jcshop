<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\validate;


use think\Validate;

class UserCouponValidate extends Validate
{
    protected $rule = [
        'page|页数' => 'require|integer',
        'size|条数' => 'require|integer',
        'user_id' => 'integer',
        'coupon_id' => 'integer',
        'type' => 'integer',
        'status' => "integer",
    ];

    protected $message = [
        'user_id.integer' => "请选择正确的用户",
        'coupon_id' => "请选择正确的优惠券",
        'type' => "请选择正确的获取方式",
        'status' => "请选择正确的状态",
    ];

}