<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\validate;


use think\Validate;

class IntegralValidate extends Validate
{
    protected $rule = [
        'is_open' => 'require|in:1,0',
        'deduction' => 'require|float',
        'get_integral_ratio' => 'require|float',
        'get_integral_type' => 'require|in:0,1,2'
    ];
    protected $message = [
        'is_open.require' => '是否开启积分必须选择',
        'is_open.in' => '参数错误',
        'deduction.require' => '积分抵扣不能为空',
        'deduction.float' => '积分抵扣必须为数字',
        'get_integral_ratio.require' => '消费积分比不能为空',
        'get_integral_ratio.float' => '消费积分比必须为数字',
        'get_integral_type.require' => '积分设置配置错误',
        'get_integral_type.in' => '参数错误'
    ];
    protected $scene = [
        'update' => ['is_open','deduction','get_integral_ratio','get_integral_type'],
        'list' => ['page','size']
    ];
}