<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\validate;


use think\Validate;

class RuleValidate extends Validate
{
    protected $rule = [
        'page' => 'require|integer',
        'size' => 'require|integer',
        'name' => 'require|max:32',
        'group' => 'require|max:32',
        'resource_name' => 'require|max:16',
        'resource' => 'require|max:16',
        'action' => 'require|max:16',
        'method' => 'require|max:7',
        'update_time' => 'require|dateFormat:Y-m-d H:i:s'
    ];
    protected $message = [
        'page' => '页数必须上传',
        'size' => '显示数量必须上传',
        'name.require' => '规则名必须上传',
        'name.max' => '规则名限定再32个字符内',
        'group.require' => '分组必须上传',
        'group.max' => '分组限定再32个字符内',
        'resource_name.require' => '资源名必须上传',
        'resource_name.max' => '资源名限定再16个字符内',
        'resource.require' => '资源必须上传',
        'resource.max' => '资源限定再16个字符内',
        'action.require' => '控制器必须上传',
        'action.max' => '控制器限定在16个字符内',
        'method.require' => '方法必须上传',
        'method.max' => '方法名限定在7个字符内',
        'update_time' => '更新时间不正确'
    ];
    protected $scene = [
        'list' => ['page','size'],
        'save' => ['name','group','resource_name','resource','action','method'],
        'update' => ['update_time']
    ];
}