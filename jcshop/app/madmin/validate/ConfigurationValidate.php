<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\validate;


use think\Validate;

class ConfigurationValidate extends Validate
{

    protected $rule = [
        'update_time|更新时间' => 'require|dateFormat:Y-m-d H:i:s',

        //mallBase
        'configuration.is_open' => 'require|in:0,1',
        'configuration.cs_is_open' => 'require|in:0,1',
        'configuration.mall_name|商城名称' => 'require',
        'configuration.logo|商城主图' => 'require',
        'configuration.is_handset|强制绑定手机开关' => 'in:0,1',
        'configuration.customer_service_type|商城主图' => 'in:1,2,3',

        //tradeSetting
        'configuration.order_is_close|订单强制关闭开关按钮' => 'require|in:0,1',
        'configuration.order_close_time|订单关闭时间(分钟)' => 'integer',
        'configuration.is_auto_receiving|自动收货开关按钮' => 'require|in:0,1',
        'configuration.receiving_time|订单自动收货时间(天)' => 'integer',
        'configuration.deal_enhance|交易增强' => 'require|in:0,1',
        'configuration.invoice|发票设置' => 'require|in:1,2',
        'configuration.order_evaluate|订单评价' => 'require|in:0,1',
        'configuration.is_show_evaluate|显示评价' => 'require|in:0,1',
        'configuration.audit_evaluate|评价审核' => 'require|in:0,1',
        'configuration.order_end_time|订单完成时间(天)' => 'require|integer',
        'configuration.agent_get_money_time|分销商获得分销奖励间隔时间(天)' => 'require|integer',

        //rightsSetting
        'configuration.is_rights_apply|售后维权申请' => 'require|in:0,1',
        'configuration.rights_apply_time|维权期限(天)' => 'integer',
        'configuration.refund_single|单品退换货' => 'require|in:0,1',

        //integralBalance
        'configuration.get_integral_type|积分赠与方式' => 'require',
        'configuration.integral_text|积分替换文字' => 'chs|min:1|max:4',
        'configuration.integral_upper|积分上限按钮' => 'require|in:0,1',
        'configuration.integral_upper_limit|积分上限' => 'integer',
        'configuration.balance_text|余额替换文字' => 'chs|min:1|max:4',
        'configuration.balance_setting|充值设置' => 'require|in:0,1',
        'configuration.balance_cash|最低充值金额' => 'float',
        'configuration.withdraw|余额提现开关' => 'require|in:0,1',
        'configuration.withdraw_type|余额线上提现方式' => 'in:1',
        'configuration.withdraw_type_outline|余额线下提现方式' => 'in:1,2,3',
        'configuration.withdraw_limit|余额提现限制' => 'in:0,1',
        'configuration.withdraw_limit_cash|最低余额提现金额' => 'float',
        'configuration.withdraw_service|余额提现是否扣手续费开关' => 'in:0,1',
        'configuration.service_charge|余额提现手续费(%)' => 'float',
        'configuration.is_free_fee|余额提现是否免手续费开关' => 'in:0,1',
        'configuration.agent_withdraw|分销提现开关' => 'require|in:0,1',
        'configuration.agent_withdraw_type|分销线上提现方式' => 'in:1',
        'configuration.agent_withdraw_type_outline|分销线上提现方式' => 'in:1,2,3',
        'configuration.agent_withdraw_limit|分销提现限制' => 'in:0,1',
        'configuration.agent_withdraw_limit_cash|最低分销提现金额' => 'float',
        'configuration.agent_withdraw_service|分销提现是否扣手续费开关' => 'in:0,1',
        'configuration.agent_service_charge|分销提现手续费(%)' => 'float',
        'configuration.agent_is_free_fee|分销提现是否免手续费开关' => 'in:0,1',
        'configuration.merchant_withdraw|商户提现开关' => 'require|in:0,1',
        'configuration.merchant_withdraw_type|商户线上提现方式' => 'in:1',
        'configuration.merchant_withdraw_type_outline|商户线上提现方式' => 'in:1,2,3',
        'configuration.merchant_withdraw_limit|商户提现限制' => 'in:0,1',
        'configuration.merchant_withdraw_limit_cash|最低商户提现金额' => 'float',
        'configuration.merchant_withdraw_service|商户提现是否扣手续费开关' => 'in:0,1',
        'configuration.merchant_service_charge|商户提现手续费(%)' => 'float',
        'configuration.merchant_is_free_fee|商户提现是否免手续费开关' => 'in:0,1',

        //mallPayMode
        'configuration.wechat.wx|微信公众号:微信支付开关按钮' => 'require|in:0,1',
        'configuration.wechat.wx_pay_mode_id|微信公众号:微信支付模板' => 'integer',
        'configuration.wechat.ali|微信公众号:支付宝支付开关按钮' => 'require|in:0,1',
        'configuration.wechat.wx_pay_mode_id|微信公众号:支付宝支付模板' => 'integer',
        'configuration.wechat.recharge|微信公众号:余额支付开关按钮' => 'require|in:0,1',
        'configuration.wechat.recharge|微信公众号:货到付款开关按钮' => 'require|in:0,1',
        'configuration.wechata.wx|微信小程序:微信支付开关按钮' => 'require|in:0,1',
        'configuration.wechata.wx_pay_mode_id|微信小程序:微信支付模板' => 'integer',
        'configuration.wechata.recharge|微信小程序:余额支付开关按钮' => 'require|in:0,1',
        'configuration.wechata.recharge|微信小程序:货到付款开关按钮' => 'require|in:0,1',
        'configuration.web.wx|浏览器H5:微信支付开关按钮' => 'require|in:0,1',
        'configuration.web.wx_pay_mode_id|浏览器H5:微信支付模板' => 'integer',
        'configuration.web.ali|浏览器H5:支付宝支付开关按钮' => 'require|in:0,1',
        'configuration.web.wx_pay_mode_id|浏览器H5:支付宝支付模板' => 'integer',
        'configuration.web.recharge|浏览器H5:余额支付开关按钮' => 'require|in:0,1',
        'configuration.web.sendPay|浏览器H5:货到付款开关按钮' => 'require|in:0,1',

        //remitSetting
        'configuration.ali_remit|支付宝打款开关按钮' => 'require|in:0,1',
        'configuration.ali_remit_pay_mode_id|支付宝打款模板' => 'integer',
        'configuration.wx_remit|微信打款开关按钮' => 'require|in:0,1',
        'configuration.wx_remit_wechat_pay_mode_id|微信公众号打款模板' => 'integer',
        'configuration.wx_remit_wechata_pay_mode_id|微信小程序打款模板' => 'integer',
        'configuration.wx_remit_commission|佣金打款' => 'in:1,2',
        'configuration.wx_remit_withdraw|提现申请' => 'in:1,2',
        'configuration.wx_remit_packet|红包金额' => 'in:188,288,388',

        //aliSms
        'configuration.accessKeyId|阿里云短信key' => 'require',
        'configuration.accessSecret|阿里云短信secret' => 'require',

        //wx
        'configuration.appid|appid' => 'require',
        'configuration.appsecret|appsecret' => 'require',
//        'configuration.URL|URL' => 'require|url',
        // 'configuration.Token|Token' => 'require|min:3|max:32',
        'configuration.EncodingAESKey|EncodingAESKey' => 'require|length:43',

        //exoressBird
        'configuration.appId|快递鸟appId' => 'require',
        'configuration.key|快递鸟key' => 'require',

        //agentExplain
        'configuration.open|分销开关' => 'require|in:0,1',
        'configuration.level|分销层级' => 'require|in:0,1,2,3',
        'configuration.inPurchasing|分销内购' => 'require|in:0,1',
        'configuration.is_show_profit|是否在商品页面显示分销佣金' => 'require|in:0,1,2',
        'configuration.profit.first|一级分销比例' => 'float',
        'configuration.profit.second|二级分销比例' => 'float',
        'configuration.profit.third|三级分销比例' => 'float',
        'configuration.become.type|成为分销商条件' => 'require|in:0,1,2,3,4',
        'configuration.isAudit|是否需要审核' => 'require|in:0,1',
        'configuration.relationship|成为下级条件' => 'require|in:0,1',
//        'configuration.isShowProfit|佣金显示' => 'require|in:0,1',

        'configuration.lottery_open|是否开启' => 'require|in:0,1',
        'configuration.lottery_config_id|抽奖的配置' => 'require|integer',
        'configuration.lottery_type|抽奖类型' => 'require|in:1,2',
        'configuration.lottery_limit|抽奖上限' => 'require|integer',
        'configuration.lottery_money|满足条件的金额' => 'number',
        'configuration.lottery_number|赠送的抽奖次数' => 'require|integer',
        'configuration.superposition|是否叠加次数' => 'require|in:1,2'

//        'configuration'
    ];

    protected $message = [

        //mallBase
        'configuration.is_open.in' => "请传入正确的开启关闭值",
        'configuration.is_handset.in' => "请传入正确的强制绑定手机开关值",
        'configuration.customer_service_type.in' => "请传入正确的强制使用官方客服开关值",

        //tradeSetting
        'configuration.order_is_close.in' => "请选择正确的订单强制关闭按钮开关值",
        'configuration.is_auto_receiving.in' => "请选择正确的订单自动收货按钮开关值",


    ];

    protected $scene = [

        'mallBaseSave' => [
            'configuration.is_open',
            'configuration.mall_name',
            'configuration.logo',
            'configuration.is_handset',
            'configuration.customer_service_type',
        ],
        'mallBaseUpdate' => [
            'update_time',
            'configuration.is_open',
            'configuration.mall_name',
            'configuration.logo',
            'configuration.is_handset',
            'configuration.customer_service_type',
        ],
        'tradeSettingSave' => [
            'configuration.order_is_close',
            'configuration.order_close_time',
            'configuration.is_auto_receiving',
            'configuration.receiving_time',
            'configuration.deal_enhance',
            'configuration.order_end_time',
            //'configuration.invoice',
            'configuration.order_evaluate',
            'configuration.receiving_time',
            'configuration.receiving_time',
            'configuration.receiving_time',
        ],
        'tradeSettingUpdate' => [
            'update_time',
            'configuration.order_is_close',
            'configuration.order_close_time',
            'configuration.is_auto_receiving',
            'configuration.receiving_time',
            'configuration.deal_enhance',
            //'configuration.invoice',
            'configuration.order_end_time',
            'configuration.order_evaluate',
            'configuration.receiving_time',
            'configuration.receiving_time',
            'configuration.receiving_time',
        ],
        'rightsSettingSave' => [
            'configuration.is_rights_apply',
            //'configuration.rights_apply_time',
            'configuration.refund_single',
        ],
        'rightsSettingUpdate' => [
            'update_time',
            'configuration.is_rights_apply',
            //'configuration.rights_apply_time',
            'configuration.refund_single',
        ],
        'integralBalanceSave' => [
            'configuration.integral_text',
            'configuration.integral_upper',
            'configuration.integral_upper_limit',
            'configuration.balance_text',
            'configuration.balance_setting',
            'configuration.balance_cash',
            'configuration.withdraw',
//            'configuration.withdraw_type',
//            'configuration.withdraw_type_outline',
            'configuration.withdraw_limit',
            'configuration.withdraw_limit_cash',
            'configuration.withdraw_service',
            'configuration.service_charge',
            'configuration.is_free_fee',
            'configuration.agent_withdraw',
//            'configuration.agent_withdraw_type',
//            'configuration.agent_withdraw_type_outline',
            'configuration.agent_withdraw_limit',
            'configuration.agent_withdraw_limit_cash',
            'configuration.agent_withdraw_service',
            'configuration.agent_service_charge',
            'configuration.agent_is_free_fee',
            'configuration.merchant_withdraw',
//            'configuration.merchant_withdraw_type',
//            'configuration.merchant_withdraw_type_outline',
            'configuration.merchant_withdraw_limit',
            'configuration.merchant_withdraw_limit_cash',
            'configuration.merchant_withdraw_service',
            'configuration.merchant_service_charge',
            'configuration.merchant_is_free_fee',
        ],
        'integralBalanceUpdate' => [
            'update_time',
            'configuration.integral_text',
            'configuration.integral_upper',
            'configuration.integral_upper_limit',
            'configuration.balance_text',
            'configuration.balance_setting',
            'configuration.balance_cash',
            'configuration.withdraw',
//            'configuration.withdraw_type',
//            'configuration.withdraw_type_outline',
            'configuration.withdraw_limit',
            'configuration.withdraw_limit_cash',
            'configuration.withdraw_service',
            'configuration.service_charge',
            'configuration.is_free_fee',
            'configuration.agent_withdraw',
//            'configuration.agent_withdraw_type',
//            'configuration.agent_withdraw_type_outline',
            'configuration.agent_withdraw_limit',
            'configuration.agent_withdraw_limit_cash',
            'configuration.agent_withdraw_service',
            'configuration.agent_service_charge',
            'configuration.agent_is_free_fee',
            'configuration.merchant_withdraw',
//            'configuration.merchant_withdraw_type',
//            'configuration.merchant_withdraw_type_outline',
            'configuration.merchant_withdraw_limit',
            'configuration.merchant_withdraw_limit_cash',
            'configuration.merchant_withdraw_service',
            'configuration.merchant_service_charge',
            'configuration.merchant_is_free_fee',
        ],
        'mallPayModeSave' => [
            'configuration.wechat.wx',
            'configuration.wechat.wx_pay_mode_id',
            'configuration.wechat.ali',
            'configuration.wechat.wx_pay_mode_id',
            'configuration.wechat.recharge',
            'configuration.wechat.recharge',
            'configuration.wechata.wx',
            'configuration.wechata.wx_pay_mode_id',
            'configuration.wechata.recharge',
            'configuration.wechata.recharge',
//            'configuration.web.wx',
            'configuration.web.wx_pay_mode_id',
            'configuration.web.ali',
            'configuration.web.wx_pay_mode_id',
            'configuration.web.recharge',
            'configuration.web.sendPay',
        ],
        'mallPayModeUpdate' => [
            'update_time',
            'configuration.wechat.wx',
            'configuration.wechat.wx_pay_mode_id',
            'configuration.wechat.ali',
            'configuration.wechat.wx_pay_mode_id',
            'configuration.wechat.recharge',
            'configuration.wechat.recharge',
            'configuration.wechata.wx',
            'configuration.wechata.wx_pay_mode_id',
            'configuration.wechata.recharge',
            'configuration.wechata.recharge',
//            'configuration.web.wx',
            'configuration.web.wx_pay_mode_id',
            'configuration.web.ali',
            'configuration.web.wx_pay_mode_id',
            'configuration.web.recharge',
            'configuration.web.sendPay',
        ],
        'remitSettingSave' => [
            'configuration.ali_remit',
            'configuration.ali_remit_pay_mode_id',
            'configuration.wx_remit',
            'configuration.wx_remit_wechat_pay_mode_id',
            'configuration.wx_remit_wechata_pay_mode_id',
            'configuration.wx_remit_commission',
            'configuration.wx_remit_withdraw',
            'configuration.wx_remit_packet',
        ],
        'remitSettingUpdate' => [
            'update_time',
            'configuration.ali_remit',
            'configuration.ali_remit_pay_mode_id',
            'configuration.wx_remit',
            'configuration.wx_remit_wechat_pay_mode_id',
            'configuration.wx_remit_wechata_pay_mode_id',
            'configuration.wx_remit_commission',
            'configuration.wx_remit_withdraw',
            'configuration.wx_remit_packet',
        ],
        'aliSmsSave' => [
            'configuration.accessKeyId',
            'configuration.accessSecret',
        ],
        'aliSmsUpdate' => [
            'update_time',
            'configuration.accessKeyId',
            'configuration.accessSecret',
        ],
        'wxSave' => [
            'configuration.appid',
            'configuration.appsecret',
//            'configuration.URL',
            // 'configuration.Token',
            // 'configuration.EncodingAESKey',
        ],
        'wxUpdate' => [
            'update_time',
            'configuration.appid',
            'configuration.appsecret',
//            'configuration.URL',
            // 'configuration.Token',
            // 'configuration.EncodingAESKey',
        ],
        'exoressBirdSave' => [
            'configuration.appId',
            'configuration.key',
        ],
        'exoressBirdUpdate' => [
            'update_time',
            'configuration.appId',
            'configuration.key',
        ],
        'agentExplainSave' => [
            'configuration.open',
            'configuration.level',
            'configuration.inPurchasing',
            'configuration.is_show_profit',
            'configuration.profit.first',
            'configuration.profit.second',
            'configuration.profit.third',
            'configuration.become.type',
            'configuration.isAudit',
            'configuration.relationship',
//            'configuration.isShowProfit',
        ],
        'agentExplainUpdate' => [
            'update_time',
            'configuration.open',
            'configuration.level',
            'configuration.inPurchasing',
            'configuration.is_show_profit',
            'configuration.profit.first',
            'configuration.profit.second',
            'configuration.profit.third',
            'configuration.become.type',
            'configuration.isAudit',
            'configuration.relationship',
//            'configuration.isShowProfit',
        ],
        'lotterySave' => [
            'configuration.lottery_open',
            'configuration.lottery_config_id',
            'configuration.lottery_type',
            'configuration.lottery_limit',
            'configuration.lottery_money',
            'configuration.lottery_number',
            'configuration.superposition'
        ],
        'lotteryUpdate' => [
            'update_time',
            'configuration.lottery_open',
            'configuration.lottery_config_id',
            'configuration.lottery_type',
            'configuration.lottery_limit',
            'configuration.lottery_money',
            'configuration.lottery_number',
            'configuration.superposition'
        ]
    ];

}