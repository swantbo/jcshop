<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\validate;


use think\Validate;

class RechargeRecordValidate extends Validate
{
    protected $rule = [
        'page|页数' => 'require|integer',
        'size|条数' => 'require|integer',
        'user_id' => 'integer',
        'base_in' => 'in:admin,wechata,wechat,web',
        'source' => 'chsDash',
        'is_online' => 'in:0,1',
        'type' => 'in:0,1,2',
        'money|金额' => 'float',
        'service_charge|服务费' => 'float',
        'trade|内部单号' => 'alphaDash',
        'pay_no|支付单号' => 'alphaDash',
        'status' => "integer",
        'begin_date|开始时间' => 'dateFormat:Y-m-d',
        'end_date|结束时间' => 'dateFormat:Y-m-d',

    ];

    protected $message = [
        'user_id.integer' => "请选择正确的用户",
        'base_in.in' => "请选择正确的变更位置",
        'source.chsDash' => "请选择正确的来源",
        'is_online.in' => "请选择正确的是否线上",
        'type.in' => "请选择正确的类型",
        'status.integer' => "状态有误",
    ];

    protected $scene = [
        'list' => [
            'page',
            'size',
            'user_id',
            'base_in',
            'source',
            'is_online',
            'type',
            'money',
            'service_charge',
            'trade',
            'pay_no',
            'status',
            'begin_date',
            'end_date',
        ],
    ];

}