<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\validate;


use think\Validate;

class UserValidae extends Validate
{
    protected $rule = [
        'page' => 'require',
        'size' => 'require',
        'nickname' => 'max:16',
        'name' => 'max:16',
        'mobile' => 'mobile',
        'begin_date' => 'dateFormat:Y-m-d',
        'end_date' => 'dateFormat:Y-m-d',
        'update_time' => 'require|dateFormat:Y-m-d H:i:s',
        'status' => 'integer'
    ];
    protected $message = [
        'page' => '页数必须上传',
        'size' => '显示数量必须上传',
        'nickname' => '昵称请限定再16个字符以内',
        'name' => '名称请限定再16个字符以内',
        'mobile' => '手机号码格式错误',
        'begin_date' => '开始时间必须是日期格式',
        'end_date' => '结束时间必须是日期格式',
        'update_time' => '更新时间不正确',
        'status' => '状态错误'
    ];
    protected $scene = [
        'list' => ['page', 'size', 'nickname', 'name', 'begin_date', 'end_date'],
        'update' => ['update_time', 'mobile', 'name', 'status']
    ];
}