<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\validate;


use think\Validate;

class StoreValidate extends Validate
{

    protected $rule = [
        'page' => 'require|integer',
        'size' => 'require|integer',
        'audit' => 'integer',
//        'audit_remark' => '',
        'update_time' => 'require|dateFormat:Y-m-d H:i:s'
    ];
    protected $message = [
        'page' => '页数必须上传',
        'size' => '显示数量必须上传',
        'audit.integer' => '审核状态不正确',
//        'audit_remark' => '',
        'update_time' => '更新时间不正确'
    ];
    protected $scene = [
        'list' => ['page','size'],
        'update' => ['update_time','audit']
    ];
}