<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\utils;

use think\Exception;

class CheckWithdraw
{
    private $configuration;

    /**
     * CheckWithdraw constructor.
     * @param array $configuration
     * @throws Exception
     */
    public function __construct(array $configuration)
    {
        if (empty($configuration))
            throw new Exception("未完善积分余额配置");
        $this->configuration = $configuration;
    }


    /**
     * 获取分销提现手续费
     * @param float $amount
     * @return int|string
     * @throws Exception
     */
    public function commissionAssessment(float $amount)
    {
        return $this->getServiceChargeFromAmount($amount, "agent_");
//        return $this->getServiceChargeFromAmount($amount, "commission_assessment_");
    }
    
    /**
     * 获取分销提现手续费
     * @param float $amount
     * @return int|string
     * @throws Exception
     */
    public function agent(float $amount)
    {
        return $this->getServiceChargeFromAmount($amount, "agent_");
    }

    /**
     * 获取商户提现手续费
     * @param float $amount
     * @return int|string
     * @throws Exception
     */
    public function merchant(float $amount)
    {
        return $this->getServiceChargeFromAmount($amount, "merchant_");

    }

    /**
     * 获取余额提现手续费
     * @param float $amount
     * @return int|string
     * @throws Exception
     */
    public function total(float $amount)
    {
        return $this->getServiceChargeFromAmount($amount);
    }

    /**
     * 整理手续费
     * @param $amount
     * @param string|null $method
     * @return int|string
     * @throws Exception
     */
    private function getServiceChargeFromAmount($amount, string $method = null)
    {
        if (
            !isset($this->configuration[$method . 'withdraw']) ||
            empty($this->configuration[$method . 'withdraw'])
        )
            throw new Exception('不支持提现方式', HTTP_NOTACCEPT);
        if (
            !empty($this->configuration[$method . 'withdraw_limit']) &&
            $amount < $this->configuration[$method . 'withdraw_limit_cash']
        )
            throw new Exception("最低提现金额为:" . $this->configuration[$method . 'withdraw_limit_cash'] . "元", HTTP_NOTACCEPT);
        $serviceCharge = 0;
        if (isset($this->configuration[$method . 'withdraw_service']) && !empty($this->configuration[$method . 'withdraw_service'])) {
            $serviceCharge = bcmul((string)$amount, (string)($this->configuration[$method . 'service_charge'] / 100));
            if (
                isset($this->configuration[$method . 'is_free_fee']) &&
                is_array($this->configuration[$method . 'is_free_fee']) &&
                !empty($this->configuration[$method . 'is_free_fee'])
            ) {
                if (
                    $serviceCharge > $this->configuration[$method . 'free_fee_section'][0]
                    && $serviceCharge < $this->configuration[$method . 'free_fee_section'][1]
                )
                    $serviceCharge = 0;
            }
        }
        return $serviceCharge;
    }


}