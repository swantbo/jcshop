<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\utils;


use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;
use think\Exception;
use think\facade\Cache;
use think\facade\Db;
use think\facade\Env;
use think\facade\Log;

class AliSms
{

    /**
     * AliSms constructor.
     * @throws ClientException
     * @throws Exception
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function __construct(int $merchantId = null,int $is_saas = 0)
    {
        if($is_saas == 0){
            $configuration = Db::name('configuration')
                ->where(['type' => 'aliSms', 'status' => 1,'mall_id'=>$merchantId])
                ->value('configuration');
            $configuration = json_decode($configuration, true);
            if (empty($configuration) || !isset($configuration['accessKeyId'], $configuration['accessSecret']))
                throw new Exception('请完善阿里云短信配置');
        }else{
            $configuration = Db::name('ali_sms_saas')->where('is_open',1)->find();
            $configuration['accessKeyId'] = $configuration['access_key_id'];
            $configuration['accessSecret'] = $configuration['access_secret'];
        }
        AlibabaCloud::accessKeyClient($configuration['accessKeyId'], $configuration['accessSecret'])
            ->regionId('cn-hangzhou')
            ->asDefaultClient();


    }

    /**
     * @param string $method
     * @param array $options
     * @return array
     */
    private function callMethod(string $method, array $options)
    {
        $options = array_merge(['RegionId' => "cn-hangzhou"], $options);
        try {
            $result = AlibabaCloud::rpc()
                ->product('Dysmsapi')
                ->scheme('https') // https | http
                ->version('2017-05-25')
                ->action($method)
                ->method('POST')
                ->host('dysmsapi.aliyuncs.com')
                ->options([
                    'query' => $options,
                ])
                ->request();
            return [HTTP_SUCCESS, $result->toArray()];
        } catch (ClientException $e) {
            Log::error("$method:" . $e->getErrorMessage());
            return [$e->getCode(), $e->getErrorMessage()];
        } catch (ServerException $e) {
            Log::error("$method:" . $e->getErrorMessage());
            return [$e->getCode(), $e->getErrorMessage()];
        }
//        return [HTTP_SERVERERROR, '操作失败'];
    }

    public function AddSmsSign(array $options)
    {
        $ret = $this->callMethod(__FUNCTION__, $options);
        return $ret;
    }

    public function QuerySmsSign(array $options)
    {
        $ret = $this->callMethod(__FUNCTION__, $options);
        return $ret;
    }

    public function ModifySmsSign(array $options)
    {
        $ret = $this->callMethod(__FUNCTION__, $options);
        return $ret;
    }

    public function DeleteSmsSign(array $options)
    {
        $ret = $this->callMethod(__FUNCTION__, $options);
        return $ret;
    }

    public function AddSmsTemplate(array $options)
    {
        $ret = $this->callMethod(__FUNCTION__, $options);
        return $ret;
    }

    public function QuerySmsTemplate(array $options)
    {
        $ret = $this->callMethod(__FUNCTION__, $options);
        return $ret;
    }

    public function ModifySmsTemplate(array $options)
    {
        $ret = $this->callMethod(__FUNCTION__, $options);
        return $ret;
    }

    public function DeleteSmsTemplate(array $options)
    {
        $ret = $this->callMethod(__FUNCTION__, $options);
        return $ret;
    }

    public function SendSms(array $options)
    {
        $ret = $this->callMethod(__FUNCTION__, $options);
        return $ret;
    }

    public function SendBatchSms(array $options)
    {
        $ret = $this->callMethod(__FUNCTION__, $options);
        return $ret;
    }

    public function QuerySendDetails(array $options)
    {
        $ret = $this->callMethod(__FUNCTION__, $options);
        return $ret;
    }

}