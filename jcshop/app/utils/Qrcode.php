<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\utils;

use app\index\model\Configuration;
use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\QrCode as util;
use think\Exception;
use think\facade\Db;

class Qrcode
{

    /**
     * @param string $path 路径
     * @param string $name 文件名
     * @param string $text 内容
     * @param int $size 大小
     * @param int $margin 边框
     * @param array $logo ["path"=>"xxx","width":100,"height":100]
     * @param string $type 类型
     * @param string $encoding 编码
     * @param int[] $foregroundColor 前置颜色
     * @param int[] $backgroundColor 背景颜色深
     * @param array $option 参数
     * @return string
     */
    public static function create(
        $path,
        $name,
        $text,
        $size = 300,
        $margin = 10,
        $logo = [],
        $type = 'png',
        $encoding = 'UTF-8',
        $foregroundColor = ['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0],
        $backgroundColor = ['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0],
        $option = []
    )
    {
        !is_dir($path) && mkdir($path, 0755, true);
        $qrCode = new util($text);
        $qrCode->setSize($size);
        $qrCode->setMargin($margin);

        $qrCode->setWriterByName($type);
        $qrCode->setEncoding($encoding);
        $qrCode->setErrorCorrectionLevel(ErrorCorrectionLevel::HIGH());
        $qrCode->setForegroundColor($foregroundColor);
        $qrCode->setBackgroundColor($backgroundColor);
        if (!empty($logo)) {
            $qrCode->setLogoPath($logo['path']);
            $qrCode->setLogoSize($logo['width'], $logo['heigth']);
        }

        $qrCode->setRoundBlockSize(true, util::ROUND_BLOCK_SIZE_MODE_MARGIN);
        $qrCode->setRoundBlockSize(true, util::ROUND_BLOCK_SIZE_MODE_ENLARGE);
        $qrCode->setRoundBlockSize(true, util::ROUND_BLOCK_SIZE_MODE_SHRINK);
        $qrCode->setWriterOptions($option);

        $qrCode->writeFile($path . $name);

        return $path . $name;
    }

    /*
     * 生成微信小程序码
     */
    public function wxa($path, $name, $scene, $page,$type = 0)
    {
        !is_dir($path) && mkdir($path, 0755, true);
        if ($type === 0){
            $options = $this->getOptions();
        }else{
            $options = $this->getMallOptions();
        }

        $qrcode = new \WeMini\Qrcode($options);
        $data = $qrcode->createMiniScene($scene, $page, $width = 430, $auto_color = false, $line_color = ["r" => "0", "g" => "0", "b" => "0"], $is_hyaline = false, $outType = null);
        file_put_contents($path . $name, $data);
        return $path . $name;
    }

    private function getOptions()
    {
        $configuration = Configuration::where(['type' => "wxa"])->value("configuration");
        $configuration = $this->checkOptions($configuration);
        return $configuration;
    }

    private function getMallOptions()
    {
        global $user;
        $configuration = Db::table('jiecheng_configuration')
            ->where(['mall_id' =>$user->mall_id,'type' =>'wxa'])
            ->value("configuration");
        $configuration = $this->checkOptions($configuration);
        return $configuration;
    }

    private function checkOptions($configuration)
    {
        if (empty($configuration))
            throw new Exception("未完善配置");
        $configuration = json_decode($configuration, true);
        if (!isset($configuration['appid'], $configuration['appsecret'])
            || empty(trim($configuration['appsecret']))
            || empty(trim($configuration['appid']))
        )
            throw new Exception("未开启配置");
        return $configuration;
    }

}