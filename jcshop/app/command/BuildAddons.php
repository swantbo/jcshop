<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\command;

use app\madmin\service\AddonsData;
use app\utils\RedisUtil;
use think\console\Input;
use think\console\input\Argument;
use think\console\Output;
use think\Exception;
use think\facade\Db;

class BuildAddons extends BaseCommand
{
    protected function configure()
    {
        // 指令配置
        $this->setName('build_addons')
            ->addArgument('addons_id',Argument::REQUIRED,'插件ID')
            ->setDescription('快速将插件打包');
    }

    protected function execute(Input $input, Output $output)
    {
        $this->setDb($input, $output);
    }

    protected function executeAction(Input $input, Output $output)
    {
        $addons_id=$input->getArgument('addons_id');
        if(empty($addons_id)){
            $output->writeln("请指定插件ID");
            exit();
        }
        $this->build($addons_id,$input,$output);
    }
    public function build($addons_id,Input $input, Output $output){
        if ($addons_id == 100){
            $list = [1, 2, 3, 5, 10, 11, 12, 13, 14, 16, 19, 20, 21, 22, 23, 26, 27, 28, 30, 31, 32, 35];
            foreach($list as $item=>$value){
                $addons_id=(string)$value;
                $addons_key="addons_{$addons_id}";
                $addonsData= new AddonsData();
                $data=$addonsData->$addons_key();
                if(empty($data)){
                    continue;
                }
                $app_root=App()->getRootPath();
                try{
                    $zip = new \ZipArchive();
                    if($zip->open($app_root."addons/addons-{$addons_id}-build.zip",\ZipArchive::CREATE)===true){
                        foreach($data as $item){
                            echo $app_root.$item.PHP_EOL;
                            if(file_exists($app_root.$item)){
                                $zip->addFile($app_root.$item,$item);
                            }
                        }
                        $zip->close();
                    }
                    $output->writeln("插件打包成功");
                }catch (\Exception $e){
                    $output->writeln($e->getMessage());
                }
            }
            $output->writeln("插件批量打包成功");

        }else{
            $addons_key="addons_{$addons_id}";
            try{
                $addonsData= new AddonsData();
                $data=$addonsData->$addons_key();
                if(empty($data)){
                    throw new Exception("该插件暂不支持打包");
                }
            }catch (\Throwable $e){
                $output->writeln("打包异常错误信息:".$e->getMessage());
                exit();
            }
            $app_root=App()->getRootPath();
            try{
                $zip = new \ZipArchive();
                if($zip->open($app_root."addons/addons-{$addons_id}-build.zip",\ZipArchive::CREATE)===true){
                    foreach($data as $item){
                        echo $app_root.$item.PHP_EOL;
                        if(file_exists($app_root.$item)){
                            $zip->addFile($app_root.$item,$item);
                        }
                    }
                    $zip->close();
                }
                $output->writeln("插件打包成功");
            }catch (\Exception $e){
                $output->writeln($e->getMessage());
            }
        }

    }
}