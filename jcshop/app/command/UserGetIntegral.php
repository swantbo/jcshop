<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\command;

use think\console\Input;
use think\console\Output;
use think\facade\Db;

class UserGetIntegral extends BaseCommand
{
    protected function configure()
    {
        // 指令配置
        $this->setName('user_get_integral')
            ->setDescription('消费者获得积分');
    }
    protected function execute(Input $input, Output $output){
        $this->setDb($input, $output);
    }

    protected function executeAction(Input $input, Output $output)
    {
        $config = Db::name('configuration')
            ->field('configuration,mall_id')
            ->where('type','integralBalance')
            ->select();
        $select = Db::name('integral_record')
            ->field('i.id,i.user_id,i.integral,i.order_id')
            ->alias('i')
            ->join('order_commodity oc','oc.order_id=i.order_id AND oc.commodity_id=i.commodity_id','left')
            ->where('oc.status',4)//已完结
            ->where('i.status',0)
            ->select()
            ->toArray();
        $order_id = array_column($select,'order_id');
        $order_id = array_unique($order_id);
        foreach ($config AS $key => $value){
            $c = json_decode($value['configuration']);
            if(empty($c->get_integral_type))continue;
            $order = Db::name('order')
                ->alias('o')
                ->field('id,money,count,user_id')
                ->join(
                    Db::name('order_commodity')
                        ->field('count(*) AS count,order_id')
                        ->where('commodity_id','in',function ($sql){
                            $sql->name('commodity')->field('id')->where('is_integral',1);
                        })
                        ->group('order_id')
                        ->buildSql().'oc',
                    'oc.order_id=o.id',
                    'left'
                )
                ->where('oc.count','NOT NULL')
                ->where('update_time','>=',date('Y-m-d'))
                ->where('status',4)
                ->where('mall_id',$value['mall_id'])
                ->where('id','NOT IN',$order_id)
                ->select();
            $user = [];
            foreach ($order AS $index => $item) {
                if ($c->get_integral_type == 1)
                    $integral = $c->get_integral_ratio * $item['money'];
                else
                    $integral = $c->get_integral_ratio * $item['count'];
                $integral = round($integral);
                if (empty($integral))continue;
                $user[$item['user_id']][] = $integral;
                $record[] = [
                    'mall_id' => $value['mall_id'],
                    'user_id' => $item['user_id'],
                    'order_id' => $item['id'],
                    'commodity_id' => 0,
                    'sku_id' => 0,
                    'number' => $item['count'],
                    'type' => 1,
                    'status' => 1,
                    'integral' => $integral,
                    'create_time' => date('Y-m-d H:i:s'),
                    'update_time' => date('Y-m-d H:i:s')
                ];
            }
            foreach ($user AS $index => $item){
                Db::name('user_cash')
                    ->where('user_id',$index)
                    ->inc('integral_total',array_sum($item))
                    ->inc('integral',array_sum($item))
                    ->update(['update_time' => date('Y-m-d H:i:s')]);
            }
            if (!empty($record))
                Db::name('integral_record')->insertAll($record);
        }
        $id = array_column($select,'id');
        Db::name('integral_record')
            ->where('id','IN',$id)
            ->update([
                'update_time' => date('Y-m-d H:i:s'),
                'status' => 1
            ]);
        foreach ($select AS $key => $value){
            Db::name('user_cash')
                ->where('user_id',$value['user_id'])
                ->inc('integral_total',$value['integral'])
                ->inc('integral',$value['integral'])
                ->update(['update_time' => date('Y-m-d H:i:s')]);
        }
    }
}