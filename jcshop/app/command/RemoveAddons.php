<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\command;

use app\madmin\service\AddonsData;
use app\utils\RedisUtil;
use think\console\Input;
use think\console\input\Argument;
use think\console\Output;
use think\Exception;
use think\facade\Db;

class RemoveAddons extends BaseCommand
{
    protected function configure()
    {
        // 指令配置
        $this->setName('remove_addons')
            ->setDescription('将所有可卸载的插件都卸一遍');
    }

    protected function execute(Input $input, Output $output)
    {
        $this->setDb($input, $output);
    }

    protected function executeAction(Input $input, Output $output)
    {
        try{
            AddonsData::removeAll();
        }catch (\Throwable $e){
            $output->writeln("删除异常错误信息:".$e->getMessage());
            exit();
        }
        $output->writeln("删除成功");
    }
}