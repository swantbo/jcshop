<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\shop_admin\controller;


use app\shop_admin\service\ServiceFactory;
use think\App;
use think\Exception;

class Receipts extends BaseAction
{
    private $is_bool;
    public function __construct(App $app)
    {
        global $user;
        parent::__construct($app);
        $this->is_bool = \app\utils\Addons::check($user->mall_id,10);
    }

    public function create(){
        if(!$this->is_bool)
            return json(['msg' => '请先激活小票助手插件'],HTTP_NOTACCEPT);
        $template = input('template_id');
        $order_id = input('order_id');
        $printer = input('printer_id');
        ServiceFactory::getResult('ReceiptsService','create',$template,$order_id,$printer,0,null);
        return json(['msg' => ''],HTTP_SUCCESS);
    }
    public function template_create(){
        if(!$this->is_bool)
            return json(['msg' => '请先激活小票助手插件'],HTTP_NOTACCEPT);
        $config = input('config',[]);
        $name = input('name');
        list($code,$result) = ServiceFactory::getResult('ReceiptsService','create_template',$config,$name,$this->user->merchant_id);
        return json(['msg' => $result],$code);
    }
    public function template_update(){
        if(!$this->is_bool)
            return json(['msg' => '请先激活小票助手插件'],HTTP_NOTACCEPT);
        $id = input('id');
        $config = input('config',[]);
        $name = input('name');
        list($code,$result) = ServiceFactory::getResult('ReceiptsService','update_template',$config,$name,$id);
        return json(['msg' => $result],$code);
    }
    public function template_delete(){
        if(!$this->is_bool)
            return json(['msg' => '请先激活小票助手插件'],HTTP_NOTACCEPT);
        $id = input('id');
        list($code,$result) = ServiceFactory::getResult('ReceiptsService','delete_template',$id);
        return json(['msg' => $result],$code);
    }
    public function template_list(){
        if(!$this->is_bool)
            return json(['msg' => []],HTTP_SUCCESS);
        list($code,$result) = ServiceFactory::getResult('ReceiptsService','list_template',$this->user->merchant_id);
        return json(['msg' => $result],$code);
    }
    public function template_find(){
        if(!$this->is_bool)
            return json(['msg' => '请先激活小票助手插件'],HTTP_NOTACCEPT);
        $id = input('id');
        list($code,$result) = ServiceFactory::getResult('ReceiptsService','find_template',$id);
        return json(['msg' => $result],$code);
    }

    public function template_default(){
        if(!$this->is_bool)
            return json(['msg' => '请先激活小票助手插件'],HTTP_NOTACCEPT);
        $id = input('id');
        list($code,$result) = ServiceFactory::getResult('ReceiptsService','default_template',$id,$this->user->merchant_id);
        return json(['msg' => $result],$code);
    }

    public function printer_insert(){
        if(!$this->is_bool)
            return json(['msg' => '请先激活小票助手插件'],HTTP_NOTACCEPT);
        $name = input('name');
        $printer_no = input('printer_no');
        $client_id = input('client_id');
        $client_secret = input('client_secret');
        $printer_secret = input('printer_secret');
        list($code,$result) = ServiceFactory::getResult('ReceiptsService','insert_printer',$name,$printer_no,$client_id,$client_secret,$printer_secret,$this->user->merchant_id);
        return json(['msg' => $result],$code);
    }

    public function printer_update(){
        if(!$this->is_bool)
            return json(['msg' => '请先激活小票助手插件'],HTTP_NOTACCEPT);
        $id = input('id');
        $data = input('put.');
        list($code,$result) = ServiceFactory::getResult('ReceiptsService','update_printer',$id,$data);
        return json(['msg' => $result],$code);
    }

    public function printer_delete(){
        if(!$this->is_bool)
            return json(['msg' => '请先激活小票助手插件'],HTTP_NOTACCEPT);
        $id = input('id');
        list($code,$result) = ServiceFactory::getResult('ReceiptsService','delete_printer',$id);
        return json(['msg' => $result],$code);
    }


    public function printer_default(){
        if(!$this->is_bool)
            return json(['msg' => '请先激活小票助手插件'],HTTP_NOTACCEPT);
        $id = input('id');
        list($code,$result) = ServiceFactory::getResult('ReceiptsService','default_printer',$id,$this->user->merchant_id);
        return json(['msg' => $result],$code);
    }
    public function printer_find(){
        if(!$this->is_bool)
            return json(['msg' => '请先激活小票助手插件'],HTTP_NOTACCEPT);
        $id = input('id');
        list($code,$result) = ServiceFactory::getResult('ReceiptsService','find_printer',$id);
        return json(['msg' => $result],$code);
    }
    public function printer_list(){
        if(!$this->is_bool)
            return json(['msg' => []],HTTP_SUCCESS);
        list($code,$result) = ServiceFactory::getResult('ReceiptsService','list_printer',$this->user->merchant_id);
        return json(['msg' => $result],$code);
    }

    public function task_list(){
        if(!$this->is_bool)
            return json(['msg' => []],HTTP_SUCCESS);
        list($code,$result) = ServiceFactory::getResult('ReceiptsService','list_task',$this->user->merchant_id);
        return json(['msg' => $result],$code);
    }

    public function task_find(){
        if(!$this->is_bool)
            return json(['msg' => '请先激活小票助手插件'],HTTP_NOTACCEPT);
        $id = input('id');
        list($code,$result) = ServiceFactory::getResult('ReceiptsService','find_task',$id);
        return json(['msg' => $result],$code);
    }

    public function task_create(){
        if(!$this->is_bool)
            return json(['msg' => '请先激活小票助手插件'],HTTP_NOTACCEPT);
        $post = input('post.');
        list($code,$result) = ServiceFactory::getResult('ReceiptsService','create_task',$post,$this->user->merchant_id);
        return json(['msg' => $result],$code);
    }

    public function task_update(){
        if(!$this->is_bool)
            return json(['msg' => '请先激活小票助手插件'],HTTP_NOTACCEPT);
        $id = input('id');
        $put = input('put.');
        list($code,$result) = ServiceFactory::getResult('ReceiptsService','update_task',$id,$put);
        return json(['msg' => $result],$code);
    }
    public function task_delete(){
        if(!$this->is_bool)
            return json(['msg' => '请先激活小票助手插件'],HTTP_NOTACCEPT);
        $id = input('id');
        list($code,$result) = ServiceFactory::getResult('ReceiptsService','delete_task',$id);
        return json(['msg' => $result],$code);
    }
}