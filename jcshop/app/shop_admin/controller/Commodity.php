<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\shop_admin\controller;

use app\madmin\model\LiveLog;
use app\madmin\model\ScrapyLog;
use app\Oss;
use app\shop_admin\model\LiveGood;
use app\shop_admin\service\CommodityService;
use app\shop_admin\service\PicService;
use app\shop_admin\service\ServiceFactory;
use app\utils\Addons;
use app\utils\scrapy\Base;
use app\utils\scrapy\Jd;
use app\utils\scrapy\Pdd;
use app\utils\scrapy\Suning;
use app\utils\scrapy\Taobao;
use app\utils\scrapy\Taobao1688;
use app\utils\scrapy\Tmall;
use GuzzleHttp\Client;
use think\App;
use think\facade\Cache;
use think\Request;


class Commodity extends BaseAction
{


    public function scrapy()
    {

        $check = Addons::check($this->user->mall_id, 26);

        if (!$check)
            throw new \Exception("请先激活插件", HTTP_NOTACCEPT);

        $urls=[
            "https://item.jd.com/10052971560774.html",
            "https://mobile.pinduoduo.com/goods.html?goods_id=349418420304&_oak_gallery=https%3A%2F%2Fimg.pddpic.com%2Fmms-material-img%2F2022-04-13%2F48e0e066-8866-48ff-a58c-96a5cf5cc627.png&_x_query=%E6%89%8B%E6%9C%BA&refer_page_el_sn=99369&refer_rn=&refer_page_name=search_result&refer_page_id=10015_1653894845418_7atesqqv0w&refer_page_sn=10015",
            "https://product.suning.com/0070688515/12362165160.html?safp=d488778a.13701.productWrap.10&safc=prd.0.0&safpn=10007",
            "https://item.taobao.com/item.htm?spm=a230r.1.14.22.18157f2f8fu8eT&id=630655814664&ns=1&abbucket=16#detail",
            "https://detail.1688.com/offer/653860986118.html?spm=a262eq.12572798.jsczf959.1.67c52fb1urLydi&&scm=1007.30832.181565.0&pvid=b082e713-1e3a-4ad9-aefc-60a1c1d3a137&object_id=653860986118&scm2=1007.30657.177495.0&pvid2=6bc4a9e8-e0b5-426b-96a5-5d3dcfeaeeac&trackInfo=0_653860986118_0.0678695_0.0_0.0_0.0__1294152",
            "https://chaoshi.detail.tmall.com/item.htm?spm=a230r.1.14.34.3a5073dbM75EJv&id=655712287964&ns=1&abbucket=16"
        ];

        $urls=request()->post('urls',"");
        if(empty($urls)){
            return json(['msg' => "参数缺失"], HTTP_NOTACCEPT);
        }
        $urls=explode("\\n",$urls);

        $error_urls=[];
        $success_urls=[];


        foreach ($urls as $url) {
            try {
                $type = Base::getType($url);
                if ($type) {
                    $type = "app\\utils\\scrapy\\$type";
                    $scrapy = new $type($url);
                    $scrapy->scrapy();
                    $data=$scrapy->getSuccessData();
                    $success_info=['msg'=>'采集成功','url'=>$url,'data'=>$data];
                    ScrapyLog::write('采集成功',$success_info,1);
                    $success_urls[] = $success_info;
                } else {
                    #ScrapyLog::write('采集失败',['msg' => "采集链接异常", 'url' => $url],0);
                    $error_urls[] = ['msg' => '采集链接异常', 'url' => $url];
                }
            } catch (\Exception $e) {
                $error_info = ['msg' => $e->getMessage(), 'url' => $url];
                array_push($error_urls,$error_info);
                ScrapyLog::write('采集失败',$error_info,0);
                continue;
            }
        }



        if(empty($success_urls)){
            return json(['msg'=>'全部抓取失败','data'=>['error_list'=>$error_urls,'success_list'=>$success_urls]],HTTP_NOTACCEPT);
        }
        if(!empty($error_urls) && !empty($success_urls)){
            return json(['msg'=>'部分抓取成功','data'=>['error_list'=>$error_urls,'success_list'=>$success_urls]],HTTP_SUCCESS);
        }
        return json(['msg' => "成功"], HTTP_SUCCESS);
    }

    public function scrapyLogList()
    {

        $check = Addons::check($this->user->mall_id, 26);

        if (!$check){
            throw new \Exception("请先激活插件", HTTP_NOTACCEPT);
        }

        $page = $this->request->get('page',1);
        $size = $this->request->get('size',10);

        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            (array)$this->data, $page, $size);
        return json(['msg' => $msg], $code);
    }

    public function index()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }


    public function findByIds()
    {
        $ids = input("post.ids");
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $ids);
        return json(['msg' => $msg], $code);
    }


    public function findSellOut()
    {
        $page = $this->request->get('page');
        $size = $this->request->get('size');
        $data = array_filter($this->request->post(), 'filterArr');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $data, $page, $size);
        return json(['msg' => $msg], $code);
    }


    public function findAll()
    {
        $page = $this->request->get('page');
        $size = $this->request->get('size');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $this->data, $page, $size);
        return json(['msg' => $msg], $code);
    }

    public function save(Request $request)
    {
        $data = $request->post();
        unset($data['audit'], $data['status']);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $data);
        return json(['msg' => $msg], $code);
    }

    public function read(int $id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }


    public function update(int $id)
    {
        $data = $this->request->put();
        unset($data['audit']);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service',
            __FUNCTION__, $id, $data);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => $msg], $code);
    }


    public function reaudit($id)
    {

        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $id);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);

    }


    public function inventory($id)
    {

        $data = $this->request->put();
        unset($data['audit']);

        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $id, $data);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);

    }

    public function shelf()
    {
        $data = $this->request->put();
        unset($data['audit']);

        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service',
            __FUNCTION__, $data);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => $msg], $code);
    }


    public function delete()
    {
        $ids = input("delete.ids");
        $code = ServiceFactory::getResult(class_basename($this) . 'Service',
            __FUNCTION__, $ids);
        return response()->code((int)$code);
    }

    public function shop_preview()
    {
        $id = input('post.id');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }


    public function wechataPreview($id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }

    public function changeLiveGoodStatus()
    {
        list($code, $msg) = LiveGood::changeStatus();
        return json(['msg' => $msg], $code);
    }
}
