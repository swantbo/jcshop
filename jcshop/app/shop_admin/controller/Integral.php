<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\controller;


use app\madmin\controller\Configuration;
use app\shop_admin\service\ServiceFactory;
use think\facade\Db;

class Integral extends BaseAction
{
    public function deduction(){

        $integral = input('integral',0);
        $money = ServiceFactory::getResult('IntegralService','deduction',$integral);
        return json(['msg' => $money],HTTP_SUCCESS);
    }

    public function open(){
        $is_bool = \app\utils\Addons::check($this->user['mall_id'],7);
        if (!$is_bool)
            return json(['msg' => 0],HTTP_SUCCESS);
        $configuration = Db::name('configuration')
            ->where('mall_id',$this->user['mall_id'])
            ->where('type','integralBalance')
            ->value('configuration');
        if (empty($configuration))
            return json(['msg' => 0],HTTP_SUCCESS);
        $configuration = json_decode($configuration);
        if (empty($configuration))
            return json(['msg' => 0],HTTP_SUCCESS);
        if (empty($configuration->is_open))
            return json(['msg' => 0],HTTP_SUCCESS);
        $open = $configuration->is_open;
        if ($open == 0)
            return json(['msg' => 0],HTTP_SUCCESS);
        return json(['msg' => 1],HTTP_SUCCESS);
    }
}