<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\controller;


use app\shop_admin\service\ServiceFactory;
use think\exception\ValidateException;
class Upload extends BaseAction
{
    public function images()
    {
        $files = request()->file();
        try {
            validate(['images' => 'fileSize:10240000|fileExt:jpg,jpeg,gif,png,webp,bmp|fileMime:image/jpeg,image/gif,image/png,image/webp,image/bmp'])
                ->check($files);
        } catch (ValidateException $e) {
            return json(['msg' => $e->getMessage()], HTTP_NOTACCEPT);
        }
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . "Service", __FUNCTION__, $files);
        return json(['msg' => $msg], $code);

    }


}