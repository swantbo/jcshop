<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\controller;


use app\shop_admin\service\ServiceFactory;
use think\App;

class ExpressBill extends BaseAction
{
    private $is_bool;
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->is_bool = \app\utils\Addons::check($this->user->mall_id,14);
    }


    public function getConfig(){
        if (!$this->is_bool)return json(['msg' => []],HTTP_SUCCESS);
        list($code,$config) = ServiceFactory::getResult('ExpressBillService','getConfig',$this->user->merchant_id);
        return json(['msg' => $config],$code);
    }

    public function setConfig(){
        if (!$this->is_bool)return json(['msg' => []],HTTP_SUCCESS);
        $config = input('config');
        list($code,$result) = ServiceFactory::getResult('ExpressBillService','setConfig',$this->user->merchant_id,$this->user->mall_id,json_encode($config));
        return json(['msg' => $result],$code);
    }

    public function create_bill(){
        if (!$this->is_bool)return json(['msg' => []],HTTP_SUCCESS);
        $order_id = input('order_id',[]);
        $order_commodity_id = input('order_commodity_id',[]);
        $template_id = input('template_id','');
        list($code,$result) = ServiceFactory::getResult('ExpressBillService','create',$order_id,$order_commodity_id,$template_id,$this->user->merchant_id);//$this->user->mer
        return json(['msg' => $result],$code);
    }

    public function list_template(){
        if (!$this->is_bool)return json(['msg' => []],HTTP_SUCCESS);
        list($code,$result) = ServiceFactory::getResult('ExpressBillService','template_list',$this->user->merchant_id);
        return json(['msg' => $result],$code);
    }


    public function find_template(){
        if (!$this->is_bool)return json(['msg' => []],HTTP_SUCCESS);
        $id = input('id');
        list($code,$result) =  ServiceFactory::getResult('ExpressBillService','template_find',$id);
        return json(['msg' => $result],$code);
    }


    public function default_template(){
        if (!$this->is_bool)return json(['msg' => []],HTTP_SUCCESS);
        $id = input('id');
        list($code,$result) = ServiceFactory::getResult('ExpressBillService','template_default',$id,$this->user->merchant_id);
        return json(['msg' => $result],$code);
    }


    public function create_template(){
        if (!$this->is_bool)return json(['msg' => []],HTTP_SUCCESS);
        $data = input('post.','');
        list($code,$result) = ServiceFactory::getResult('ExpressBillService','template_create',$data,$this->user->merchant_id);
        return json(['msg' => $result],$code);
    }

    public function update_template(){
        if (!$this->is_bool)return json(['msg' => []],HTTP_SUCCESS);
        $id = input('id',0);
        $data = input('put.','');
        list($code,$result) = ServiceFactory::getResult('ExpressBillService','template_update',$data,$id);
        return json(['msg' => $result],$code);
    }

    public function delete_template(){
        if (!$this->is_bool)return json(['msg' => []],HTTP_SUCCESS);
        $id = input('id',0);
        list($code,$result) = ServiceFactory::getResult('ExpressBillService','template_delete',$id);
        return json(['msg' => $result],$code);
    }
}