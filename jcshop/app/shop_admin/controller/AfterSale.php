<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\controller;


use app\shop_admin\service\ServiceFactory;

class AfterSale extends BaseAction
{

    public function afterSaleList()
    {
        $data = input('get.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data, $data['page'], $data['size']);
        return json(['msg' => $msg], $code);
    }

    
    public function afterInfo()
    {
        $id = input('id');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }


    public function upAfterState()
    {
        $put = input('put.');
        $js = is_object($put) ? json_decode($put) : $put;
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $js);
        return json(['msg' => $msg], $code);
    }

    
    public function getAfterAddress()
    {
        $page = input('page', 1);
        $size = input('size', 10);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $page, $size);
        return json(['msg' => $msg], $code);
    }


}