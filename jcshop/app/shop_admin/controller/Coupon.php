<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\shop_admin\controller;

use app\shop_admin\service\ServiceFactory;
use app\utils\Addons;
use think\App;
use think\Exception;
use think\Request;


class Coupon extends BaseAction
{

    public function __construct(App $app)
    {
        parent::__construct($app);
        $check = Addons::check($this->user->mall_id, 3);
        if (!$check)
            throw new Exception("请先优惠券激活插件", HTTP_NOTACCEPT);

    }


    public function findAll()
    {
        $check = Addons::check($this->user->mall_id, 3);
        if (!$check)
            return json(['msg' => []],HTTP_SUCCESS);
        $page = $this->request->get('page');
        $size = $this->request->get('size');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $this->data, $page, $size);
        return json(['msg' => $msg], $code);
    }

    public function save(Request $request)
    {
        $data = $request->post();
        if ($data['denomination'] > $data['conditions'])
            throw new Exception("面额不能大于使用条件", HTTP_NOTACCEPT);
        if (isset($data['time_limit_type']) && $data['time_limit_type'] != 1) {
            unset($data['limit_indate_begin'], $data['limit_indate_end']);
        }
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $data);
        return json(['msg' => $msg], $code);
    }

    public function read(int $id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }

    
    public function update(int $id)
    {

        $data = $this->request->put();
        if (isset($data['denomination'],$data['conditions']) && $data['denomination'] > $data['conditions'])
            throw new Exception("面额不能大于使用条件",HTTP_NOTACCEPT);
        if (isset($data['time_limit_type']) && $data['time_limit_type'] != 1) {
            unset($data['limit_indate_begin'], $data['limit_indate_end']);
        }

        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service',
            __FUNCTION__, $id, $data);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }

    public function delete(int $id)
    {
        $code = ServiceFactory::getResult(class_basename($this) . 'Service',
            __FUNCTION__, $id);
        return response()->code($code);
    }

    public function send(int $id)
    {
        $userList = explode(",", input("post.userList"));
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $id, $userList);
        return json(['msg' => $msg], $code);
    }
}
