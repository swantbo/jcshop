<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\controller;


use app\shop_admin\service\ServiceFactory;

class Evaluate extends BaseAction
{

    public function evaluateList()
    {
        $data = input('get.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data);
        return json(['msg' => $msg], $code);
    }


    public function examine()
    {
        $post = input('post.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $post);
        return json(['msg' => $msg], $code);
    }

    public function reply()
    {
        $post = input('post.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $post);
        return json(['msg' => $msg], $code);
    }


    public function evaluateDel(int $id)
    {
        $code = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return response()->code($code);
    }

}