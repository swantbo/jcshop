<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types = 1);

namespace app\shop_admin\model;

use think\db\Query;
use think\Exception;
use think\Model;
use think\model\concern\SoftDelete;

class Store extends BaseModel
{

    use SoftDelete;

    protected $table="jiecheng_store";

    private static $audit;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (isset($data['status'])){
            self::append(['status_text']);
        }
        $merchant = Merchant::where(['status' => 1])->field('audit_type')->find(parent::$merchantId);
        if (!$merchant) {
            throw new Exception("操作有误",HTTP_FORBIDDEN);
        }
        self::$audit = $merchant->audit_type;
    }

    public function scopeMerchantId(Query $query)
    {
        $query->where('merchant_id', self::$merchantId);
    }

    public function getStatusTextAttr($value,$data)
    {
        $status = [0=>'禁用',1=>'正常'];
        return $status[$data['status']];
    }

//    public function getAuditTextAttr($value,$data)
//    {
//        $status = [0=>'审核中',1=>'审核通过'];
//        return $status[$data['status']];
//    }

    public function shopAssistant()
    {
        return $this->hasMany(ShopAssistant::class,'store_id','id');
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class,'merchant_id','id');
    }

    public static function onAfterRead(Model $model)
    {
        if (self::$merchantId == 1)
            unset($model->delete_time);
        else
            unset($model->merchant_id, $model->delete_time);
    }

//    public static function onBeforeInsert(Model $model)
//    {
//        parent::onBeforeInsert($model);
//
//            $model->audit = self::$audit;
//
//    }
//
//    public static function onBeforeUpdate(Model $model)
//    {
//        parent::onBeforeUpdate($model);
//
//        $model->audit = self::$audit;
//
//    }

}
