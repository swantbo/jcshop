<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\model;
use app\SearchModel;

class OrderAfter extends SearchModel
{
    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (isset($data['state']) && isset($data['type'])) self::append(['state_text','type_text']);
        //if(isset($data['type'])) self::append(['type_text']);
    }
    public function order()
    {
       return $this->belongsTo('Order','order_id','id');
    }

    public function process()
    {
        return $this->hasMany('AfterProcess','order_after_id','id');
    }

    public function afterCommodity()
    {
        return $this->hasMany('AfterOrderCommodity','order_after_id','id');
    }

    public function getImgsAttr(string $name)
    {
       return json_decode($name,true);
    }

    public function getStateTextAttr($value,$data)
    {
        //1-等待商家处理 2-同意申请 3-驳回申请 4-商家已发货，等待客户收货 5-手動退款  6-确认发货 7-退款退货完成 8-退款完成 9-换货完成 10-退回物品,等待退款  11-同意退款 12-等待买家退回商品 13-买家退回物品，等待退款 14-买家退回物品，等待商家重新发货
        $state = [
            1=>"等待商家处理",
            2=>"同意申请",
            3=>"驳回申请",
            4=>"商家已发货，等待客户收货",
            5=>"手动退款",
            6=>"确认发货",
            7=>"退款退货完成",
            8=>"退款完成",
            9=>"换货完成",
            10=>"退回物品,等待退款",
            11=>"同意退款",
            12=>"等待买家退回商品",
            13=>"买家退回物品，等待退款",
            14=>"买家退回物品，等待商家重新发货"
        ];
        return $state[$data['state']];
    }

    public function getTypeTextAttr($value,$data)
    {
        $arr = [1=>"退款",2=>"退货退款",3=>"换货"];
        return $arr[$data['type']];
    }
}