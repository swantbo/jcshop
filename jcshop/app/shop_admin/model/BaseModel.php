<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\model;


use app\SearchModel;
use think\db\Query;
use think\Model;

class BaseModel extends SearchModel
{
    protected static $merchantId;

    protected static $mallId;

    protected static $isSelf;

    protected static $auditType;

    protected $globalScope = ['merchantId', 'mallId'];

    public function __construct(array $data = [])
    {
        parent::__construct($data);

        global $user;
        if (!empty($user)) {
            self::$merchantId = $user->merchant_id;
            self::$mallId = $user->mall_id;
            self::$isSelf = $user->is_self ?? 1;
            self::$auditType = $user->audit_type ?? 0;
        }
    }

    public function scopeMerchantId(Query $query)
    {
        if (self::$merchantId){
            $query->where('merchant_id', self::$merchantId);
        }

    }

//    public function scopeMallId(Query $query)
//    {
//        $query->where('mall_id', self::$mallId);
//    }

    public static function onBeforeInsert(Model $model)
    {
        $model->merchant_id = self::$merchantId;
        $model->mall_id = self::$mallId;
    }

    public static function onAfterInsert(Model $model)
    {
        unset($model->merchant_id, $model->mall_id);
    }

    public static function onAfterRead(Model $model)
    {
        unset($model->delete_time);
    }

}