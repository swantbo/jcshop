<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\shop_admin\model;

use app\index\model\UserWx;
use think\model\concern\SoftDelete;

class User extends BaseMallModel
{
    use SoftDelete;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (isset($data['status'])){
            self::append(['status_text']);
        }
    }

    public function getStatusTextAttr($value,$data)
    {
        $status = [
            0 => '未启用',
            1 => '正常',
            2 => '过期',
            3 => '限制'
        ];
        return $status[$data['status']];
    }

    public function user_wx()
    {
        return $this->hasOne(UserWx::class,'user_id','id');
    }

    public function userWx()
    {
        return $this->hasOne(UserWx::class,'user_id','id');
    }

}
