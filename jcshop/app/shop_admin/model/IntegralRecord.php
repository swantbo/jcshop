<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\model;


use app\SearchModel;
use think\Model;

class IntegralRecord extends SearchModel
{
    public function getTypeTextAttr($value,$data) : string{
        $array = [
            1 => '消费获得',
            2 => '使用',
            3 => '分享获得',
            4 => '后台增加',
            5 => '后台减少',
            6 => '签到',
            7 => '新人领取'
        ];
        return $array[$data['type']];
    }
}