<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\shop_admin\service;

use app\Oss;
use think\facade\Filesystem;

class UploadService
{

    public function images($files)
    {
        global $user;
        $saveName = [];
        foreach ($files as $file) {
            foreach ($file as $v) {
                $path = "storage/" . Filesystem::disk('public')->putFile('shop_admin', $v);
                $oss = new Oss();
                $up = $oss->up($v->getPathname(), $path, $user->mall_id);
                if (!is_null($up)) {
                    !empty($up) && @unlink($path);
                    $saveName[] = $up;
                } else {
                    $saveName[] = request()->server("REQUEST_SCHEME") . '://' .$_SERVER['HTTP_HOST'] . '/' . $path;
                }
            }
        }

        return [HTTP_SUCCESS, $saveName];
    }

    public function upBase64Img($string)
    {
        $res = base64_image_content($string);
        return [HTTP_SUCCESS, $res];
    }

}
