<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\service;


use app\shop_admin\model\IntegralRecord;
use app\shop_admin\model\Order;
use think\Exception;
use think\facade\Db;

class IntegralService
{
    private $config;
    public function __construct()
    {
        global $user;
        $this->config = Db::name('configuration')
            ->where('type','integral')
            ->where('mall_id',$user['mall_id'])
            ->value('configuration');
        $this->config = json_decode($this->config);
    }

    /**
     * 计算积分抵扣的金额
     * @param int $integral
     * @return float
     */
    public function deduction(int $integral) : float {
        return bcdiv($integral , $this->config->deduction,2);
    }

    /**
     * 计算折扣后的订单金额
     * @param int $integral
     * @param int $order_id
     * @return float
     */
    public function deduction_money(int $integral, int $order_id) : float{
        $order = Order::field('user_id,money')->where('id',$order_id)->find();
        $user = Db::name('user_cash')
            ->where('user_id',$order->user_id)
            ->find();
        if ($user['integral'] < $integral)
            throw new Exception('积分不足',HTTP_INVALID);
        $amount = $this->deduction($integral);
        Db::name('user_cash')
            ->where('user_id',$user['user_id'])
            ->dec('integral',$user['integral'])
            ->update([
                'update_time' => date('Y-m-d H:i:s')
            ]);
        IntegralRecord::create([
            'user_id' => $user['user_id'],
            'order_id' => $order_id,
            'integral' => $integral,
            'type' => 2,
            'status' => 1,
            'create_time' => date('Y-m-d H:i:s'),
            'update_time' => date('Y-m-d H:i:s')
        ]);
        return $amount;
    }


    /**
     * 添加积分记录
     * @param int $user_id
     * @param int $order_id
     */
    public function integral_address(int $order_id) : void{
        $select = Db::name('order_commodity')
            ->alias('oc')
            ->field('s.sku_id,oc.commodity_id,oc.count,(case when oc.sku_id is null then c.integral else s.integral end) AS integral')
            ->join('commodity c','c.id=oc.commodity_id','left')
            ->join('sku_inventory s','s.sku_id=oc.sku_id','left')
            ->where('oc.order_id',$order_id)
            ->select();
        $user_id = Db::name('order')->where('id',$order_id)->value('user_id');
        foreach ($select AS $key => $value){
            $integral[] = [
                'user_id' => $user_id,
                'order_id' => $order_id,
                'integral' => $value['count'] * $value['integral'],
                'commodity_id' => $value['commodity_id'],
                'sku_id' => empty($value['sku_id']) ? 0 : $value['sku_id'],
                'number' => $value['count'],
                'type' => 1,
                'status' => 0,
                'create_time' => date('Y-m-d H:i:s'),
                'update_time' => date('Y-m-d H:i:s')
            ];
        }
        Db::name('integral_record')
            ->insertAll($integral);
    }

}