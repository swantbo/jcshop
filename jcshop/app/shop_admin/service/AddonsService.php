<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\shop_admin\service;

use think\facade\Db;

class AddonsService
{

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index()
    {
        global $user;
        $addonsList = Db::table("jiecheng_lessee")
            ->where("id",$user->mall_id)
            ->value("addons");
        if ($addonsList)
            $addonsList = json_decode($addonsList, true);
        return [HTTP_SUCCESS, $addonsList];

    }

}
