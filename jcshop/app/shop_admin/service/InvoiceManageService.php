<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\service;

use app\shop_admin\model\Invoice;
use think\Exception;

class InvoiceManageService
{
    private $merchant_id;
    private $mall_id;
    public function __construct()
    {
        global $user;
        $this->merchant_id = $user->merchant_id;
    }

    /**发票管理列表
     * @param array $data
     * @param int $page
     * @param int $size
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function invoiceList(array $data, string $page, string $size)
    {
        $res = Invoice::where($this->buildWhere($data))
            ->join('order o', 'a.order_id = o.id', 'LEFT')
            ->alias('a')
            ->order('a.status', 'asc')
            ->with(['user', 'order'])
            ->field('a.*,o.merchant_id')
            ->paginate(['page' => $page, 'list_rows' => $size]);
        return [HTTP_SUCCESS, $res];
    }

    /**修改数据
     * @param array $data
     * @return array
     */
    public function examine(array $data)
    {
        $id = $data['id'];
        $find = Invoice::where('status', 'IN', [2, 3])->find($id);
        if ($find) throw new Exception('已审核，请勿重复操作', HTTP_INVALID);
        unset($data['id']);
        $up = Invoice::update($data, ['id' => $id], ['status', 'remake']);
        return [HTTP_SUCCESS, $up];
    }

    /**读取一条数据
     * @param array $data
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function read(array $data)
    {
        $find = Invoice::with(['user'])->find($data['id']);
        return [HTTP_SUCCESS, $find];
    }

    public function buildWhere(array $data)
    {
        $where = [];
        $arr = [];
        $where[] = ['o.merchant_id', '=', $this->merchant_id];
        !empty($data['duty_paragraph']) && $where[] = ['a.duty_paragraph', 'like', $data['duty_paragraph'] . "%"];
        !empty($data['phone']) && $where[] = ['a.phone', '=', $data['phone']];
        !empty($data['status']) && $where[] = ['a.status', '=', $data['status']];
        !empty($data['inoice_type']) && $where[] = ['a.inoice_type', '=', $data['inoice_type']];
        if (array_key_exists('start_time', $data) && array_key_exists('end_time', $data))
            $arr = $this->setTime($data['start_time'], $data['end_time']);
        return array_merge($where, $arr);
    }

    private function setTime($start_time, $end_time)
    {
        $arr = [];
        if (!empty($start_time) && !empty($end_time))
            $arr[] = ['a.create_time', 'between', [$start_time, $end_time]];
        elseif (!empty($start_time) && empty($end_time))
            $arr[] = ['a.create_time', 'between', [$start_time, date('Y-m-d H:i:s', time())]];
        elseif (empty($start_time) && !empty($end_time))
            $arr[] = ['a.create_time', 'between', [date('Y-m-d H:i:s', strtotime($end_time) - 60), $end_time]];
        return $arr;
    }
}