<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types = 1);

namespace app\shop_admin\validate;

use think\Validate;

class Department extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
	protected $rule = [
        'name'          => 'require|max:32|chsAlphaNum',
        'remark'        => 'max:64',
        'update_time'   => 'dateFormat:Y-m-d H:i:s'
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [
        'name.require'              => '部门名必须填写',
        'name.length'               => '部门名最长32个字符',
        'name.chsAlphaNum'          => '部门名只能是汉字、字母和数字',
        'remark.max'                => '备注最长64个字符',
        'update_time.dateFormat'    => '请传入正确的时间戳'
    ];

    /**
     * 定义场景
     * @var \string[][]
     */
    protected $scene = [
        'save'    => ['name','remark'],
        'update'  => ['name','remark','update_time'],
    ];
}
