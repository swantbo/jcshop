<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\validate;


use think\Validate;

class CouponValidate extends Validate
{
    protected $rule = [
        'name|优惠券名称' => 'require|length:1,32',
        'denomination|面额' => 'require|float',
        'conditions|使用条件' => 'require|float',
        'time_limit_type|使用时间限制按钮' => 'require|in:0,1',
        'limit_time|限制天数' => 'integer',
        'limit_indate_begin|有效期开始时间' => 'dateFormat:Y-m-d H:i:s',
        'limit_indate_end|有效期结束时间' => 'dateFormat:Y-m-d H:i:s',
        'total_limit_type|发放总量限制按钮' => 'require|in:0,1',
        'total_limit|发放总量' => 'integer',
        'user_limit_type|每人限领按钮' => 'require|in:0,1',
        'user_limit|每人限领' => 'integer',
        'commodity_limit_type|使用限制条件按钮' => 'require|in:0,1,2,3',
        'use_limit_type|使用限制按钮' => 'require|in:0,1',
        'get_type|领取方式按钮' => 'require|in:0,1,2',
        'sort|排序' => 'integer',
        'explain_type|说明类型按钮' => 'require|in:0,1',
        'status|状态' => 'in:0,1,2',
        'update_time|更新时间' => 'require|dateFormat:Y-m-d H:i:s'

    ];
    protected $message = [

    ];
    protected $scene = [
        'save' => [
            'name',
            'denomination',
            'conditions',
            'time_limit_type',
            'limit_time',
            'limit_indate_begin',
            'limit_indate_end',
            'total_limit_type',
            'total_limit',
            'user_limit_type',
            'user_limit',
            'commodity_limit_type',
            'use_limit_type',
            'get_type',
            'sort',
            'explain_type',
            'status',
        ],
        'update' => [
            'update_time',
//            'name',
//            'denomination',
//            'conditions',
//            'time_limit_type',
//            'limit_time',
//            'limit_indate_begin',
//            'limit_indate_end',
//            'total_limit_type',
//            'total_limit',
//            'user_limit_type',
//            'user_limit',
//            'commodity_limit_type',
//            'use_limit_type',
//            'get_type',
//            'sort',
//            'explain_type',
//            'status',
        ]

    ];
}