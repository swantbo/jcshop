<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\validate;


use think\Validate;

class AdminValidate extends Validate
{
    protected $rule = [
        'page' => 'require|integer',
        'size' => 'require|integer',
        'username' => 'require|max:32',
        'password' => 'require',
        'mobile' => 'require|moblie',
        'update_time' => 'require|dateFormat:Y-m-d H:i:s',
        'begin_date' => 'dateFormat:Y-m-d',
        'end_date' => 'dateFormat:Y-m-d'
    ];
    protected $message = [
        'page' => '页数必须上传',
        'size' => '显示数量必须上传',
        'username.require' => '账号未上传',
//        'username.max' => '',
        'password' => '密码未填写',
        'mobile' => '请填写正确的手机号',
        'update_time' => '更新时间不正确',
        'begin_date' => '开始时间必须是日期格式',
        'end_date' => '结束时间必须是日期格式'
    ];
    protected $scene = [
        'list' => ['page','size','begin_date','end_date'],
        'save' => ['username','password','mobile'],
        'update' => ['update_time']
    ];
}