<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\validate;


use think\Validate;

class FreightTempValidate extends Validate
{
    protected $rule = [
        'page' => 'require|integer',
        'size' => 'require|integer',
        'name' => 'require|max:32',
        'express_id' => 'require|integer',
        'carry_mode_type' => 'require|integer',
        'status' => 'integer'
    ];
    protected $message = [
        'page' => '页数必须上传',
        'size' => '显示数量必须上传',
        'name.require' => '模板名称不能为空',
        'name.max' => '模板名称限定在32个字符内',
        'express_id.require' => '快递必须上传',
        'express_id.integer' => '快递格式错误',
        'carry_mode_type.require' => '计费标准必须选择',
        'carry_mode_type.integer' => '计费标准错误',
        'status.integer' => '状态格式错误'
    ];
    protected $scene = [
        'list' => ['page','size','status'],
        'save' => ['name','express_id','carry_mode_type']
    ];
}