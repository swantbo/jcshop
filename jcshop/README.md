节程开源商城 v1.0 
===============

> 运行环境要求PHP7.1-7.4。数据库mysql 5.7
## 安装

## 一键安装
上传你的代码，站点入口目录设置/public
在浏览器中输入你的域名或IP（例如：www.xxx.com）,
- 域名/install.html 一键安装

后台访问地址：

- 域名/mall.html   管理端
- 域名/shop.html   多商户
- 域名/msystem     手机端

安装过程中请牢记您的账号密码！


## 手动安装
1.创建数据库，倒入数据库文件
数据库文件目录/public/install.sql
2.修改数据库连接文件
配置文件路径/.example.env
~~~
APP_DEBUG = true

[APP]
DEFAULT_TIMEZONE = Asia/Shanghai

[DATABASE]
TYPE = mysql
HOSTNAME = 127.0.0.1       #数据库地址
DATABASE = test                #数据库名称
USERNAME = username     #数据库登录账号
PASSWORD = password     #数据库登录密码
HOSTPORT = 3306             #数据库端口
CHARSET = utf8
DEBUG = true
CHARSET = utf8
PREFIX = jiecheng_


[REDIS]
HOST = 127.0.0.1  # redis链接地址
REDISPORT = 6379  #端口号
REDISPASSWORD = dnmp  # redis密码
SELECT = 1         # redis库

[LANG]
default_lang = zh-cn
~~~


## 文档

[TP6开发手册](https://www.kancloud.cn/manual/thinkphp6_0/content)


## 版权信息


本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2022-2029 by 节程 (https://www.jc362.com)

All rights reserved。

节程® 商标和著作权所有者 温州惊蛰网络科技有限公司。
